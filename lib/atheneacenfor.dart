import 'package:mailer/smtp_server.dart';

SmtpServer atheneacenfor(String username, String password) => new SmtpServer('smtp.gmail.com',
    ssl: true, port: 465, allowInsecure: false, ignoreBadCertificate: true, username: username, password: password);
