import 'package:flutter/material.dart';

const Color ibmobBlue = Colors.black;

//______________________________________TELECABLE_COLORES_DE_LA_EMPRESA

const Color amarilloTC = Color.fromRGBO(247, 180, 19, 1.0);

const Color naranjaTC = Color.fromRGBO(237, 125, 36, 1.0);

const Color rojoTC = Color.fromRGBO(230, 65, 54, 1.0);

const Color rosaTC = Color.fromRGBO(191, 51, 138, 1.0);

const Color moradoTC = Color.fromRGBO(100, 77, 154, 1.0);

const Color azulTC = Colors.black;

const Color verdeazuladoTC = Color.fromRGBO(17, 168, 159, 1.0);

const Color verdeTC = Color.fromRGBO(137, 190, 63, 1.0);

const Color negroTC = Color.fromRGBO(0, 0, 0, 1.0);

//Colores ATHENEA

const Color darkBlueAthenea = Color.fromRGBO(18, 1, 54, 1.0);
const Color blueAthenea = Color.fromRGBO(3, 90, 166, 1.0);
const Color lightBlueAthenea = Color.fromRGBO(64, 186, 213, 1.0);
const Color yellowAthenea = Color.fromRGBO(252, 191, 30, 1.0);
const Color whiteAthenea = Colors.white;
const Color blackAthenea = Colors.black;
const Color redAthenea = Colors.red;
