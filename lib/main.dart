import 'package:comunicacionesatheneacenfor/constants.dart';
import 'package:comunicacionesatheneacenfor/pages/about_page.dart';
import 'package:comunicacionesatheneacenfor/pages/altaCliente_page.dart';
import 'package:comunicacionesatheneacenfor/pages/cambiar_password_page.dart';
import 'package:comunicacionesatheneacenfor/pages/configuracion_page.dart';
import 'package:comunicacionesatheneacenfor/pages/consumo_page.dart';
import 'package:comunicacionesatheneacenfor/pages/detalle_page.dart';
import 'package:comunicacionesatheneacenfor/pages/login_form.dart';
import 'package:comunicacionesatheneacenfor/pages/recuperar_password_page.dart';
import 'package:comunicacionesatheneacenfor/pages/splash_screen_page.dart';
import 'package:comunicacionesatheneacenfor/pages/tus_datos_page.dart';
import 'package:comunicacionesatheneacenfor/providers/clientes_info_provider.dart';
import 'package:comunicacionesatheneacenfor/providers/consumo_provider.dart';
import 'package:comunicacionesatheneacenfor/providers/dropdownvalue_provider.dart';
import 'package:comunicacionesatheneacenfor/providers/estado_features_provider.dart';
import 'package:comunicacionesatheneacenfor/providers/lineas_cliente_provider.dart';
import 'package:comunicacionesatheneacenfor/providers/llamadas_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: blackAthenea,
    ));
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ClientesInfoProvider()),
        ChangeNotifierProvider(create: (context) => LineasClienteProvider()),
        ChangeNotifierProvider(create: (context) => EstadosFeaturesProvider()),
        ChangeNotifierProvider(create: (context) => LlamadasProvider()),
        ChangeNotifierProvider(create: (context) => ConsumoProvider()),
        ChangeNotifierProvider(create: (context) => DropDownValueProvider()),
      ],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Athenea Cenfor',
          theme: ThemeData(
            unselectedWidgetColor: Colors.black,
            backgroundColor: Colors.white,
            scaffoldBackgroundColor: Colors.white,
            appBarTheme: AppBarTheme(
                brightness: Brightness.light,
                color: Colors.white,
                textTheme: TextTheme(
                  headline1: TextStyle(color: Colors.black),
                  headline2: TextStyle(color: Colors.black),
                  headline3: TextStyle(color: Colors.black),
                  headline4: TextStyle(color: Colors.black),
                  headline5: TextStyle(color: Colors.black),
                  headline6: TextStyle(fontSize: 20, color: Colors.black),
                  subtitle1: TextStyle(color: Colors.black),
                  subtitle2: TextStyle(color: Colors.black),
                  bodyText1: TextStyle(color: Colors.black),
                  bodyText2: TextStyle(color: Colors.black),
                ),
                iconTheme: IconThemeData(color: Colors.black),
                elevation: 0),
            primaryColor: Colors.black,
            primarySwatch: Colors.indigo,
            //___________________________________________COLOR FIELDSET
            hintColor: Colors.black,
            inputDecorationTheme: InputDecorationTheme(
              border: OutlineInputBorder(),
              //_____________________________________COLOR_FIELDSET_INPUT_PASSWORD
              labelStyle: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
          initialRoute: 'splash',
          routes: {
            'splash': (BuildContext context) => SplashScreen(),
            'login': (BuildContext context) => LoginForm(),
            'consumo': (BuildContext context) => ConsumoPage(),
            'detalle': (BuildContext context) => DetallePage(),
            'configuracion': (BuildContext context) => ConfiguracionPage(),
            'altacliente': (BuildContext context) => AltaCliente(),
            'recuperarpassword': (BuildContext context) => RecuperarPassword(),
            'cambiarpassword': (BuildContext context) => CambiarPassword(),
            'tusdatos': (BuildContext context) => TusDatosPage(),
            'about': (BuildContext context) => AboutPage(),
          }),
    );
  }
}
