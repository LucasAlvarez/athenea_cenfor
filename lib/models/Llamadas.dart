class Llamadas {
  List<Llamada> items = new List();
  Llamadas();
  Llamadas.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;
    for (var item in jsonList) {
      final llamada = new Llamada.fromJsonMap(item);
      items.add(llamada);
    }
  }
}

class Llamada {
  String telefono;
  String tipon;
  String tipo;
  String fecha;
  String hora;
  String destino;
  String duracion;
  String acumulado;
  String importe;
  String bono;
  String cadenap;

  Llamada(this.telefono, this.tipon, this.tipo, this.fecha, this.hora, this.destino, this.duracion, this.acumulado, this.importe, this.bono,
      this.cadenap);
  Llamada.fromJsonMap(Map<String, dynamic> json) {
    telefono = json['telf'];
    tipon = json['tipon'];
    tipo = json['tipo'];
    fecha = json['fecha'];
    hora = json['hora'];
    destino = json['dest'];
    duracion = json['dura'];
    acumulado = json['acum'];
    importe = json['imp'];
    bono = json['bono'];
    cadenap = json['cadenap'];
  }
}
