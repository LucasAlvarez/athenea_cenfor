class Features {
  List<Feature> items = new List();
  Features();
  Features.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;
    for (var item in jsonList) {
      final feature = new Feature.fromJsonMap(item);
      items.add(feature);
    }
  }
}

class Feature {
  String nombre;
  String comando;
  String estado;
  String parm1;
  String parm2;
  String gestionUsuario;
  String gestionCliente;

  Feature(this.nombre, this.comando, this.estado, this.parm1, this.parm2, this.gestionUsuario, this.gestionCliente);
  Feature.fromJsonMap(Map<String, dynamic> json) {
    nombre = json['nombre'];
    comando = json['comando'];
    estado = json['estado'];
    parm1 = json['parm1'];
    parm2 = json['parm2'];
    gestionUsuario = json['gestion_usuario'];
    gestionCliente = json['gestion_cliente'];
  }
}
