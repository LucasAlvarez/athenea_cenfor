class Linea {
  String telefono;
  String peticion;
  Linea(String telefono, String peticion) {
    this.telefono = telefono;
    this.peticion = peticion;
  }
  Linea.fromJsonMap(Map<String, dynamic> json) {
    telefono = json['Telefono'];
    peticion = json['Peticion'];
  }
}

class Lineas {
  List<Linea> items = new List();
  Lineas();
  Lineas.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;
    for (var item in jsonList) {
      final linea = new Linea.fromJsonMap(item);
      items.add(linea);
    }
  }
}
