import 'package:comunicacionesatheneacenfor/constants.dart';
import 'package:comunicacionesatheneacenfor/widgets/size_config.dart';
import 'package:flutter/material.dart';

class AboutPage extends StatefulWidget {
  @override
  State<AboutPage> createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  //VARIABLES QUE VAMOS A USAR
  String currentVersion;
  var key = UniqueKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);
            return Scaffold(
              appBar: AppBar(
                backgroundColor: blueAthenea,
                iconTheme: IconThemeData(
                  color: yellowAthenea,
                ),
                title: Text('Acerca de', style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: whiteAthenea)),
                centerTitle: false,
              ),
              body: _body(),
            );
          },
        );
      },
    );
  }

  //BODY
  _body() {
    return Center(
      child: Container(
        height: 50 * SizeConfig.heightMultiplier,
        padding: EdgeInsets.symmetric(
          vertical: 1 * SizeConfig.heightMultiplier,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 5 * SizeConfig.heightMultiplier),
            _logo(),
            SizedBox(height: 4 * SizeConfig.heightMultiplier),
            Expanded(child: Container()),
            Text(
              "Versión 1.0",
              style: TextStyle(
                color: darkBlueAthenea,
                fontWeight: FontWeight.bold,
                fontSize: 2 * SizeConfig.heightMultiplier,
              ),
            ),
            SizedBox(height: 4 * SizeConfig.heightMultiplier),
            Text(
              '2023 ® PTV TELECOM \n Todos los derechos reservados',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 1.8 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
            ),
          ],
        ),
      ),
    );
  }

  //LOGO
  _logo() {
    return Center(
      child: Column(
        children: <Widget>[
          Container(
            /*     height: 25 * SizeConfig.heightMultiplier, */
            width: 40 * SizeConfig.heightMultiplier,
            child: Image(
              image: AssetImage('assets/images/logomesa.png'),
            ),
          ),
        ],
      ),
    );
  }
}
