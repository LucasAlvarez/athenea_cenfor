import 'package:comunicacionesatheneacenfor/constants.dart';
import 'package:comunicacionesatheneacenfor/widgets/form_alta_widget.dart';
import 'package:comunicacionesatheneacenfor/widgets/size_config.dart';
import 'package:flutter/material.dart';

class AltaCliente extends StatefulWidget {
  AltaCliente({Key key}) : super(key: key);

  @override
  _AltaClienteState createState() => _AltaClienteState();
}

class _AltaClienteState extends State<AltaCliente> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(builder: (context, orientation) {
        SizeConfig().init(constraints, orientation);
        return Scaffold(
          appBar: AppBar(
            backgroundColor: blueAthenea,
            iconTheme: IconThemeData(
              color: yellowAthenea,
            ),
            title: Text('Alta de nuevo usuario', style: TextStyle(fontSize: 2.5 * SizeConfig.heightMultiplier, color: whiteAthenea)),
          ),
          body: Center(child: FormAlta()),
        );
      });
    });
  }
}
