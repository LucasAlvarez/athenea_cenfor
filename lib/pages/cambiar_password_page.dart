import 'package:comunicacionesatheneacenfor/constants.dart';
import 'package:comunicacionesatheneacenfor/widgets/form_cambiar_password_widget.dart';
import 'package:comunicacionesatheneacenfor/widgets/size_config.dart';
import 'package:flutter/material.dart';

class CambiarPassword extends StatefulWidget {
  CambiarPassword({Key key}) : super(key: key);

  @override
  State<CambiarPassword> createState() => _CambiarPasswordState();
}

class _CambiarPasswordState extends State<CambiarPassword> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(builder: (context, orientation) {
        SizeConfig().init(constraints, orientation);
        return Scaffold(
          appBar: AppBar(
            backgroundColor: blueAthenea,
            title: Text(
              'Cambiar contraseña',
              style: TextStyle(
                fontSize: 2 * SizeConfig.heightMultiplier,
                color: whiteAthenea,
              ),
            ),
            iconTheme: IconThemeData(
              color: yellowAthenea,
            ),
          ),
          body: Center(child: FormCambiarPassword()),
        );
      });
    });
  }
}
