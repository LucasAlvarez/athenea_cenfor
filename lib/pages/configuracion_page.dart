import 'package:comunicacionesatheneacenfor/constants.dart';
import 'package:comunicacionesatheneacenfor/models/feature.dart';
import 'package:comunicacionesatheneacenfor/models/linea.dart';
import 'package:comunicacionesatheneacenfor/providers/estado_features_provider.dart';
import 'package:comunicacionesatheneacenfor/providers/lineas_cliente_provider.dart';
import 'package:comunicacionesatheneacenfor/widgets/size_config.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:xml2json/xml2json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ConfiguracionPage extends StatefulWidget {
  ConfiguracionPage({Key key}) : super(key: key);

  @override
  State<ConfiguracionPage> createState() => _ConfiguracionPageState();
}

class _ConfiguracionPageState extends State<ConfiguracionPage> {
  ProgressDialog pr;
  ProgressDialog prGrabar;
  final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();
  TextEditingController _textEditingController = TextEditingController();
  var desvioCondicionalCodeCountry;
  var desvioCondicionalPhone;
  var desvioAusenciaCodeCountry;
  var desvioAusenciaPhone;
  var desvioAusenciaTiempo;
  var desvioOcupadoCodeCountry;
  var desvioOcupadoPhone;
  var desvioSinConexionCodeCountry;
  var desvioSinConexionPhone;
  var _tiempoDesvioAusencia = ['5', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55'];

  bool switchVal1 = false;
  bool switchVal2 = false;
  bool switchVal3 = false;
  bool switchVal4 = false;
  bool switchVal5 = false;
  bool switchVal6 = false;
  bool switchVal7 = false;
  bool switchVal8 = false;
  bool switchVal9 = false;
  bool switchVal10 = false;
  bool switchVal11 = false;
  bool switchVal12 = false;

  String dropdownValue = 'Elige un teléfono';

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);

    pr.style(message: 'Leyendo Configuración...');

    var lineasClienteProvider = Provider.of<LineasClienteProvider>(context);
    List<Linea> lineasCliente = lineasClienteProvider.lineasCliente;
    List<String> menuItems = [];
    for (var i = 0; i < lineasCliente.length; i++) {
      var numero = int.parse(lineasCliente[i].telefono);
      if (numero > 600000000 && numero < 799999999) {
        menuItems.add(lineasCliente[i].telefono);
      } else {}
    }
    final List<DropdownMenuItem<String>> _dropDownMenuItems = menuItems
        .map(
          (String value) => DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          ),
        )
        .toList();

    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(
        builder: (context, orientation) {
          SizeConfig().init(constraints, orientation);
          return Scaffold(
            appBar: AppBar(
              backgroundColor: blueAthenea,
              iconTheme: IconThemeData(
                color: yellowAthenea,
              ),
              title: Text('Configurar servicios',
                  style: TextStyle(
                    fontSize: 2 * SizeConfig.heightMultiplier,
                    color: whiteAthenea,
                  )),
            ),
            body: SingleChildScrollView(
              child: FormBuilder(
                key: _formKey,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(
                    1 * SizeConfig.heightMultiplier,
                    1 * SizeConfig.heightMultiplier,
                    1 * SizeConfig.heightMultiplier,
                    1 * SizeConfig.heightMultiplier,
                  ),
                  child: Card(
                    shadowColor: darkBlueAthenea,
                    child: Column(
                      /*    runSpacing: 15.0,
                      alignment: WrapAlignment.center, */
                      children: <Widget>[
                        Text(
                          '¿Deseas modificar las características de tu línea móvil?',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: darkBlueAthenea, fontSize: 2 * SizeConfig.heightMultiplier),
                        ),
                        SizedBox(
                          height: 2 * SizeConfig.heightMultiplier,
                        ),
                        Container(
                          width: SizeConfig.heightMultiplier * 25,
                          decoration: BoxDecoration(
                            border: Border.all(color: blueAthenea, width: 3.0),
                            borderRadius: BorderRadius.circular(3.0),
                          ),
                          child: Center(
                            child: DropdownButton<String>(
                                isDense: true,
                                value: null,
                                hint: Text(
                                  dropdownValue,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 1.8 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                                ),
                                icon: Icon(
                                  Icons.keyboard_arrow_down,
                                  color: yellowAthenea,
                                ),
                                iconSize: 4 * SizeConfig.heightMultiplier,
                                elevation: 16,
                                style: TextStyle(
                                  color: darkBlueAthenea,
                                  fontSize: 1.8 * SizeConfig.heightMultiplier,
                                ),
                                underline: Container(
                                  height: 0,
                                ),
                                onChanged: (String value) {
                                  setState(() {
                                    dropdownValue = value;
                                    switchVal1 = false;
                                    switchVal2 = false;
                                    switchVal3 = false;
                                    switchVal4 = false;
                                    switchVal5 = false;
                                    switchVal6 = false;
                                    switchVal7 = false;
                                    switchVal8 = false;
                                    switchVal9 = false;
                                    switchVal10 = false;
                                    switchVal11 = false;
                                    switchVal12 = false;
                                    _leerFeatures();
                                  });
                                },
                                items: _dropDownMenuItems),
                          ),
                        ),
                        SizedBox(
                          height: 2 * SizeConfig.heightMultiplier,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Text(
                            'Servicios de la línea:   $dropdownValue',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontWeight: FontWeight.bold,
                              fontSize: 1.8 * SizeConfig.heightMultiplier,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 0.5 * SizeConfig.heightMultiplier,
                        ),
                        // SWITCH SERVICIOS
                        dropdownValue == 'Elige un teléfono'
                            ? Container()
                            : Center(
                                child: Container(
                                  /*  width: MediaQuery.of(context).size.width - 10, */
                                  width: 95 * SizeConfig.heightMultiplier,
                                  child: Column(
                                    children: <Widget>[
                                      //SWITCH 1 - BUZON DE VOZ
                                      Row(
                                        children: <Widget>[
                                          Switch(
                                            activeColor: lightBlueAthenea,
                                            onChanged: (bool value) {
                                              setState(() => this.switchVal1 = value);
                                            },
                                            value: this.switchVal1,
                                          ),
                                          SizedBox(
                                            width: 2 * SizeConfig.heightMultiplier,
                                          ),
                                          Text(
                                            'Buzón de voz',
                                            style: TextStyle(color: darkBlueAthenea, fontSize: 1.8 * SizeConfig.heightMultiplier),
                                          )
                                        ],
                                      ),
                                      //SWITCH 2 - CONFERENCIA TRIPARTITA
                                      Row(
                                        children: <Widget>[
                                          Switch(
                                            activeColor: lightBlueAthenea,
                                            onChanged: (bool value) {
                                              setState(() => this.switchVal2 = value);
                                            },
                                            value: this.switchVal2,
                                          ),
                                          SizedBox(
                                            width: 2 * SizeConfig.heightMultiplier,
                                          ),
                                          Text(
                                            'Conferencia tripartita',
                                            style: TextStyle(
                                              color: darkBlueAthenea,
                                              fontSize: 1.8 * SizeConfig.heightMultiplier,
                                            ),
                                          )
                                        ],
                                      ),
                                      //SWITCH 3 - DESVIO CONDICIONAL
                                      Row(
                                        children: <Widget>[
                                          Switch(
                                            activeColor: lightBlueAthenea,
                                            onChanged: (bool value) {
                                              setState(() {
                                                this.switchVal3 = value;
                                                if (value == true) {
                                                  this.switchVal1 = false;
                                                  this.switchVal4 = false;
                                                  this.switchVal5 = false;
                                                  this.switchVal6 = false;
                                                }
                                                this.switchVal3 = value;
                                              });
                                            },
                                            value: this.switchVal3,
                                          ),
                                          SizedBox(
                                            width: 2 * SizeConfig.heightMultiplier,
                                          ),
                                          Text(
                                            'Desvío condicional',
                                            style: TextStyle(
                                              color: darkBlueAthenea,
                                              fontSize: 1.8 * SizeConfig.heightMultiplier,
                                            ),
                                          )
                                        ],
                                      ),
                                      this.switchVal3 == false
                                          ? Container()
                                          : Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                  Padding(
                                                    padding: EdgeInsets.fromLTRB(
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                    ),
                                                    child: Text(
                                                      'Teléfono',
                                                      style: TextStyle(
                                                        fontSize: 1.8 * SizeConfig.heightMultiplier,
                                                        color: darkBlueAthenea,
                                                      ),
                                                    ),
                                                  ),
                                                  Row(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: <Widget>[
                                                      Padding(
                                                        padding: EdgeInsets.fromLTRB(
                                                          0.5 * SizeConfig.heightMultiplier,
                                                          0.5 * SizeConfig.heightMultiplier,
                                                          0.5 * SizeConfig.heightMultiplier,
                                                          0.5 * SizeConfig.heightMultiplier,
                                                        ),
                                                        child: CountryCodePicker(
                                                          textStyle: TextStyle(
                                                            fontSize: 1.8 * SizeConfig.heightMultiplier,
                                                            color: darkBlueAthenea,
                                                          ),
                                                          // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                                                          initialSelection: 'ES',
                                                          favorite: ['+34', 'ES'],
                                                          // optional. Shows only country name and flag
                                                          showCountryOnly: false,
                                                          // optional. Shows only country name and flag when popup is closed.
                                                          showOnlyCountryWhenClosed: false,
                                                          // optional. aligns the flag and the Text left
                                                          alignLeft: false,
                                                          onChanged: (value) {
                                                            setState(() {
                                                              desvioCondicionalCodeCountry = value;
                                                            });
                                                          },
                                                        ),
                                                      ),
                                                      SizedBox(width: 1 * SizeConfig.heightMultiplier),
                                                      Expanded(
                                                        child: Padding(
                                                          padding: EdgeInsets.fromLTRB(
                                                            0 * SizeConfig.heightMultiplier,
                                                            0 * SizeConfig.heightMultiplier,
                                                            15 * SizeConfig.heightMultiplier,
                                                            0 * SizeConfig.heightMultiplier,
                                                          ),
                                                          child: FormBuilderTextField(
                                                            decoration: InputDecoration(
                                                              border: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                                                              focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                                                              enabledBorder: OutlineInputBorder(
                                                                  borderSide: BorderSide(color: blueAthenea),
                                                                  borderRadius: BorderRadius.circular(3.0)),
                                                            ),
                                                            controller: TextEditingController(text: desvioCondicionalPhone),
                                                            keyboardType: TextInputType.phone,
                                                            name: "Teléfono",
                                                            style: TextStyle(
                                                              color: darkBlueAthenea,
                                                              fontSize: 1.8 * SizeConfig.heightMultiplier,
                                                            ),
                                                            onChanged: (value) {
                                                              /*  setState(() { */
                                                              desvioCondicionalPhone = value;
                                                              /*  }); */
                                                            },
                                                            valueTransformer: (text) => num.tryParse(text),
                                                            validator: FormBuilderValidators.compose([
                                                              FormBuilderValidators.required(context,
                                                                  errorText: 'Debe rellenar este campo'),
                                                              FormBuilderValidators.numeric(context, errorText: 'Sólo se admiten números'),
                                                              FormBuilderValidators.min(context, 600000000,
                                                                  errorText: 'El número introducido no es válido'),
                                                              FormBuilderValidators.max(context, 799999999,
                                                                  errorText: 'El número introducido no es válido'),
                                                            ]),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ]),
                                      //SWITCH 4 - DESVIO AUSENCIA
                                      Row(
                                        children: <Widget>[
                                          Switch(
                                            activeColor: lightBlueAthenea,
                                            onChanged: (bool value) {
                                              setState(() => this.switchVal4 = value);
                                            },
                                            value: this.switchVal4,
                                          ),
                                          SizedBox(
                                            width: 2 * SizeConfig.heightMultiplier,
                                          ),
                                          Text(
                                            'Desvío ausencia',
                                            style: TextStyle(
                                              color: darkBlueAthenea,
                                              fontSize: 1.8 * SizeConfig.heightMultiplier,
                                            ),
                                          )
                                        ],
                                      ),
                                      this.switchVal4 == false
                                          ? Container()
                                          : Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                  Padding(
                                                    padding: EdgeInsets.fromLTRB(
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                    ),
                                                    child: Text(
                                                      'Teléfono',
                                                      style: TextStyle(
                                                        color: darkBlueAthenea,
                                                        fontSize: 1.8 * SizeConfig.heightMultiplier,
                                                      ),
                                                    ),
                                                  ),
                                                  Row(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: <Widget>[
                                                      Padding(
                                                        padding: EdgeInsets.fromLTRB(
                                                          0.5 * SizeConfig.heightMultiplier,
                                                          0.5 * SizeConfig.heightMultiplier,
                                                          0.5 * SizeConfig.heightMultiplier,
                                                          0.5 * SizeConfig.heightMultiplier,
                                                        ),
                                                        child: CountryCodePicker(
                                                          textStyle: TextStyle(
                                                            fontSize: 1.8 * SizeConfig.heightMultiplier,
                                                            color: darkBlueAthenea,
                                                          ),
                                                          // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                                                          initialSelection: 'ES',
                                                          favorite: ['+34', 'ES'],
                                                          // optional. Shows only country name and flag
                                                          showCountryOnly: true,
                                                          // optional. Shows only country name and flag when popup is closed.
                                                          showOnlyCountryWhenClosed: false,
                                                          // optional. aligns the flag and the Text left
                                                          alignLeft: false,
                                                          onChanged: (value) {
                                                            setState(() {
                                                              desvioAusenciaCodeCountry = value;
                                                            });
                                                          },
                                                        ),
                                                      ),
                                                      SizedBox(width: 1 * SizeConfig.heightMultiplier),
                                                      Expanded(
                                                        flex: 2,
                                                        child: Padding(
                                                          padding: EdgeInsets.fromLTRB(
                                                            0 * SizeConfig.heightMultiplier,
                                                            0 * SizeConfig.heightMultiplier,
                                                            1 * SizeConfig.heightMultiplier,
                                                            0 * SizeConfig.heightMultiplier,
                                                          ),
                                                          child: FormBuilderTextField(
                                                              decoration: InputDecoration(
                                                                border: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                                                                focusedBorder:
                                                                    OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                                                                enabledBorder: OutlineInputBorder(
                                                                    borderSide: BorderSide(color: blueAthenea),
                                                                    borderRadius: BorderRadius.circular(3.0)),
                                                              ),
                                                              controller: TextEditingController(text: desvioAusenciaPhone),
                                                              keyboardType: TextInputType.phone,
                                                              name: "Teléfono",
                                                              style: TextStyle(
                                                                color: darkBlueAthenea,
                                                                fontSize: 1.8 * SizeConfig.heightMultiplier,
                                                              ),
                                                              onChanged: (value) {
                                                                /* setState(() { */
                                                                desvioAusenciaPhone = value;
                                                                /*  }); */
                                                              },
                                                              valueTransformer: (text) => num.tryParse(text),
                                                              validator: FormBuilderValidators.compose([
                                                                FormBuilderValidators.required(context,
                                                                    errorText: 'Debe rellenar este campo'),
                                                                FormBuilderValidators.numeric(
                                                                  context,
                                                                  errorText: 'Sólo se admiten números',
                                                                ),
                                                                FormBuilderValidators.min(context, 600000000,
                                                                    errorText: 'El número introducido no es válido'),
                                                                FormBuilderValidators.max(context, 799999999,
                                                                    errorText: 'El número introducido no es válido'),
                                                              ])),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 1 * SizeConfig.heightMultiplier,
                                                      ),
                                                      Expanded(
                                                          flex: 1,
                                                          child: DropdownButton<String>(
                                                            focusColor: blueAthenea,
                                                            items: _tiempoDesvioAusencia.map((String dropDownStringItem) {
                                                              return DropdownMenuItem<String>(
                                                                value: dropDownStringItem,
                                                                child: Text(dropDownStringItem,
                                                                    style: TextStyle(
                                                                        color: darkBlueAthenea,
                                                                        fontSize: 1.8 * SizeConfig.heightMultiplier)),
                                                              );
                                                            }).toList(),
                                                            onChanged: (String newTiempoSeleccionado) {
                                                              setState(() {
                                                                this.desvioAusenciaTiempo = newTiempoSeleccionado;
                                                              });
                                                            },
                                                            value: desvioAusenciaTiempo,
                                                          )),
                                                      SizedBox(
                                                        width: 1.5 * SizeConfig.heightMultiplier,
                                                      ),
                                                    ],
                                                  ),
                                                ]),
                                      //SWITCH 5 - DESVIO POR OCUPADO
                                      Row(
                                        children: <Widget>[
                                          Flexible(
                                            flex: 1,
                                            child: Switch(
                                              activeColor: lightBlueAthenea,
                                              onChanged: (bool value) {
                                                setState(() => this.switchVal5 = value);
                                              },
                                              value: this.switchVal5,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 2 * SizeConfig.heightMultiplier,
                                          ),
                                          Flexible(
                                            flex: 2,
                                            child: Text(
                                              'Desvío por ocupado',
                                              style: TextStyle(
                                                color: darkBlueAthenea,
                                                fontSize: 1.8 * SizeConfig.heightMultiplier,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      this.switchVal5 == false
                                          ? Container()
                                          : Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                  Padding(
                                                    padding: EdgeInsets.fromLTRB(
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                    ),
                                                    child: Text('Teléfono',
                                                        style: TextStyle(
                                                          color: darkBlueAthenea,
                                                          fontSize: 1.8 * SizeConfig.heightMultiplier,
                                                        )),
                                                  ),
                                                  Row(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: <Widget>[
                                                      Padding(
                                                        padding: EdgeInsets.fromLTRB(
                                                          0.5 * SizeConfig.heightMultiplier,
                                                          0.5 * SizeConfig.heightMultiplier,
                                                          0.5 * SizeConfig.heightMultiplier,
                                                          0.5 * SizeConfig.heightMultiplier,
                                                        ),
                                                        child: CountryCodePicker(
                                                          textStyle: TextStyle(
                                                            fontSize: 1.8 * SizeConfig.heightMultiplier,
                                                            color: darkBlueAthenea,
                                                          ),
                                                          // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                                                          initialSelection: 'ES',
                                                          favorite: ['+34', 'ES'],
                                                          // optional. Shows only country name and flag
                                                          showCountryOnly: true,
                                                          // optional. Shows only country name and flag when popup is closed.
                                                          showOnlyCountryWhenClosed: false,
                                                          // optional. aligns the flag and the Text left
                                                          alignLeft: false,
                                                          onChanged: (value) {
                                                            setState(() {
                                                              desvioOcupadoCodeCountry = value;
                                                            });
                                                          },
                                                        ),
                                                      ),
                                                      SizedBox(width: 1 * SizeConfig.heightMultiplier),
                                                      Expanded(
                                                        flex: 2,
                                                        child: Padding(
                                                          padding: EdgeInsets.fromLTRB(
                                                            0 * SizeConfig.heightMultiplier,
                                                            0 * SizeConfig.heightMultiplier,
                                                            15 * SizeConfig.heightMultiplier,
                                                            0 * SizeConfig.heightMultiplier,
                                                          ),
                                                          child: FormBuilderTextField(
                                                              decoration: InputDecoration(
                                                                border: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                                                                focusedBorder:
                                                                    OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                                                                enabledBorder: OutlineInputBorder(
                                                                    borderSide: BorderSide(color: blueAthenea),
                                                                    borderRadius: BorderRadius.circular(3.0)),
                                                              ),
                                                              controller: TextEditingController(text: desvioOcupadoPhone),
                                                              keyboardType: TextInputType.phone,
                                                              name: "Teléfono",
                                                              style: TextStyle(
                                                                color: darkBlueAthenea,
                                                                fontSize: 1.8 * SizeConfig.heightMultiplier,
                                                              ),
                                                              onChanged: (value) {
                                                                /* setState(() { */
                                                                desvioOcupadoPhone = value;
                                                                /* }); */
                                                              },
                                                              valueTransformer: (text) => num.tryParse(text),
                                                              validator: FormBuilderValidators.compose([
                                                                FormBuilderValidators.required(context,
                                                                    errorText: 'Debe rellenar este campo'),
                                                                FormBuilderValidators.numeric(context,
                                                                    errorText: 'Sólo se admiten números'),
                                                                FormBuilderValidators.min(context, 600000000,
                                                                    errorText: 'El número introducido no es válido'),
                                                                FormBuilderValidators.max(context, 799999999,
                                                                    errorText: 'El número introducido no es válido'),
                                                              ])),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ]),
                                      //SWITCH 6 - DESVIO SI NO HAY CONEXION
                                      Row(
                                        children: <Widget>[
                                          Flexible(
                                            flex: 1,
                                            child: Switch(
                                              activeColor: lightBlueAthenea,
                                              onChanged: (bool value) {
                                                setState(() => this.switchVal6 = value);
                                              },
                                              value: this.switchVal6,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 2 * SizeConfig.heightMultiplier,
                                          ),
                                          Flexible(
                                            flex: 2,
                                            child: Text(
                                              'Desvío si no hay conexión',
                                              style: TextStyle(color: darkBlueAthenea, fontSize: 1.8 * SizeConfig.heightMultiplier),
                                            ),
                                          )
                                        ],
                                      ),
                                      this.switchVal6 == false
                                          ? Container()
                                          : Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                  ),
                                                  child: Text('Teléfono',
                                                      style:
                                                          TextStyle(color: darkBlueAthenea, fontSize: 1.8 * SizeConfig.heightMultiplier)),
                                                ),
                                                Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                                  Padding(
                                                    padding: EdgeInsets.fromLTRB(
                                                      0.5 * SizeConfig.heightMultiplier,
                                                      0.5 * SizeConfig.heightMultiplier,
                                                      0.5 * SizeConfig.heightMultiplier,
                                                      0.5 * SizeConfig.heightMultiplier,
                                                    ),
                                                    child: CountryCodePicker(
                                                      textStyle:
                                                          TextStyle(color: darkBlueAthenea, fontSize: 1.8 * SizeConfig.heightMultiplier),
                                                      // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                                                      initialSelection: 'ES',
                                                      favorite: ['+34', 'ES'],
                                                      // optional. Shows only country name and flag
                                                      showCountryOnly: true,
                                                      // optional. Shows only country name and flag when popup is closed.
                                                      showOnlyCountryWhenClosed: false,
                                                      // optional. aligns the flag and the Text left
                                                      alignLeft: false,
                                                      onChanged: (value) {
                                                        setState(() {
                                                          desvioSinConexionCodeCountry = value;
                                                        });
                                                      },
                                                    ),
                                                  ),
                                                  SizedBox(width: 1 * SizeConfig.heightMultiplier),
                                                  Expanded(
                                                    flex: 2,
                                                    child: Padding(
                                                      padding: EdgeInsets.fromLTRB(
                                                        0 * SizeConfig.heightMultiplier,
                                                        0 * SizeConfig.heightMultiplier,
                                                        15 * SizeConfig.heightMultiplier,
                                                        0 * SizeConfig.heightMultiplier,
                                                      ),
                                                      child: FormBuilderTextField(
                                                          decoration: InputDecoration(
                                                            border: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                                                            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderSide: BorderSide(color: blueAthenea),
                                                                borderRadius: BorderRadius.circular(3.0)),
                                                          ),
                                                          controller: TextEditingController(text: desvioSinConexionPhone),
                                                          keyboardType: TextInputType.phone,
                                                          name: "Teléfono",
                                                          style: TextStyle(
                                                            color: darkBlueAthenea,
                                                            fontSize: 1.8 * SizeConfig.heightMultiplier,
                                                          ),
                                                          onChanged: (value) {
                                                            /* setState(() { */
                                                            desvioSinConexionPhone = value;
                                                            /*  }); */
                                                          },
                                                          valueTransformer: (text) => num.tryParse(text),
                                                          validator: FormBuilderValidators.compose([
                                                            FormBuilderValidators.required(context, errorText: 'Debe rellenar este campo'),
                                                            FormBuilderValidators.numeric(context, errorText: 'Sólo se admiten números'),
                                                            FormBuilderValidators.min(context, 600000000,
                                                                errorText: 'El número introducido no es válido'),
                                                            FormBuilderValidators.max(context, 799999999,
                                                                errorText: 'El número introducido no es válido'),
                                                          ])),
                                                    ),
                                                  )
                                                ]),
                                              ],
                                            ),
                                      //SWITCH 7 - INTERNET
                                      Row(
                                        children: <Widget>[
                                          Switch(
                                            activeColor: lightBlueAthenea,
                                            onChanged: (bool value) {
                                              setState(() => this.switchVal7 = value);
                                            },
                                            value: this.switchVal7,
                                          ),
                                          SizedBox(
                                            width: 2 * SizeConfig.heightMultiplier,
                                          ),
                                          Text(
                                            'Internet',
                                            style: TextStyle(color: darkBlueAthenea, fontSize: 1.8 * SizeConfig.heightMultiplier),
                                          )
                                        ],
                                      ),
                                      //SWITCH 8 - INTERNET 4G
                                      Row(
                                        children: <Widget>[
                                          Switch(
                                            activeColor: lightBlueAthenea,
                                            onChanged: (bool value) {
                                              setState(() => this.switchVal8 = value);
                                            },
                                            value: this.switchVal8,
                                          ),
                                          SizedBox(
                                            width: 2 * SizeConfig.heightMultiplier,
                                          ),
                                          Text(
                                            'Internet 4G',
                                            style: TextStyle(color: darkBlueAthenea, fontSize: 1.8 * SizeConfig.heightMultiplier),
                                          )
                                        ],
                                      ),
                                      //SWITCH 9 - LLAMADA EN ESPERA
                                      Row(
                                        children: <Widget>[
                                          Switch(
                                            activeColor: lightBlueAthenea,
                                            onChanged: (bool value) {
                                              setState(() => this.switchVal9 = value);
                                            },
                                            value: this.switchVal9,
                                          ),
                                          SizedBox(
                                            width: 2 * SizeConfig.heightMultiplier,
                                          ),
                                          Text('Llamada en espera',
                                              style: TextStyle(
                                                color: darkBlueAthenea,
                                                fontSize: 1.8 * SizeConfig.heightMultiplier,
                                              ))
                                        ],
                                      ),
                                      //SWITCH 10 - PERMITIR LLAMADAS ENTRANTES
                                      Row(
                                        children: <Widget>[
                                          Flexible(
                                            flex: 1,
                                            child: Switch(
                                              activeColor: lightBlueAthenea,
                                              onChanged: (bool value) {
                                                setState(() => this.switchVal10 = value);
                                              },
                                              value: this.switchVal10,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 2 * SizeConfig.heightMultiplier,
                                          ),
                                          Flexible(
                                            flex: 4,
                                            child: Text(
                                              'Permitir llamadas entrantes',
                                              style: TextStyle(color: darkBlueAthenea, fontSize: 1.8 * SizeConfig.heightMultiplier),
                                            ),
                                          )
                                        ],
                                      ),
                                      //SWITCH 11 - PERMITIR SMS ENTRANTES
                                      Row(
                                        children: <Widget>[
                                          Switch(
                                            activeColor: lightBlueAthenea,
                                            onChanged: (bool value) {
                                              setState(() => this.switchVal11 = value);
                                            },
                                            value: this.switchVal11,
                                          ),
                                          SizedBox(
                                            width: 2 * SizeConfig.heightMultiplier,
                                          ),
                                          Text(
                                            'Permitir SMS entrantes',
                                            style: TextStyle(color: darkBlueAthenea, fontSize: 1.8 * SizeConfig.heightMultiplier),
                                          )
                                        ],
                                      ),
                                      //SWITCH 12 - PERMITIR SMS SALIENTES
                                      Row(
                                        children: <Widget>[
                                          Switch(
                                            activeColor: lightBlueAthenea,
                                            onChanged: (bool value) {
                                              setState(() => this.switchVal12 = value);
                                            },
                                            value: this.switchVal12,
                                          ),
                                          SizedBox(
                                            width: 2 * SizeConfig.heightMultiplier,
                                          ),
                                          Text(
                                            'Permitir SMS salientes',
                                            style: TextStyle(color: darkBlueAthenea, fontSize: 1.8 * SizeConfig.heightMultiplier),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 0.5 * SizeConfig.heightMultiplier,
                                      ),
                                      RaisedButton(
                                        color: yellowAthenea,
                                        shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                                        onPressed: () {
                                          if (_formKey.currentState.validate()) {
                                            guardarConfiguracion();
                                          } else {
                                            _avisofaltandatos();
                                          }
                                        },
                                        child: Text(
                                          'Guardar',
                                          style: TextStyle(fontSize: 1.8 * SizeConfig.heightMultiplier, color: whiteAthenea),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 2 * SizeConfig.heightMultiplier,
                                      ),
                                    ],
                                  ),
                                ),
                              )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      );
    });
  }

//METODOS
  Future<List<Feature>> _leerFeatures() async {
    pr.show();

    print(dropdownValue);
    Xml2Json xml2json = new Xml2Json();

    try {
      String url = 'http://212.225.255.130:8010/ws/uvfeaarcli/ip/token/0/$dropdownValue';
      http.Response resp = await http.get(url);
      xml2json.parse(resp.body);
      String xmldata = xml2json.toGData();
      final jsondata = json.decode(xmldata);
      final features = new Features.fromJsonList(jsondata['result']['linea']);
      List<Feature> items = features.items;
      await pr.hide();

      String s1 = items[0].estado;
      print('S1= $s1');
      String s2 = items[1].estado;
      print('S2= $s2');
      String s3 = items[2].estado;
      print('S3= $s3');

      if (items[2].estado == 'S') {
        String s3Parm1 = items[2].parm1;
        print('S3 Parm1: $s3Parm1');
        String parm1 = '${items[2].parm1}';
        String parm1S3 = parm1.replaceAll(' ', '');
        int cantidad = parm1S3.length;
        desvioCondicionalCodeCountry = parm1S3.substring(0, cantidad - 9);
        desvioCondicionalPhone = cantidad == 11
            ? parm1S3.substring(2)
            : cantidad == 12
                ? parm1S3.substring(3)
                : parm1S3.substring(4);

        print('S3 Code: $desvioCondicionalCodeCountry');
        print('S3 Phone: $desvioCondicionalPhone');
      }

      String s4 = items[3].estado;
      print('S4= $s4');

      if (items[3].estado == 'S') {
        String s4Parm1 = items[3].parm1;
        print('S4 Parm1: $s4Parm1');
        String parm1 = '${items[3].parm1}';
        String parm2 = '${items[3].parm2}';
        String parm1S4 = parm1.replaceAll(" ", "");
        String parm2S4 = parm2.replaceAll(" ", "");

        int cantidad = parm1S4.length;
        desvioAusenciaCodeCountry = parm1S4.substring(0, cantidad - 9);
        desvioAusenciaPhone = cantidad == 11
            ? parm1S4.substring(2)
            : cantidad == 12
                ? parm1S4.substring(3)
                : parm1S4.substring(4);
        desvioAusenciaTiempo = parm2S4;
        print('S4 Code: $desvioAusenciaCodeCountry');
        print('S4 Phone: $desvioAusenciaPhone');
        print('S4 Parm2: ${items[3].parm2}');
      }

      String s5 = items[4].estado;
      print('S5= $s5');

      if (items[4].estado == 'S') {
        String s5Parm1 = items[4].parm1;
        print('S5 Parm1: $s5Parm1');
        String parm1 = '${items[4].parm1}';
        String parm1S5 = parm1.replaceAll(" ", "");
        int cantidad = parm1S5.length;
        desvioOcupadoCodeCountry = parm1S5.substring(0, cantidad - 9);
        desvioOcupadoPhone = cantidad == 11
            ? parm1S5.substring(2)
            : cantidad == 12
                ? parm1S5.substring(3)
                : parm1S5.substring(4);

        print('S5 Code: $desvioOcupadoCodeCountry');
        print('S5 Phone: $desvioOcupadoPhone');
      }

      String s6 = items[5].estado;
      print('S6= $s6');

      if (items[5].estado == 'S') {
        String s6Parm1 = items[5].parm1;
        print('S6 Parm1: $s6Parm1');
        String parm1 = '${items[5].parm1}';
        String parm1S6 = parm1.replaceAll(" ", "");
        int cantidad = parm1S6.length;
        desvioSinConexionCodeCountry = parm1S6.substring(0, cantidad - 9);
        desvioSinConexionPhone = cantidad == 11
            ? parm1S6.substring(2)
            : cantidad == 12
                ? parm1S6.substring(3)
                : parm1S6.substring(4);

        print('S6 Code: $desvioSinConexionCodeCountry');
        print('S6 Phone: $desvioSinConexionPhone');
      }

      String s7 = items[6].estado;
      print('S7= $s7');
      String s8 = items[7].estado;
      print('S8= $s8');
      String s9 = items[8].estado;
      print('S9= $s9');
      String s10 = items[10].estado;
      print('S10= $s10');
      String s11 = items[14].estado;
      print('S11= $s11');
      String s12 = items[15].estado;
      print('S12= $s12');

      setState(() {
        if (items[0].estado == 'S') {
          switchVal1 = true;
        }
        if (items[1].estado == 'S') {
          switchVal2 = true;
        }
        if (items[2].estado == 'S') {
          switchVal1 = false;
          switchVal4 = false;
          switchVal5 = false;
          switchVal6 = false;
          switchVal3 = true;
          String desvCondPho = items[2].parm1.substring(2);
          desvioCondicionalPhone = desvCondPho.replaceAll(" ", "");
        } else {
          switchVal3 = false;
        }
        if (items[3].estado == 'S') {
          switchVal4 = true;
          String desvAusPho = items[3].parm1.substring(2);
          desvioAusenciaPhone = desvAusPho.replaceAll(" ", "");
          String desvAusTie = items[3].parm2;
          desvioAusenciaTiempo = desvAusTie.replaceAll(" ", "");
        }
        if (items[4].estado == 'S') {
          switchVal5 = true;
          desvioOcupadoPhone = items[4].parm1.substring(2);
        }
        if (items[5].estado == 'S') {
          switchVal6 = true;
          desvioSinConexionPhone = items[5].parm1.substring(2);
        }
        if (items[6].estado == 'S') {
          switchVal7 = true;
        }
        if (items[7].estado == 'S') {
          switchVal8 = true;
        }
        if (items[8].estado == 'S') {
          switchVal9 = true;
        }
        if (items[10].estado == 'S') {
          switchVal10 = true;
        }
        if (items[14].estado == 'S') {
          switchVal11 = true;
        }
        if (items[15].estado == 'S') {
          switchVal12 = true;
        }

        var estadosFeatures = Provider.of<EstadosFeaturesProvider>(context);
        bool s1valueProvider;
        s1 == 'S' ? s1valueProvider = true : s1valueProvider = false;
        bool s2valueProvider;
        s2 == 'S' ? s2valueProvider = true : s2valueProvider = false;
        bool s3valueProvider;
        s3 == 'S' ? s3valueProvider = true : s3valueProvider = false;
        bool s4valueProvider;
        s4 == 'S' ? s4valueProvider = true : s4valueProvider = false;
        bool s5valueProvider;
        s5 == 'S' ? s5valueProvider = true : s5valueProvider = false;
        bool s6valueProvider;
        s6 == 'S' ? s6valueProvider = true : s6valueProvider = false;
        bool s7valueProvider;
        s7 == 'S' ? s7valueProvider = true : s7valueProvider = false;
        bool s8valueProvider;
        s8 == 'S' ? s8valueProvider = true : s8valueProvider = false;
        bool s9valueProvider;
        s9 == 'S' ? s9valueProvider = true : s9valueProvider = false;
        bool s10valueProvider;
        s10 == 'S' ? s10valueProvider = true : s10valueProvider = false;
        bool s11valueProvider;
        s11 == 'S' ? s11valueProvider = true : s11valueProvider = false;
        bool s12valueProvider;
        s12 == 'S' ? s12valueProvider = true : s12valueProvider = false;

        estadosFeatures.s1 = s1valueProvider;
        estadosFeatures.s2 = s2valueProvider;
        estadosFeatures.s3 = s3valueProvider;
        estadosFeatures.s3parm1 = desvioCondicionalPhone;
        estadosFeatures.s4 = s4valueProvider;
        estadosFeatures.s4parm1 = desvioAusenciaPhone;
        estadosFeatures.s4parm2 = items[3].parm2;
        estadosFeatures.s5 = s5valueProvider;
        estadosFeatures.s5parm1 = items[4].parm1.substring(2);
        estadosFeatures.s6 = s6valueProvider;
        estadosFeatures.s6parm1 = items[5].parm1.substring(2);
        estadosFeatures.s7 = s7valueProvider;
        estadosFeatures.s8 = s8valueProvider;
        estadosFeatures.s9 = s9valueProvider;
        estadosFeatures.s10 = s10valueProvider;
        estadosFeatures.s11 = s11valueProvider;
        estadosFeatures.s12 = s12valueProvider;
      });

      return features.items;
    }
    //SI SALTA EL CATCH
    catch (e) {
      print('Error: $e');

      await pr.hide();
      showDialog(
          context: context,
          builder: (context) => new AlertDialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                content: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.error,
                      color: redAthenea,
                      size: 3 * SizeConfig.heightMultiplier,
                    ),
                    SizedBox(width: 2 * SizeConfig.heightMultiplier),
                    Expanded(
                      child: Text(
                        "Error de comunicación. Inténtelo más tarde o póngase en contacto con su operador.",
                        style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                      ),
                    ),
                  ],
                ),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Text('Ok',
                        style: TextStyle(
                          color: darkBlueAthenea,
                          fontSize: 2 * SizeConfig.heightMultiplier,
                        )),
                  ),
                ],
              ));
    }
    return [];
  }

  void guardarConfiguracion() async {
    pr.update(message: 'Realizando Operación...');
    pr.show();
    var estadosFeatures = Provider.of<EstadosFeaturesProvider>(context);

    bool s1valueProvider = estadosFeatures.s1;
    bool s2valueProvider = estadosFeatures.s2;
    bool s3valueProvider = estadosFeatures.s3;
    desvioCondicionalPhone = desvioCondicionalPhone;
    bool s4valueProvider = estadosFeatures.s4;
    desvioAusenciaPhone = desvioAusenciaPhone;
    desvioAusenciaTiempo = desvioAusenciaTiempo;
    bool s5valueProvider = estadosFeatures.s5;
    desvioOcupadoPhone = estadosFeatures.s5parm1;
    bool s6valueProvider = estadosFeatures.s6;
    desvioSinConexionPhone = estadosFeatures.s6parm1;
    bool s7valueProvider = estadosFeatures.s7;
    bool s8valueProvider = estadosFeatures.s8;
    bool s9valueProvider = estadosFeatures.s9;
    bool s10valueProvider = estadosFeatures.s10;
    bool s11valueProvider = estadosFeatures.s11;
    bool s12valueProvider = estadosFeatures.s12;

    //VALIDAR DATOS DESVIO CONDICIONAL
    //VALIDAR CODIGO PAIS
    if (switchVal3 == true && desvioCondicionalCodeCountry == null) {
      setState(() {
        desvioCondicionalCodeCountry = '34';
      });
      //VALIDAR NUMERO DESVIO
    }
    if (switchVal3 == true && desvioCondicionalPhone == null) {
      _avisofaltaphone();
    }

    //VALIDAR DATOS DESVIO AUSENCIA
    //VALIDAR CODIGO PAIS
    if (switchVal4 == true && desvioAusenciaCodeCountry == null) {
      setState(() {
        desvioAusenciaCodeCountry = '34';
      });
      //VALIDAR NUMERO DESVIO
    }
    if (switchVal4 == true && desvioAusenciaPhone == null) {
      _avisofaltaphone();
    }
    //VALIDAR DATOS DESVIO POR OCUPADO
    //VALIDAR CODIGO PAIS
    if (switchVal5 == true && desvioOcupadoCodeCountry == null) {
      setState(() {
        desvioOcupadoCodeCountry = '34';
      });
      //VALIDAR NUMERO DESVIO
    }
    if (switchVal5 == true && desvioOcupadoPhone == null) {
      _avisofaltaphone();
    }

    //VALIDAR DATOS DESVIO NO HAY CONEXION
    //VALIDAR CODIGO PAIS
    if (switchVal6 == true && desvioSinConexionCodeCountry == null) {
      setState(() {
        desvioSinConexionCodeCountry = '34';
      });
      //VALIDAR NUMERO DESVIO
    }
    if (switchVal6 == true && desvioSinConexionPhone == null) {
      _avisofaltaphone();
    }

    //GRABAR BUZON DE VOZ

    if (s1valueProvider != switchVal1) {
      if (switchVal1 == true) {
        if (switchVal3 == true) {
          Xml2Json xml2json = new Xml2Json();
          try {
            String url =
                'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/CFU/N/$desvioCondicionalCodeCountry$desvioCondicionalPhone';

            http.Response resp = await http.get(url);
            xml2json.parse(resp.body);
            String xmldata = xml2json.toGData();
            final jsondata = json.decode(xmldata);
            print('Desconectar Desvío condicional: $jsondata');
          }
          //SI SALTA EL CATCH
          catch (e) {
            print('Error: $e');
            await pr.hide();
            showDialog(
                context: context,
                builder: (context) => new AlertDialog(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                      content: new Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            Icons.error,
                            color: redAthenea,
                            size: 3 * SizeConfig.heightMultiplier,
                          ),
                          SizedBox(width: 2 * SizeConfig.heightMultiplier),
                          Expanded(
                            child: Text(
                              "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                              style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                            ),
                          ),
                        ],
                      ),
                      actions: <Widget>[
                        new FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: new Text('Ok',
                              style: TextStyle(
                                color: darkBlueAthenea,
                                fontSize: 2 * SizeConfig.heightMultiplier,
                              )),
                        ),
                      ],
                    ));
          }
        }

        Xml2Json xml2json = new Xml2Json();
        try {
          String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/BUZON/S/34744602235';

          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Conectar Buzón de Vóz: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      } else {
        Xml2Json xml2json = new Xml2Json();
        try {
          String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/BUZON/N';

          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Desconectar Buzón de Vóz: $jsondata');

          setState(() {
            switchVal4 = false;
            switchVal5 = false;
            switchVal6 = false;
          });
        } catch (e) {
          //SI SALTA EL CATCH
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      }
    }
    //GRABAR CONFERENCIA TRIPARTITA
    if (s2valueProvider != switchVal2) {
      if (switchVal2 == true) {
        Xml2Json xml2json = new Xml2Json();
        try {
          String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/MPTY/S';
          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Conectar conferencia Tripartita: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      } else {
        Xml2Json xml2json = new Xml2Json();
        try {
          String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/MPTY/N';
          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Desconectar conferencia Tripartita: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      }
    }
    //GRABAR DESVIO CONDICIONAL
    if (s3valueProvider != switchVal3) {
      if (switchVal3 == true) {
        Xml2Json xml2json = new Xml2Json();
        try {
          if (switchVal1 == true) {
            Xml2Json xml2json = new Xml2Json();
            try {
              String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/BUZON/N';

              http.Response resp = await http.get(url);
              xml2json.parse(resp.body);
              String xmldata = xml2json.toGData();
              final jsondata = json.decode(xmldata);
              print('Desconectar Buzón Voz: $jsondata');
              setState(() {
                switchVal4 = false;
                switchVal5 = false;
                switchVal6 = false;
              });
            }
            //SI SALTA EL CATCH
            catch (e) {
              print('Error: $e');
              await pr.hide();
              showDialog(
                  context: context,
                  builder: (context) => new AlertDialog(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                        content: new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.error,
                              color: redAthenea,
                              size: 3 * SizeConfig.heightMultiplier,
                            ),
                            SizedBox(width: 2 * SizeConfig.heightMultiplier),
                            Expanded(
                              child: Text(
                                "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                                style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                              ),
                            ),
                          ],
                        ),
                        actions: <Widget>[
                          new FlatButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: new Text('Ok',
                                style: TextStyle(
                                  color: darkBlueAthenea,
                                  fontSize: 2 * SizeConfig.heightMultiplier,
                                )),
                          ),
                        ],
                      ));
            }
          }

          if (switchVal4 == true) {
            String urlAusencia = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/CFNRY/N';

            http.Response resp2 = await http.get(urlAusencia);
            xml2json.parse(resp2.body);
            String xmldata2 = xml2json.toGData();
            final jsondata2 = json.decode(xmldata2);
            print('Desconectar Buzón Ausencia: $jsondata2');
          }

          if (switchVal5 == true) {
            String urlOcupado = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/CFB/N';

            http.Response resp3 = await http.get(urlOcupado);
            xml2json.parse(resp3.body);
            String xmldata3 = xml2json.toGData();
            final jsondata3 = json.decode(xmldata3);
            print('Desconectar Buzón Ocupado: $jsondata3');
          }

          if (switchVal6 == true) {
            String urlSinConexion = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/CFNRC/N';

            http.Response resp4 = await http.get(urlSinConexion);
            xml2json.parse(resp4.body);
            String xmldata4 = xml2json.toGData();
            final jsondata4 = json.decode(xmldata4);
            print('Desconectar Buzón Ocupado: $jsondata4');
          }

          print(desvioCondicionalCodeCountry);
          print(desvioCondicionalPhone);

          String url =
              'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/CFU/S/$desvioCondicionalCodeCountry$desvioCondicionalPhone';

          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Conectar Desvío condicional: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      } else {
        Xml2Json xml2json = new Xml2Json();
        try {
          String url =
              'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/CFU/N/$desvioCondicionalCodeCountry$desvioCondicionalPhone';

          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Desconectar Desvío condicional: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      }
    }

    //GRABAR DESVIO AUSENCIA
    //|| estadosFeatures.s4parm2 != desvioAusenciaTiempo || estadosFeatures.s4parm1 != desvioAusenciaPhone
    if (s4valueProvider != switchVal4) {
      Xml2Json xml2json = new Xml2Json();
      try {
        if (switchVal4 == true) {
          String urlAusencia =
              'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/CFNRY/S/$desvioAusenciaCodeCountry$desvioAusenciaPhone/$desvioAusenciaTiempo';

          http.Response resp2 = await http.get(urlAusencia);
          xml2json.parse(resp2.body);
          String xmldata2 = xml2json.toGData();
          final jsondata2 = json.decode(xmldata2);
          print('Conectar Desvío Ausencia: $jsondata2');
        } else if (switchVal4 == false) {
          String urlAusencia = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/CFNRY/N';

          http.Response resp2 = await http.get(urlAusencia);
          xml2json.parse(resp2.body);
          String xmldata2 = xml2json.toGData();
          final jsondata2 = json.decode(xmldata2);
          print('Desconectar Desvío Ausencia: $jsondata2');
        }
      }
      //SI SALTA EL CATCH
      catch (e) {
        print('Error: $e');
        await pr.hide();
        showDialog(
            context: context,
            builder: (context) => new AlertDialog(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                  content: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        Icons.error,
                        color: redAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      SizedBox(width: 2 * SizeConfig.heightMultiplier),
                      Expanded(
                        child: Text(
                          "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                          style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                        ),
                      ),
                    ],
                  ),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: new Text('Ok',
                          style: TextStyle(
                            color: darkBlueAthenea,
                            fontSize: 2 * SizeConfig.heightMultiplier,
                          )),
                    ),
                  ],
                ));
      }
    } else if (estadosFeatures.s4parm2 != desvioAusenciaTiempo || estadosFeatures.s4parm1 != desvioAusenciaPhone) {
      Xml2Json xml2json = new Xml2Json();
      try {
        String urlAusencia =
            'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/CFNRY/S/$desvioAusenciaCodeCountry$desvioAusenciaPhone/$desvioAusenciaTiempo';

        http.Response resp2 = await http.get(urlAusencia);
        xml2json.parse(resp2.body);
        String xmldata2 = xml2json.toGData();
        final jsondata2 = json.decode(xmldata2);
        print('Conectar Desvío Ausencia: $jsondata2');
      }
      //SI SALTA EL CATCH
      catch (e) {
        print('Error: $e');
        await pr.hide();
        showDialog(
            context: context,
            builder: (context) => new AlertDialog(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                  content: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        Icons.error,
                        color: redAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      SizedBox(width: 2 * SizeConfig.heightMultiplier),
                      Expanded(
                        child: Text(
                          "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                          style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                        ),
                      ),
                    ],
                  ),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: new Text('Ok',
                          style: TextStyle(
                            color: darkBlueAthenea,
                            fontSize: 2 * SizeConfig.heightMultiplier,
                          )),
                    ),
                  ],
                ));
      }
    }

    //GRABAR DESVIO POR OCUPADO
    if (s5valueProvider != switchVal5 || estadosFeatures.s5parm1 != desvioOcupadoPhone) {
      Xml2Json xml2json = new Xml2Json();
      try {
        if (switchVal5 == true) {
          String urlOcupado =
              'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/CFB/S/$desvioOcupadoCodeCountry$desvioOcupadoPhone';

          http.Response resp2 = await http.get(urlOcupado);
          xml2json.parse(resp2.body);
          String xmldata2 = xml2json.toGData();
          final jsondata2 = json.decode(xmldata2);
          print('Conectar Desvío Ocupado: $jsondata2');
        } else {
          String urlOcupado = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/CFB/N';

          http.Response resp3 = await http.get(urlOcupado);
          xml2json.parse(resp3.body);
          String xmldata3 = xml2json.toGData();
          final jsondata3 = json.decode(xmldata3);
          print('Desconectar Desvío Ocupado: $jsondata3');
        }
      }
      //SI SALTA EL CATCH
      catch (e) {
        print('Error: $e');
        await pr.hide();
        showDialog(
            context: context,
            builder: (context) => new AlertDialog(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                  content: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        Icons.error,
                        color: redAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      SizedBox(width: 2 * SizeConfig.heightMultiplier),
                      Expanded(
                        child: Text(
                          "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                          style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                        ),
                      ),
                    ],
                  ),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: new Text('Ok',
                          style: TextStyle(
                            color: darkBlueAthenea,
                            fontSize: 2 * SizeConfig.heightMultiplier,
                          )),
                    ),
                  ],
                ));
      }
    }

    //GRABAR DESVIO SI NO HAY CONEXION
    if (s6valueProvider != switchVal6 || estadosFeatures.s6parm1 != desvioSinConexionPhone) {
      Xml2Json xml2json = new Xml2Json();
      try {
        if (switchVal6 == true) {
          String urlSinConexion =
              'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/CFNRC/S/$desvioSinConexionCodeCountry$desvioSinConexionPhone';

          http.Response resp2 = await http.get(urlSinConexion);
          xml2json.parse(resp2.body);
          String xmldata2 = xml2json.toGData();
          final jsondata2 = json.decode(xmldata2);
          print('Conectar Desvío sin Conexión: $jsondata2');
        } else {
          String urlSinConexion = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/CFNRC/N';

          http.Response resp3 = await http.get(urlSinConexion);
          xml2json.parse(resp3.body);
          String xmldata3 = xml2json.toGData();
          final jsondata3 = json.decode(xmldata3);
          print('Desconectar Desvío sin Conexión: $jsondata3');
        }
      }
      //SI SALTA EL CATCH
      catch (e) {
        print('Error: $e');
        await pr.hide();
        showDialog(
            context: context,
            builder: (context) => new AlertDialog(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                  content: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        Icons.error,
                        color: redAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      SizedBox(width: 2 * SizeConfig.heightMultiplier),
                      Expanded(
                        child: Text(
                          "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                          style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                        ),
                      ),
                    ],
                  ),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: new Text('Ok',
                          style: TextStyle(
                            color: darkBlueAthenea,
                            fontSize: 2 * SizeConfig.heightMultiplier,
                          )),
                    ),
                  ],
                ));
      }
    }

    //GRABAR INTERNET
    if (s7valueProvider != switchVal7) {
      if (switchVal7 == true) {
        Xml2Json xml2json = new Xml2Json();
        try {
          String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/NAM/S/BOTH';

          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Conectar Internet: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      } else {
        Xml2Json xml2json = new Xml2Json();
        try {
          String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/NAM/N/MSC';

          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Desconectar Internet: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      }
    }

    //GRABAR INTERNET 4G
    if (s8valueProvider != switchVal8) {
      if (switchVal8 == true) {
        Xml2Json xml2json = new Xml2Json();
        try {
          String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/EPS/S';

          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Conectar Internet 4G: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      } else {
        Xml2Json xml2json = new Xml2Json();
        try {
          String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/EPS/N';

          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Desconectar Internet 4G: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      }
    }

    //GRABAR LLAMADA EN ESPERA
    if (s9valueProvider != switchVal9) {
      if (switchVal9 == true) {
        Xml2Json xml2json = new Xml2Json();
        try {
          String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/CW/S';

          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Conectar Llamada en Espera: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      } else {
        Xml2Json xml2json = new Xml2Json();
        try {
          String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/CW/N';

          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Desconectar Llamada en Espera: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      }
    }

    //GRABAR PERMITIR LLAMADAS ENTRANTES
    if (s10valueProvider != switchVal10) {
      if (switchVal10 == true) {
        Xml2Json xml2json = new Xml2Json();
        try {
          String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/ODBIC/S/NOBIC';

          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Conectar Llamadas Entrantes: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      } else {
        Xml2Json xml2json = new Xml2Json();
        try {
          String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/ODBIC/N';

          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Desconectar Llamadas Entrantes: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      }
    }

    //GRABAR PERMITIR SMS ENTRANTES
    if (s11valueProvider != switchVal11) {
      if (switchVal11 == true) {
        Xml2Json xml2json = new Xml2Json();
        try {
          String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/SMSMT/S';

          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Conectar Permitir SMS entrantes: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      } else {
        Xml2Json xml2json = new Xml2Json();
        try {
          String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/SMSMT/N';

          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Desconectar Permitir SMS entrantes: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      }
    }

//GRABAR PERMITIR SMS SALIENTES
    if (s12valueProvider != switchVal12) {
      if (switchVal12 == true) {
        Xml2Json xml2json = new Xml2Json();
        try {
          String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/SMSMO/S';

          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Conectar Permitir SMS Salientes: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      } else {
        Xml2Json xml2json = new Xml2Json();
        try {
          String url = 'http://212.225.255.130:8010/ws/uvcopehw/ip/token/SASE/$dropdownValue/SMSMO/N';

          http.Response resp = await http.get(url);
          xml2json.parse(resp.body);
          String xmldata = xml2json.toGData();
          final jsondata = json.decode(xmldata);
          print('Desconectar Permitir SMS Salientes: $jsondata');
        }
        //SI SALTA EL CATCH
        catch (e) {
          print('Error: $e');
          await pr.hide();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error en la operación. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: darkBlueAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      }
    }

    try {
      pr.update(message: 'Leyendo Configuración...');
    } catch (e) {}
    _leerFeatures();
  }

  void _avisofaltandatos() {
    showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
              content: new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    Icons.error,
                    color: redAthenea,
                    size: 3 * SizeConfig.heightMultiplier,
                  ),
                  SizedBox(width: 2 * SizeConfig.heightMultiplier),
                  Expanded(
                    child: Text(
                      "Datos incompletos. Hay datos sin rellenar.",
                      style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                    ),
                  ),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text('Ok',
                      style: TextStyle(
                        color: darkBlueAthenea,
                        fontSize: 2 * SizeConfig.heightMultiplier,
                      )),
                ),
              ],
            ));
  }

  void _avisofaltaphone() {
    showDialog(
        context: context,
        builder: (context) => new AlertDialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
              content: new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    Icons.error,
                    color: redAthenea,
                    size: 3 * SizeConfig.heightMultiplier,
                  ),
                  SizedBox(width: 2 * SizeConfig.heightMultiplier),
                  Expanded(
                    child: Text(
                      "Datos incompletos. Ha seleccionado Desvío condicional sin indicar un número al que desviar las llamadas.",
                      style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                    ),
                  ),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text('Ok',
                      style: TextStyle(
                        color: darkBlueAthenea,
                        fontSize: 2 * SizeConfig.heightMultiplier,
                      )),
                ),
              ],
            ));
  }
}
