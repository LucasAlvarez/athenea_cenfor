import 'package:comunicacionesatheneacenfor/constants.dart';
import 'package:comunicacionesatheneacenfor/models/linea.dart';
import 'package:comunicacionesatheneacenfor/providers/consumo_provider.dart';
import 'package:comunicacionesatheneacenfor/providers/dropdownvalue_provider.dart';
import 'package:comunicacionesatheneacenfor/providers/lineas_cliente_provider.dart';
import 'package:comunicacionesatheneacenfor/widgets/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:xml2json/xml2json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:xml/xml.dart' as xml;

class ConsumoPage extends StatefulWidget {
  ConsumoPage({Key key}) : super(key: key);

  @override
  State<ConsumoPage> createState() => _ConsumoPageState();
}

class _ConsumoPageState extends State<ConsumoPage> {
  //VARIABLES QUE VAMOS A UTILIZAR
  ProgressDialog pr;
  String dropdownValue = 'Elige un teléfono';
  double datoRoscoLlamadas;
  String excesoMin;
  bool flagIsData = false;
  bool flagIsDataIlimitada = false;

  @override
  void initState() {
    super.initState();
  }

  //METODO PARA CARGAR DATOS DE LA LINEA
  Future leerDatosTelefono() async {
    setState(() {
      var consumoProvider = Provider.of<ConsumoProvider>(context);
      consumoProvider.tarifaMovil = null;
      consumoProvider.tipollamada = null;
      consumoProvider.consumoMovil = null;
      consumoProvider.tipoRed = null;
      consumoProvider.bonoMovil = null;
      consumoProvider.bonoR = null;
      consumoProvider.balanceR = null;
      consumoProvider.teQuedan = null;

      var dropdownvalueProvider = Provider.of<DropDownValueProvider>(context);

      dropdownvalueProvider.dropDownValue = dropdownValue;
    });
    Xml2Json xml2json = new Xml2Json();
    try {
      String urltarifa = 'http://212.225.255.130:8010/ws/devtarifa/$dropdownValue';
      http.Response resptarifa = await http.get(urltarifa);
      await pr.hide();

      xml2json.parse(resptarifa.body);
      String xmldata = xml2json.toGData();
      Map<String, dynamic> data = convert.jsonDecode(xmldata);
      String tarifa = data['result']['Registro']['Tarifa'];
      /* print("Tarifas para ver pruebas: $tarifa"); */

      if (tarifa.substring(0, 4) == 'DEIO') {
        if (tarifa.contains('DEION_BASE') == true) {
          getDeionBase2();
        } else if (tarifa.contains('DEION') == true && tarifa != 'DEION_BASE') {
          getDeion();
        }
        //NUEVO 4/2/22 -> AÑADIDO OPCIONES DE TERNARIO EN BASE CON EL NOMBRE DE LAS TARIFAS SIGUIENDO EL MODELO DEL CODIGO YA HECHO
      } else if (tarifa.contains('DIS') == true ||
          tarifa.contains('SHEN') == true ||
          tarifa.contains('PEK') == true ||
          tarifa.contains('SHAN') == true ||
          tarifa.contains('CHON') == true ||
          tarifa.contains('NANK') == true ||
          tarifa.contains('FAM') == true) {
        getDis();
      } else if (tarifa.contains('MOVISUR') == true) {
        if (tarifa.contains('MOVISUR_SUR17_Base') == true) {
          getMovisurBase();
        } else if (tarifa == 'MOVISUR') {
          getMovisur();
        }
      } else if (tarifa.contains('COSTE') == true) {
        setState(() {
          var consumoProvider = Provider.of<ConsumoProvider>(context);
          consumoProvider.tarifaMovil = 'COSTE';
        });
      }
    }
    //SI SALTA EL CATCH
    catch (e) {
      pr.show();
      print('Error: $e');
      await pr.hide();
      showDialog(
          context: context,
          builder: (context) => new AlertDialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                content: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.error,
                      color: redAthenea,
                      size: 3 * SizeConfig.heightMultiplier,
                    ),
                    SizedBox(width: 2 * SizeConfig.heightMultiplier),
                    Expanded(
                      child: Text(
                        "Error de comunicación. Inténtelo más tarde o póngase en contacto con su operador.",
                        style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                      ),
                    ),
                  ],
                ),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Text('Ok',
                        style: TextStyle(
                          color: yellowAthenea,
                          fontSize: 2 * SizeConfig.heightMultiplier,
                        )),
                  ),
                ],
              ));
    }
    return [];
  }

  //METODO PARA CARGAR INTERNET DE LA LINEA
  Future leerDatosInternet() async {
    setState(() {
      var consumoProvider = Provider.of<ConsumoProvider>(context);

      consumoProvider.bonoDatos = null;
      consumoProvider.balanceDatos = null;
      consumoProvider.consumoDatos = null;
      consumoProvider.masMegasDatos = null;
      consumoProvider.bonoR = null;
      consumoProvider.balanceR = null;
      consumoProvider.teQuedan = null;
    });

    Xml2Json xml2json = new Xml2Json();
    try {
      String url = 'http://212.225.255.130:8010/ws/rtvbonlin/$dropdownValue';
      http.Response resp = await http.get(url);
      await pr.hide();

      xml2json.parse(resp.body);
      String xmldata = xml2json.toGData();
      Map<String, dynamic> data = convert.jsonDecode(xmldata);
      String bonoDatos = data['result']['Registro']['Bono'];
      String balanceDatos = data['result']['Registro']['Balance'];
      String consumoDatos = data['result']['Registro']['Consumo'];
      String masMegasDatos = data['result']['Registro']['MasMegas'];
      String bonoR = data['result']['Registro']['Bonor'];
      String balanceR = data['result']['Registro']['Balancer'];

      double bonoDatosNumero = double.parse(bonoDatos);
      double balanceDatosNumero = double.parse(balanceDatos);
      double consumoDatosNumero = double.parse(consumoDatos);
      double masMegasDatosNumero = double.parse(masMegasDatos);
      double bonoRNumero = double.parse(bonoR);
      double balanceRNumero = double.parse(balanceR);

      setState(() {
        var consumoProvider = Provider.of<ConsumoProvider>(context);

        consumoProvider.bonoDatos = (bonoDatosNumero / 1000000);
        consumoProvider.balanceDatos = (balanceDatosNumero / 1000000);
        consumoProvider.consumoDatos = (consumoDatosNumero / 1000000);
        consumoProvider.masMegasDatos = masMegasDatosNumero / 1000000;
        consumoProvider.bonoR = bonoRNumero / 1000000;
        consumoProvider.balanceR = balanceRNumero / 1000000;
        consumoProvider.teQuedan = consumoProvider.bonoR - consumoProvider.balanceR;
      });
    }
    //SI SALTA EL CATCH
    catch (e) {
      pr.show();
      print('Error: $e');
      await pr.hide();
      showDialog(
          context: context,
          builder: (context) => new AlertDialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                content: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.error,
                      color: redAthenea,
                      size: 3 * SizeConfig.heightMultiplier,
                    ),
                    SizedBox(width: 2 * SizeConfig.heightMultiplier),
                    Expanded(
                      child: Text(
                        "Error de comunicación. Inténtelo más tarde o póngase en contacto con su operador.",
                        style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                      ),
                    ),
                  ],
                ),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Text('Ok',
                        style: TextStyle(
                          color: yellowAthenea,
                          fontSize: 2 * SizeConfig.heightMultiplier,
                        )),
                  ),
                ],
              ));
    }
    return [];
  }

  final GlobalKey<AnimatedCircularChartState> _chartKey1 = new GlobalKey<AnimatedCircularChartState>();
  final GlobalKey<AnimatedCircularChartState> _chartKey2 = new GlobalKey<AnimatedCircularChartState>();

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    pr.style(message: 'Leyendo Consumo...');
    var lineasClienteProvider = Provider.of<LineasClienteProvider>(context);
    List<Linea> lineasCliente = lineasClienteProvider.lineasCliente;
    List<String> menuItems = [];

    //CARGAMOS LINEAS DEL CLIENTE
    for (var i = 0; i < lineasCliente.length; i++) {
      var numero = int.parse(lineasCliente[i].telefono);
      if (numero > 600000000 && numero < 799999999) {
        menuItems.add(lineasCliente[i].telefono);
      } else {}
    }
    final List<DropdownMenuItem<String>> _dropDownMenuItems = menuItems
        .map(
          (String value) => DropdownMenuItem<String>(
            value: value,
            child: Text(
              value,
            ),
          ),
        )
        .toList();

    var consumoProvider = Provider.of<ConsumoProvider>(context);
    print("${consumoProvider.toString()}");
    print("_____");

    double bonoDatos = consumoProvider.bonoDatos;
    double balanceDatos = consumoProvider.balanceDatos;
    double consumoDatos = consumoProvider.consumoDatos;
    double masMegasDatos = consumoProvider.masMegasDatos;
    double consumoMovil = consumoProvider.consumoMovil;
    String bonoMovil = consumoProvider.bonoMovil;
    String tarifaMovil = consumoProvider.tarifaMovil;
    double bonoR = consumoProvider.bonoR;
    double balanceR = consumoProvider.balanceR;
    double teQuedan = consumoProvider.teQuedan;

    if (bonoDatos != null && bonoDatos >= 250.0) {
      /* print("BonoDatos: $bonoDatos"); */
      flagIsData = true;
      /*   print("Flag para comprobar datos esta a: $flagIsData"); */
      consumoDatos = 0;
      balanceDatos = 100;
    } else {
      /*   print("BonoDatos null: $bonoDatos"); */
      flagIsDataIlimitada = false;
      /*  print("Flag para comprobar datos ilimitados esta a: $flagIsDataIlimitada"); */
    }

    //CIRCULO DE CARGA DE DATOS DE INTERNET
    List<CircularStackEntry> dataInternet = <CircularStackEntry>[
      new CircularStackEntry(<CircularSegmentEntry>[
        new CircularSegmentEntry(consumoDatos, lightBlueAthenea, rankKey: 'Q1'),
        new CircularSegmentEntry(balanceDatos, blueAthenea, rankKey: 'Q2'),
      ], rankKey: 'Quarterly Profits')
    ];
    //NUEVO 4/2/22 -> AÑADIDOS TERNARIOS PARA TARIFAS NUEVAS Y MODIFICADA CONDICION PARA QUE RECOJA LOS ILIMITADOS
    if (bonoMovil.toString().contains('BONOILIMITADO') || bonoMovil.toString().contains('ILIMITADO')) {
      datoRoscoLlamadas = 0;
    } else {
      datoRoscoLlamadas = consumoMovil;
    }

    //CIRCULO DE CARGA DE DATOS DE LLAMADAS
    List<CircularStackEntry> dataMovil = <CircularStackEntry>[
      new CircularStackEntry(<CircularSegmentEntry>[
        new CircularSegmentEntry(datoRoscoLlamadas, lightBlueAthenea, rankKey: 'Q1'),
        new CircularSegmentEntry(100, blueAthenea, rankKey: 'Q2'),
      ], rankKey: 'Quarterly Profits')
    ];
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(builder: (context, orientation) {
          SizeConfig().init(constraints, orientation);
          return Scaffold(
            appBar: AppBar(
              backgroundColor: blueAthenea,
              title: Text('Tu consumo',
                  style: TextStyle(
                    fontSize: 2 * SizeConfig.heightMultiplier,
                    color: whiteAthenea,
                  )),
              iconTheme: IconThemeData(
                color: yellowAthenea,
              ),
            ),
            body: Container(
              child: Stack(children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 1 * SizeConfig.heightMultiplier,
                      ),
                      Center(
                        child: Card(
                          elevation: 2.5,
                          color: whiteAthenea,
                          shadowColor: darkBlueAthenea,
                          child: Container(
                            width: 95 * SizeConfig.heightMultiplier,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(
                                1 * SizeConfig.heightMultiplier,
                                1 * SizeConfig.heightMultiplier,
                                1 * SizeConfig.heightMultiplier,
                                1 * SizeConfig.heightMultiplier,
                              ),
                              child: Column(
                                /*   runSpacing: 3 * SizeConfig.heightMultiplier,
                                alignment: WrapAlignment.center, */
                                children: <Widget>[
                                  Text(
                                    'Consulta el consumo del número seleccionado',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: darkBlueAthenea, fontSize: 2 * SizeConfig.heightMultiplier),
                                  ),
                                  SizedBox(
                                    height: 2 * SizeConfig.heightMultiplier,
                                  ),
                                  Container(
                                    width: SizeConfig.heightMultiplier * 25,
                                    decoration: BoxDecoration(
                                      border: Border.all(color: blueAthenea, width: 3.0),
                                      borderRadius: BorderRadius.circular(3.0),
                                    ),
                                    child: Center(
                                      child: DropdownButton<String>(
                                          isDense: true,
                                          value: null,
                                          hint: Text(
                                            dropdownValue,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(fontSize: 1.8 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                                          ),
                                          icon: Icon(Icons.keyboard_arrow_down, color: yellowAthenea),
                                          iconSize: 4 * SizeConfig.heightMultiplier,
                                          elevation: 16,
                                          style: TextStyle(
                                            color: darkBlueAthenea,
                                            fontSize: 1.8 * SizeConfig.heightMultiplier,
                                          ),
                                          underline: Container(
                                            height: 0,
                                          ),
                                          onChanged: (String value) {
                                            setState(() {
                                              excesoMin = null;
                                              dropdownValue = value;
                                              leerDatosTelefono();
                                              leerDatosInternet();
                                            });
                                          },
                                          items: _dropDownMenuItems),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 2 * SizeConfig.heightMultiplier,
                                  ),
                                  Text(
                                    'Consumo de la línea: $dropdownValue',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: darkBlueAthenea,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 1.8 * SizeConfig.heightMultiplier,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 0.5 * SizeConfig.heightMultiplier,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: dropdownValue == 'Elige un teléfono'
                            ? Container()
                            : SingleChildScrollView(
                                child: Container(
                                  width: 95 * SizeConfig.heightMultiplier,
                                  child: Column(
                                    children: <Widget>[
                                      // TITULOS COLUMNAS
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            //TITULO LLAMADAS
                                            child: Card(
                                              elevation: 2.5,
                                              shadowColor: darkBlueAthenea,
                                              color: blueAthenea,
                                              child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                      'LLAMADAS',
                                                      style: TextStyle(fontSize: 1.5 * SizeConfig.heightMultiplier, color: whiteAthenea),
                                                    ),
                                                  )),
                                            ),
                                          ),
                                          Expanded(
                                            //TITULO DATOS
                                            child: Card(
                                                color: blueAthenea,
                                                elevation: 2.5,
                                                shadowColor: darkBlueAthenea,
                                                child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                      'DATOS',
                                                      style: TextStyle(fontSize: 1.5 * SizeConfig.heightMultiplier, color: whiteAthenea),
                                                    ),
                                                  ),
                                                )),
                                          ),
                                        ],
                                      ),
                                      // ROSCOS AQUI
                                      Row(
                                        children: <Widget>[
                                          //ROSCO LLAMADAS
                                          Expanded(
                                            child: Card(
                                              elevation: 2.5,
                                              shadowColor: darkBlueAthenea,
                                              color: whiteAthenea,
                                              child: consumoMovil == null && tarifaMovil == null
                                                  ? Container(
                                                      /*     height: MediaQuery.of(context).size.width * 0.50 - 10.0, */
                                                      height: 35 * SizeConfig.heightMultiplier,
                                                      width: 35 * SizeConfig.heightMultiplier,
                                                      child: Padding(
                                                        padding: EdgeInsets.fromLTRB(
                                                          4 * SizeConfig.heightMultiplier,
                                                          7 * SizeConfig.heightMultiplier,
                                                          4 * SizeConfig.heightMultiplier,
                                                          7 * SizeConfig.heightMultiplier,
                                                        ),
                                                        child: CircularProgressIndicator(
                                                          valueColor: AlwaysStoppedAnimation<Color>(whiteAthenea),
                                                          backgroundColor: blueAthenea,
                                                        ),
                                                      ))
                                                  : tarifaMovil.contains('COSTE') == true
                                                      ? Container(
                                                          /*    height: MediaQuery.of(context).size.width * 0.50 - 10.0, */
                                                          height: 35 * SizeConfig.heightMultiplier,
                                                          child: Center(
                                                            child: Text(
                                                              'Vea su Consumo \n en la página \n detalle de llamadas',
                                                              style: TextStyle(
                                                                  fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                                                              textAlign: TextAlign.center,
                                                            ),
                                                          ),
                                                        )
                                                      : tarifaMovil.contains('DISNAVEGAMINI') == true
                                                          ? Container(
                                                              height: 35 * SizeConfig.heightMultiplier,
                                                              child: Center(
                                                                child: Text(
                                                                  'Tarifa sin \n Bono de Llamadas',
                                                                  style: TextStyle(
                                                                      fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                                                                  textAlign: TextAlign.center,
                                                                ),
                                                              ),
                                                            )
                                                          : Container(
                                                              /*  height: MediaQuery.of(context).size.width * 0.50 - 10.0, */
                                                              height: 35 * SizeConfig.heightMultiplier,
                                                              child: Padding(
                                                                padding: EdgeInsets.fromLTRB(
                                                                  1 * SizeConfig.heightMultiplier,
                                                                  1 * SizeConfig.heightMultiplier,
                                                                  1 * SizeConfig.heightMultiplier,
                                                                  1 * SizeConfig.heightMultiplier,
                                                                ),
                                                                child: Center(
                                                                  child: Stack(
                                                                    fit: StackFit.loose,
                                                                    children: <Widget>[
                                                                      Center(
                                                                        child: new AnimatedCircularChart(
                                                                          holeRadius: 20.0,
                                                                          key: _chartKey2,
                                                                          /* size: Size(250.0, 250.0), */
                                                                          size: Size(
                                                                            35 * SizeConfig.heightMultiplier,
                                                                            35 * SizeConfig.heightMultiplier,
                                                                          ),
                                                                          initialChartData: dataMovil,
                                                                          chartType: CircularChartType.Radial,
                                                                          labelStyle: new TextStyle(
                                                                            color: darkBlueAthenea,
                                                                            fontSize: 2 * SizeConfig.heightMultiplier,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                      _centroRoscoMovil(),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                            ),
                                          ),
                                          //ROSCO DATOS
                                          Expanded(
                                            child: Card(
                                              elevation: 2.5,
                                              color: whiteAthenea,
                                              shadowColor: darkBlueAthenea,
                                              child: consumoDatos == null
                                                  ? Container(
                                                      height: 35 * SizeConfig.heightMultiplier,
                                                      width: 35 * SizeConfig.heightMultiplier,
                                                      child: Padding(
                                                        padding: EdgeInsets.fromLTRB(
                                                          4 * SizeConfig.heightMultiplier,
                                                          7 * SizeConfig.heightMultiplier,
                                                          4 * SizeConfig.heightMultiplier,
                                                          7 * SizeConfig.heightMultiplier,
                                                        ),
                                                        child: CircularProgressIndicator(
                                                          valueColor: AlwaysStoppedAnimation<Color>(whiteAthenea),
                                                          backgroundColor: blueAthenea,
                                                        ),
                                                      ))
                                                  : balanceDatos == null
                                                      ? CircularProgressIndicator(
                                                          valueColor: AlwaysStoppedAnimation<Color>(whiteAthenea),
                                                          backgroundColor: blueAthenea,
                                                        )
                                                      : Container(
                                                          height: 35 * SizeConfig.heightMultiplier,
                                                          child: Padding(
                                                            padding: EdgeInsets.fromLTRB(
                                                              1 * SizeConfig.heightMultiplier,
                                                              1 * SizeConfig.heightMultiplier,
                                                              1 * SizeConfig.heightMultiplier,
                                                              1 * SizeConfig.heightMultiplier,
                                                            ),
                                                            child: Center(
                                                              child: Stack(
                                                                children: <Widget>[
                                                                  Center(
                                                                    child: new AnimatedCircularChart(
                                                                      holeRadius: 20.0,
                                                                      key: _chartKey1,
                                                                      size: Size(
                                                                        35 * SizeConfig.heightMultiplier,
                                                                        35 * SizeConfig.heightMultiplier,
                                                                      ),
                                                                      initialChartData: dataInternet,
                                                                      chartType: CircularChartType.Radial,
                                                                      labelStyle: new TextStyle(
                                                                        color: darkBlueAthenea,
                                                                        fontSize: 2 * SizeConfig.heightMultiplier,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  _centroRoscoDatos(),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      // FILA CUOTAS
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Card(
                                              elevation: 2.5,
                                              color: whiteAthenea,
                                              shadowColor: darkBlueAthenea,
                                              child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                  ),
                                                  child: Center(
                                                    child: Column(
                                                      children: <Widget>[
                                                        Text(
                                                          'CONSUMIDO:',
                                                          style: TextStyle(
                                                              fontWeight: FontWeight.bold,
                                                              color: blueAthenea,
                                                              fontSize: 1 * SizeConfig.heightMultiplier),
                                                        ),
                                                        SizedBox(height: 0.5 * SizeConfig.heightMultiplier),
                                                        bonoMovil == null && tarifaMovil == null
                                                            ? Container(
                                                                /*  height: 20,
                                                                width: 20, */
                                                                height: 2 * SizeConfig.heightMultiplier,
                                                                width: 2 * SizeConfig.heightMultiplier,
                                                                child: CircularProgressIndicator(
                                                                  valueColor: AlwaysStoppedAnimation<Color>(whiteAthenea),
                                                                  backgroundColor: blueAthenea,
                                                                ))
                                                            : tarifaMovil.contains('COSTE') == true ||
                                                                    tarifaMovil.contains('TBC_DISTRIBUIDOR') == true ||
                                                                    tarifaMovil.contains('DISNAVEGAMINI') == true ||
                                                                    bonoMovil.toString().contains('ILIMITADO')
                                                                ? Text(
                                                                    '-',
                                                                    style: TextStyle(
                                                                        color: darkBlueAthenea,
                                                                        fontSize: 1.5 * SizeConfig.heightMultiplier),
                                                                  )
                                                                : Text(
                                                                    '${consumoMovil.round()} Min',
                                                                    style: TextStyle(
                                                                        color: darkBlueAthenea,
                                                                        fontSize: 1.5 * SizeConfig.heightMultiplier),
                                                                  ),
                                                      ],
                                                    ),
                                                  )),
                                            ),
                                          ),
                                          Expanded(
                                            child: Card(
                                                elevation: 2.5,
                                                color: whiteAthenea,
                                                shadowColor: darkBlueAthenea,
                                                child: Padding(
                                                    padding: EdgeInsets.fromLTRB(
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                    ),
                                                    child: Center(
                                                      child: Column(
                                                        children: <Widget>[
                                                          Text(
                                                            'CUOTA:',
                                                            style: TextStyle(
                                                                fontWeight: FontWeight.bold,
                                                                color: blueAthenea,
                                                                fontSize: 1 * SizeConfig.heightMultiplier),
                                                          ),
                                                          SizedBox(height: 0.5 * SizeConfig.heightMultiplier),
                                                          bonoDatos == null
                                                              ? Container(
                                                                  height: 2 * SizeConfig.heightMultiplier,
                                                                  width: 2 * SizeConfig.heightMultiplier,
                                                                  child: CircularProgressIndicator(
                                                                    valueColor: AlwaysStoppedAnimation<Color>(whiteAthenea),
                                                                    backgroundColor: blueAthenea,
                                                                  ))
                                                              : bonoDatos == 0.0
                                                                  ? Text('-',
                                                                      style: TextStyle(
                                                                          color: darkBlueAthenea,
                                                                          fontSize: 1.5 * SizeConfig.heightMultiplier))
                                                                  : bonoDatos >= 250.0
                                                                      ? Text('GB Ilimitados',
                                                                          style: TextStyle(
                                                                              color: darkBlueAthenea,
                                                                              fontWeight: FontWeight.bold,
                                                                              fontSize: 1.5 * SizeConfig.heightMultiplier))
                                                                      : Text(
                                                                          '$bonoDatos GB',
                                                                          style: TextStyle(
                                                                              color: darkBlueAthenea,
                                                                              fontSize: 1.5 * SizeConfig.heightMultiplier),
                                                                        ),
                                                        ],
                                                      ),
                                                    ))),
                                          ),
                                        ],
                                      ),
                                      // FILA TE QUEDAN
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Card(
                                              elevation: 2.5,
                                              color: whiteAthenea,
                                              shadowColor: darkBlueAthenea,
                                              child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                  ),
                                                  child: Center(
                                                    child: Column(
                                                      children: <Widget>[
                                                        Text(
                                                          'TE QUEDAN:',
                                                          style: TextStyle(
                                                              fontWeight: FontWeight.bold,
                                                              color: blueAthenea,
                                                              fontSize: 1 * SizeConfig.heightMultiplier),
                                                        ),
                                                        SizedBox(height: 0.5 * SizeConfig.heightMultiplier),
                                                        bonoMovil == null && tarifaMovil == null
                                                            ? Container(
                                                                height: 2 * SizeConfig.heightMultiplier,
                                                                width: 2 * SizeConfig.heightMultiplier,
                                                                child: CircularProgressIndicator(
                                                                  valueColor: AlwaysStoppedAnimation<Color>(whiteAthenea),
                                                                  backgroundColor: blueAthenea,
                                                                ))
                                                            : tarifaMovil.contains('COSTE') == true ||
                                                                    tarifaMovil.contains('DISNAVEGAMINI') == true
                                                                ? Text('-',
                                                                    style: TextStyle(
                                                                        color: darkBlueAthenea,
                                                                        fontSize: 1.5 * SizeConfig.heightMultiplier))
                                                                : bonoMovil.contains('BONOILIMITADO') == true
                                                                    ? Text(
                                                                        'Min.Ilimitados',
                                                                        style: TextStyle(
                                                                            color: darkBlueAthenea,
                                                                            fontWeight: FontWeight.bold,
                                                                            fontSize: 1.5 * SizeConfig.heightMultiplier),
                                                                      )
                                                                    : Text(
                                                                        '${bonoMovilTeQuedan()}',
                                                                        style: TextStyle(
                                                                            color: darkBlueAthenea,
                                                                            fontSize: 1.5 * SizeConfig.heightMultiplier),
                                                                      ),
                                                      ],
                                                    ),
                                                  )),
                                            ),
                                          ),
                                          Expanded(
                                            child: Card(
                                                elevation: 2.5,
                                                color: whiteAthenea,
                                                shadowColor: darkBlueAthenea,
                                                child: Padding(
                                                    padding: EdgeInsets.fromLTRB(
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                    ),
                                                    child: Center(
                                                      child: Column(
                                                        children: <Widget>[
                                                          Text(
                                                            'TE QUEDAN:',
                                                            style: TextStyle(
                                                                fontWeight: FontWeight.bold,
                                                                color: blueAthenea,
                                                                fontSize: 1 * SizeConfig.heightMultiplier),
                                                          ),
                                                          SizedBox(height: 0.5 * SizeConfig.heightMultiplier),
                                                          bonoDatos == null
                                                              ? Container(
                                                                  height: 2 * SizeConfig.heightMultiplier,
                                                                  width: 2 * SizeConfig.heightMultiplier,
                                                                  child: CircularProgressIndicator(
                                                                    valueColor: AlwaysStoppedAnimation<Color>(whiteAthenea),
                                                                    backgroundColor: blueAthenea,
                                                                  ))
                                                              : bonoDatos == 0.0
                                                                  ? Text('-',
                                                                      style: TextStyle(
                                                                          color: darkBlueAthenea,
                                                                          fontSize: 1.5 * SizeConfig.heightMultiplier))
                                                                  : bonoDatos >= 250.0
                                                                      ? Text('Datos Ilimitados',
                                                                          style: TextStyle(
                                                                              color: darkBlueAthenea,
                                                                              fontWeight: FontWeight.bold,
                                                                              fontSize: 1.5 * SizeConfig.heightMultiplier))
                                                                      : Text(
                                                                          '${balanceDatos.toStringAsPrecision(3)} GB',
                                                                          style: TextStyle(
                                                                              color: darkBlueAthenea,
                                                                              fontSize: 1.5 * SizeConfig.heightMultiplier),
                                                                        ),
                                                        ],
                                                      ),
                                                    ))),
                                          ),
                                        ],
                                      ),
                                      // FILA EXCESO
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Card(
                                              elevation: 2.5,
                                              color: whiteAthenea,
                                              shadowColor: darkBlueAthenea,
                                              child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                  ),
                                                  child: Center(
                                                    child: Column(
                                                      children: <Widget>[
                                                        Text(
                                                          'EXCESO:',
                                                          style: TextStyle(
                                                              fontWeight: FontWeight.bold,
                                                              color: blueAthenea,
                                                              fontSize: 1 * SizeConfig.heightMultiplier),
                                                        ),
                                                        SizedBox(height: 0.5 * SizeConfig.heightMultiplier),
                                                        bonoMovil == null && tarifaMovil == null
                                                            ? Container(
                                                                height: 2 * SizeConfig.heightMultiplier,
                                                                width: 2 * SizeConfig.heightMultiplier,
                                                                child: CircularProgressIndicator(
                                                                  valueColor: AlwaysStoppedAnimation<Color>(whiteAthenea),
                                                                  backgroundColor: blueAthenea,
                                                                ),
                                                              )
                                                            : tarifaMovil.contains('COSTE') == true ||
                                                                    tarifaMovil.contains('TBC_DISTRIBUIDOR') == true ||
                                                                    tarifaMovil.contains('DISNAVEGAMINI') == true
                                                                ? Text('-',
                                                                    style: TextStyle(
                                                                        color: darkBlueAthenea,
                                                                        fontSize: 1.5 * SizeConfig.heightMultiplier))
                                                                : Text(
                                                                    excesoMin != null ? '$excesoMin Min' : '-',
                                                                    style: TextStyle(
                                                                        color: darkBlueAthenea,
                                                                        fontSize: 1.5 * SizeConfig.heightMultiplier),
                                                                  ),
                                                      ],
                                                    ),
                                                  )),
                                            ),
                                          ),
                                          Expanded(
                                            child: Card(
                                                elevation: 2.5,
                                                color: whiteAthenea,
                                                shadowColor: darkBlueAthenea,
                                                child: Padding(
                                                    padding: EdgeInsets.fromLTRB(
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                      1 * SizeConfig.heightMultiplier,
                                                    ),
                                                    child: Center(
                                                      child: Column(
                                                        children: <Widget>[
                                                          Text(
                                                            'EXCESO:',
                                                            style: TextStyle(
                                                                fontWeight: FontWeight.bold,
                                                                color: blueAthenea,
                                                                fontSize: 1 * SizeConfig.heightMultiplier),
                                                          ),
                                                          SizedBox(height: 0.5 * SizeConfig.heightMultiplier),
                                                          bonoDatos == null
                                                              ? Container(
                                                                  height: 2 * SizeConfig.heightMultiplier,
                                                                  width: 2 * SizeConfig.heightMultiplier,
                                                                  child: CircularProgressIndicator(
                                                                    valueColor: AlwaysStoppedAnimation<Color>(whiteAthenea),
                                                                    backgroundColor: blueAthenea,
                                                                  ))
                                                              : bonoDatos == 0.0
                                                                  ? Text('-',
                                                                      style: TextStyle(
                                                                          color: darkBlueAthenea,
                                                                          fontSize: 1.5 * SizeConfig.heightMultiplier))
                                                                  : bonoDatos >= 250.0
                                                                      ? Text('-',
                                                                          style: TextStyle(
                                                                              color: darkBlueAthenea,
                                                                              fontSize: 1.5 * SizeConfig.heightMultiplier))
                                                                      : Text(
                                                                          '${masMegasDatos.toStringAsPrecision(1)} GB',
                                                                          style: TextStyle(
                                                                              color: darkBlueAthenea,
                                                                              fontSize: 1.5 * SizeConfig.heightMultiplier),
                                                                        ),
                                                        ],
                                                      ),
                                                    ))),
                                          ),
                                        ],
                                      ),
                                      // FILA ROAMING
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Card(
                                              elevation: 2.5,
                                              color: whiteAthenea,
                                              shadowColor: darkBlueAthenea,
                                              child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                    1 * SizeConfig.heightMultiplier,
                                                  ),
                                                  child: Center(
                                                    child: Column(
                                                      children: <Widget>[
                                                        Text(
                                                          'ROAMING RESTANTE:',
                                                          style: TextStyle(
                                                              fontWeight: FontWeight.bold,
                                                              color: blueAthenea,
                                                              fontSize: 1 * SizeConfig.heightMultiplier),
                                                        ),
                                                        SizedBox(height: 0.5 * SizeConfig.heightMultiplier),
                                                        bonoDatos == null
                                                            ? Container(
                                                                height: 2 * SizeConfig.heightMultiplier,
                                                                width: 2 * SizeConfig.heightMultiplier,
                                                                child: CircularProgressIndicator(
                                                                  valueColor: AlwaysStoppedAnimation<Color>(whiteAthenea),
                                                                  backgroundColor: blueAthenea,
                                                                ))
                                                            : bonoR == 0.0
                                                                ? Text('-',
                                                                    style: TextStyle(
                                                                        fontWeight: FontWeight.bold,
                                                                        color: darkBlueAthenea,
                                                                        fontSize: 1.5 * SizeConfig.heightMultiplier))
                                                                : teQuedan == 0.0
                                                                    ? Text(
                                                                        '$balanceR GB',
                                                                        style: TextStyle(
                                                                            color: darkBlueAthenea,
                                                                            fontSize: 1.5 * SizeConfig.heightMultiplier),
                                                                      )
                                                                    : Text(
                                                                        '$balanceR GB',
                                                                        style: TextStyle(
                                                                            color: darkBlueAthenea,
                                                                            fontSize: 1.5 * SizeConfig.heightMultiplier),
                                                                      ),
                                                      ],
                                                    ),
                                                  )),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                      )
                    ],
                  ),
                ),
              ]),
            ),
          );
        });
      },
    );
  }

  Future getDeionBase2() async {
    setState(() {
      var consumoProvider = Provider.of<ConsumoProvider>(context);
      consumoProvider.tarifaMovil = null;
      consumoProvider.tipollamada = null;
      consumoProvider.consumoMovil = null;
      consumoProvider.tipoRed = null;
      consumoProvider.bonoMovil = null;
      var dropdownvalueProvider = Provider.of<DropDownValueProvider>(context);
      dropdownValue = dropdownvalueProvider.dropDownValue;
    });

    var envelope = '''
     <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:Operaciones">
         <soapenv:Header/>
         <soapenv:Body>
           <urn:getvertarifaContratadaDEION_BASE_2 soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
          <telefono xsi:type="xsd:string">$dropdownValue</telefono>
          <token xsi:type="xsd:string">999</token>
          <tipoRetorno xsi:type="xsd:string">?</tipoRetorno>
       </urn:getvertarifaContratadaDEION_BASE_2>
         </soapenv:Body>
     </soapenv:Envelope>
    ''';

    http.Response response = await http.post('http://212.225.255.6/AreaClientes/ApiAreaClientes/intws.php',
        headers: {'Content-Type': 'text/xml; charset=utf-8', 'SOAPAction': 'AuthenticateCredential'}, body: envelope);
    var document = xml.parse(response.body);

    var _tarifaMovil = document.findAllElements('TARIFA_MOVIL').map((each) => each.text);
    var tarifaMovil = _tarifaMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
    print('TARIFA MOVIL : $tarifaMovil');

    var _tipoLlamadaMovil = document.findAllElements('TIPO_LLAMADA').map((each) => each.text);

    var tipoLlamadaMovil = _tipoLlamadaMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
    print('TIPO LLAMADA: $tipoLlamadaMovil');

    var _consumoMovil =
        document.findAllElements('item').map((f) => double.parse(f.findAllElements('CONSUMO').single.text)).reduce((a, b) => (a + b));
    var consumoMovil = _consumoMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
    print('CONSUMO: $consumoMovil');

    var _tipoRedMovil = document.findAllElements('TIPO_RED').map((each) => each.text);
    var tipoRedMovil = _tipoRedMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
    print('TIPO RED: $tipoRedMovil');

    var _bonoMovil = document.findAllElements('item').map((f) => f.findAllElements('BONO').single.text);
    var bonoMovil = _bonoMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
    print('BONO A:$bonoMovil');

    setState(() {
      var consumoProvider = Provider.of<ConsumoProvider>(context);
      consumoProvider.tarifaMovil = tarifaMovil;
      consumoProvider.tipollamada = tipoLlamadaMovil;
      consumoProvider.consumoMovil = double.parse(consumoMovil);
      consumoProvider.tipoRed = tipoRedMovil;
      consumoProvider.bonoMovil = bonoMovil;
    });
  }

  Future getDeion() async {
    setState(() {
      var consumoProvider = Provider.of<ConsumoProvider>(context);
      consumoProvider.tarifaMovil = null;
      consumoProvider.tipollamada = null;
      consumoProvider.consumoMovil = null;
      consumoProvider.tipoRed = null;
      consumoProvider.bonoMovil = null;
      var dropdownvalueProvider = Provider.of<DropDownValueProvider>(context);
      dropdownValue = dropdownvalueProvider.dropDownValue;
    });

    var envelope = '''
     <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:Operaciones">
         <soapenv:Header/>
         <soapenv:Body>
           <urn:getvertarifaContratadaDEION soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
              <telefono xsi:type="xsd:string">$dropdownValue</telefono>
              <token xsi:type="xsd:string">999</token>
              <tipoRetorno xsi:type="xsd:string">?</tipoRetorno>
           </urn:getvertarifaContratadaDEION>
         </soapenv:Body>
     </soapenv:Envelope>
     ''';

    http.Response response = await http.post('http://212.225.255.6/AreaClientes/ApiAreaClientes/intws.php',
        headers: {'Content-Type': 'text/xml; charset=utf-8', 'SOAPAction': 'AuthenticateCredential'}, body: envelope);
    var document = xml.parse(response.body);

    print('RESPONSE: $document');

    var control = document.findAllElements('item').map((each) => each.text);

    if (control.length != 0) {
      var _tarifaMovil = document.findAllElements('TARIFA_MOVIL').map((each) => each.text);
      var tarifaMovil = _tarifaMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
      print('TARIFA MOVIL : $tarifaMovil');

      var _tipoLlamadaMovil = document.findAllElements('TIPO_LLAMADA').map((each) => each.text);

      var tipoLlamadaMovil = _tipoLlamadaMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
      print('TIPO LLAMADA: $tipoLlamadaMovil');

      var _consumoMovil =
          document.findAllElements('item').map((f) => double.parse(f.findAllElements('CONSUMO').single.text)).reduce((a, b) => (a + b));
      var consumoMovil = _consumoMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
      print('CONSUMO: $consumoMovil');

      var _tipoRedMovil = document.findAllElements('TIPO_RED').map((each) => each.text);
      var tipoRedMovil = _tipoRedMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
      print('TIPO RED: $tipoRedMovil');

      var _bonoMovil = document.findAllElements('item').map((f) => f.findAllElements('BONO').single.text);
      var bonoMovil = _bonoMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
      print('BONO B:$bonoMovil');

      setState(() {
        var consumoProvider = Provider.of<ConsumoProvider>(context);
        consumoProvider.tarifaMovil = tarifaMovil;
        consumoProvider.tipollamada = tipoLlamadaMovil;
        consumoProvider.consumoMovil = double.parse(consumoMovil);
        consumoProvider.tipoRed = tipoRedMovil;
        consumoProvider.bonoMovil = bonoMovil;
      });
    } else {
      var control = document.findAllElements('item').map((each) => each.text);

      print(control.length);

      print('SIN BONO');
      Xml2Json xml2json = new Xml2Json();
      String tarifaUrl = 'http://212.225.255.130:8010/ws/devtarifa/$dropdownValue';
      http.Response resptarifa = await http.get(tarifaUrl);
      await pr.hide();
      xml2json.parse(resptarifa.body);

      String xmldata = xml2json.toGData();
      Map<String, dynamic> data = convert.jsonDecode(xmldata);
      String tarifaMovil = data['result']['Registro']['Tarifa'].replaceAll(" ", "");

      if (tarifaMovil != 'DISNAVEGAMINI') {
        String controlurl = 'http://212.225.255.130:8010/ws/devbonotar/$tarifaMovil';
        print("Result: $controlurl");

        http.Response respcontrol = await http.get(controlurl);
        await pr.hide();
        xml2json.parse(respcontrol.body);

        String xmldata2 = xml2json.toGData();
        Map<String, dynamic> data2 = convert.jsonDecode(xmldata2);
        String bonoDis = data2['result']['Registro']['Bono'].replaceAll(" ", "");

        setState(() {
          var consumoProvider = Provider.of<ConsumoProvider>(context);
          consumoProvider.tarifaMovil = tarifaMovil;
          consumoProvider.tipollamada = '';
          consumoProvider.consumoMovil = double.parse('0');
          consumoProvider.tipoRed = '';
          consumoProvider.bonoMovil = bonoDis;
        });
      } else {
        setState(() {
          var consumoProvider = Provider.of<ConsumoProvider>(context);
          consumoProvider.tarifaMovil = tarifaMovil;
          consumoProvider.tipollamada = null;
          consumoProvider.consumoMovil = null;
          consumoProvider.tipoRed = null;
          consumoProvider.bonoMovil = null;
        });
      }
    }
  }

  Future getDis() async {
    setState(() {
      var consumoProvider = Provider.of<ConsumoProvider>(context);
      consumoProvider.tarifaMovil = null;
      consumoProvider.tipollamada = null;
      consumoProvider.consumoMovil = null;
      consumoProvider.tipoRed = null;
      consumoProvider.bonoMovil = null;
      var dropdownvalueProvider = Provider.of<DropDownValueProvider>(context);
      dropdownValue = dropdownvalueProvider.dropDownValue;
    });

    var envelope = '''
     <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:Operaciones">
   <soapenv:Header/>
   <soapenv:Body>
      <urn:getvertarifaContratadaDISPLATA soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
         <telefono xsi:type="xsd:string">$dropdownValue</telefono>
         <token xsi:type="xsd:string">?</token>
         <tipoRetorno xsi:type="xsd:string">?</tipoRetorno>
      </urn:getvertarifaContratadaDISPLATA>
   </soapenv:Body>
</soapenv:Envelope>
     ''';

    http.Response response = await http.post('http://212.225.255.6/AreaClientes/ApiAreaClientes/intws.php',
        headers: {'Content-Type': 'text/xml; charset=utf-8', 'SOAPAction': 'AuthenticateCredential'}, body: envelope);
    var document = xml.parse(response.body);

    var control = document.findAllElements('item').map((each) => each.text);

    if (control.length == 0) {
      print('SIN BONO');
      Xml2Json xml2json = new Xml2Json();
      String tarifaUrl = 'http://212.225.255.130:8010/ws/devtarifa/$dropdownValue';
      http.Response resptarifa = await http.get(tarifaUrl);
      await pr.hide();
      xml2json.parse(resptarifa.body);

      String xmldata = xml2json.toGData();
      Map<String, dynamic> data = convert.jsonDecode(xmldata);
      String tarifaMovil = data['result']['Registro']['Tarifa'].replaceAll(" ", "");
      print("Tarifa buscadora de pruebas: $tarifaMovil");

      if (tarifaMovil != 'DISNAVEGAMINI') {
        String controlurl = 'http://212.225.255.130:8010/ws/devbonotar/$tarifaMovil';

        http.Response respcontrol = await http.get(controlurl);
        await pr.hide();
        xml2json.parse(respcontrol.body);

        String xmldata2 = xml2json.toGData();
        Map<String, dynamic> data2 = convert.jsonDecode(xmldata2);
        String bonoDis = data2['result']['Registro']['Bono'].replaceAll(" ", "");
        print("Bono C: $bonoDis");

        setState(() {
          var consumoProvider = Provider.of<ConsumoProvider>(context);
          consumoProvider.tarifaMovil = tarifaMovil;
          consumoProvider.tipollamada = '';
          consumoProvider.consumoMovil = double.parse('0');
          consumoProvider.tipoRed = '';
          consumoProvider.bonoMovil = bonoDis;
        });
      } else {
        setState(() {
          var consumoProvider = Provider.of<ConsumoProvider>(context);
          consumoProvider.tarifaMovil = tarifaMovil;
          consumoProvider.tipollamada = null;
          consumoProvider.consumoMovil = null;
          consumoProvider.tipoRed = null;
          consumoProvider.bonoMovil = null;
        });
      }
    } else {
      var _tarifaMovil = document.findAllElements('TARIFA_MOVIL').map((each) => each.text);
      var tarifaMovil = _tarifaMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
      print('TARIFA MOVIL : $tarifaMovil');

      var _tipoLlamadaMovil = document.findAllElements('TIPO_LLAMADA').map((each) => each.text);

      var tipoLlamadaMovil = _tipoLlamadaMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
      print('TIPO LLAMADA: $tipoLlamadaMovil');

      var _consumoMovil =
          document.findAllElements('item').map((f) => double.parse(f.findAllElements('CONSUMO').single.text)).reduce((a, b) => (a + b));
      var consumoMovil = _consumoMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
      print('CONSUMO: $consumoMovil');

      var _tipoRedMovil = document.findAllElements('TIPO_RED').map((each) => each.text);
      var tipoRedMovil = _tipoRedMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
      print('TIPO RED: $tipoRedMovil');

      var _bonoMovil = document.findAllElements('item').map((f) => f.findAllElements('BONO').single.text);
      var bonoMovil = _bonoMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
      print('BONO D:$bonoMovil');

      setState(() {
        var consumoProvider = Provider.of<ConsumoProvider>(context);
        consumoProvider.tarifaMovil = tarifaMovil;
        consumoProvider.tipollamada = tipoLlamadaMovil;
        consumoProvider.consumoMovil = double.parse(consumoMovil);
        consumoProvider.tipoRed = tipoRedMovil;
        consumoProvider.bonoMovil = bonoMovil;
      });
    }
  }

  Future getMovisurBase() async {
    print('seleccionado getMovisurBase');

    setState(() {
      var consumoProvider = Provider.of<ConsumoProvider>(context);
      consumoProvider.tarifaMovil = null;
      consumoProvider.tipollamada = null;
      consumoProvider.consumoMovil = null;
      consumoProvider.tipoRed = null;
      consumoProvider.bonoMovil = null;
      var dropdownvalueProvider = Provider.of<DropDownValueProvider>(context);
      dropdownValue = dropdownvalueProvider.dropDownValue;
    });

    var envelope = '''
     <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:Operaciones">
   <soapenv:Header/>
   <soapenv:Body>
      <urn:getvertarifaContratadaMOVISUR_BASE soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
         <telefono xsi:type="xsd:string">$dropdownValue</telefono>
         <token xsi:type="xsd:string">999</token>
         <tipoRetorno xsi:type="xsd:string">?</tipoRetorno>
      </urn:getvertarifaContratadaMOVISUR_BASE>
   </soapenv:Body>
</soapenv:Envelope>
     ''';

    http.Response response = await http.post('http://212.225.255.6/AreaClientes/ApiAreaClientes/intws.php',
        headers: {'Content-Type': 'text/xml; charset=utf-8', 'SOAPAction': 'AuthenticateCredential'}, body: envelope);
    var document = xml.parse(response.body);

    var _tarifaMovil = document.findAllElements('TARIFA_MOVIL').map((each) => each.text);
    var tarifaMovil = _tarifaMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
    print('TARIFA MOVIL : $tarifaMovil');

    var _tipoLlamadaMovil = document.findAllElements('TIPO_LLAMADA').map((each) => each.text);

    var tipoLlamadaMovil = _tipoLlamadaMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
    print('TIPO LLAMADA: $tipoLlamadaMovil');

    var _consumoMovil =
        document.findAllElements('item').map((f) => double.parse(f.findAllElements('CONSUMO').single.text)).reduce((a, b) => (a + b));
    var consumoMovil = _consumoMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
    print('CONSUMO: $consumoMovil');

    var _tipoRedMovil = document.findAllElements('TIPO_RED').map((each) => each.text);
    var tipoRedMovil = _tipoRedMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
    print('TIPO RED: $tipoRedMovil');

    var _bonoMovil = document.findAllElements('item').map((f) => f.findAllElements('BONO').single.text);
    var bonoMovil = _bonoMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
    print('BONO E:$bonoMovil');

    setState(() {
      var consumoProvider = Provider.of<ConsumoProvider>(context);
      consumoProvider.tarifaMovil = tarifaMovil;
      consumoProvider.tipollamada = tipoLlamadaMovil;
      consumoProvider.consumoMovil = double.parse(consumoMovil);
      consumoProvider.tipoRed = tipoRedMovil;
      consumoProvider.bonoMovil = bonoMovil;
    });
  }

  Future getMovisur() async {
    setState(() {
      var consumoProvider = Provider.of<ConsumoProvider>(context);
      consumoProvider.tarifaMovil = null;
      consumoProvider.tipollamada = null;
      consumoProvider.consumoMovil = null;
      consumoProvider.tipoRed = null;
      consumoProvider.bonoMovil = null;
      var dropdownvalueProvider = Provider.of<DropDownValueProvider>(context);
      dropdownValue = dropdownvalueProvider.dropDownValue;
    });

    var envelope = '''
    <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:Operaciones">
   <soapenv:Header/>
   <soapenv:Body>
      <urn:getvertarifaContratadaMOVISUR_C soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
         <telefono xsi:type="xsd:string">$dropdownValue</telefono>
         <token xsi:type="xsd:string">999</token>
         <tipoRetorno xsi:type="xsd:string">?</tipoRetorno>
      </urn:getvertarifaContratadaMOVISUR_C>
   </soapenv:Body>
</soapenv:Envelope>
    ''';

    http.Response response = await http.post('http://212.225.255.6/AreaClientes/ApiAreaClientes/intws.php',
        headers: {'Content-Type': 'text/xml; charset=utf-8', 'SOAPAction': 'AuthenticateCredential'}, body: envelope);
    var document = xml.parse(response.body);

    var _tarifaMovil = document.findAllElements('TARIFA_MOVIL').map((each) => each.text);
    var tarifaMovil = _tarifaMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
    print('TARIFA MOVIL : $tarifaMovil');

    var _tipoLlamadaMovil = document.findAllElements('TIPO_LLAMADA').map((each) => each.text);

    var tipoLlamadaMovil = _tipoLlamadaMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
    print('TIPO LLAMADA: $tipoLlamadaMovil');

    var _consumoMovil =
        document.findAllElements('item').map((f) => double.parse(f.findAllElements('CONSUMO').single.text)).reduce((a, b) => (a + b));
    var consumoMovil = _consumoMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
    print('CONSUMO: $consumoMovil');

    var _tipoRedMovil = document.findAllElements('TIPO_RED').map((each) => each.text);
    var tipoRedMovil = _tipoRedMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
    print('TIPO RED: $tipoRedMovil');

    var _bonoMovil = document.findAllElements('item').map((f) => f.findAllElements('BONO').single.text);
    var bonoMovil = _bonoMovil.toString().replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '');
    print('BONO F:$bonoMovil');

    setState(() {
      var consumoProvider = Provider.of<ConsumoProvider>(context);
      consumoProvider.tarifaMovil = tarifaMovil;
      consumoProvider.tipollamada = tipoLlamadaMovil;
      consumoProvider.consumoMovil = double.parse(consumoMovil);
      consumoProvider.tipoRed = tipoRedMovil;
      consumoProvider.bonoMovil = bonoMovil;
    });
  }

  Future getCoste() async {
    return;
  }

  Widget _centroRoscoMovil() {
    var consumoProvider = Provider.of<ConsumoProvider>(context);
    var bonoMovil = consumoProvider.bonoMovil;

    double porcentajeConsumido150 = consumoProvider.consumoMovil * 100 / 150;
    double porcentajeConsumido100 = consumoProvider.consumoMovil * 100 / 100;
    double porcentajeConsumido200 = consumoProvider.consumoMovil * 100 / 200;
    double porcentajeConsumido300 = consumoProvider.consumoMovil * 100 / 300;

    if (bonoMovil.contains('ILIMITADO') == true || bonoMovil.contains('Ilimit') == true) {
      return Center(
        child: Container(
          child: Icon(
            Icons.all_inclusive,
            size: 8 * SizeConfig.heightMultiplier,
            color: darkBlueAthenea,
          ),
        ),
      );
    }
    if (bonoMovil.contains('150') == true) {
      return Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Consumido',
            style: TextStyle(fontSize: 1.6 * SizeConfig.heightMultiplier, color: darkBlueAthenea, fontWeight: FontWeight.bold),
          ),
          Text(
            '${porcentajeConsumido150.toStringAsPrecision(4)}%',
            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
          ),
        ],
      ));
    } else if (bonoMovil.contains('100') == true) {
      return Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Consumido',
            style: TextStyle(fontSize: 1.6 * SizeConfig.heightMultiplier, color: darkBlueAthenea, fontWeight: FontWeight.bold),
          ),
          Text(
            '${porcentajeConsumido100.round()}%',
            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
          ),
        ],
      ));
    } else if (bonoMovil.contains('200') == true) {
      return Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Consumido',
            style: TextStyle(fontSize: 1.6 * SizeConfig.heightMultiplier, color: darkBlueAthenea, fontWeight: FontWeight.bold),
          ),
          Text(
            '${porcentajeConsumido200.toStringAsFixed(1)}%',
            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
          ),
        ],
      ));
    } else if (bonoMovil.contains('300') == true) {
      return Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Consumido',
            style: TextStyle(fontSize: 1.6 * SizeConfig.heightMultiplier, color: darkBlueAthenea, fontWeight: FontWeight.bold),
          ),
          Text(
            '${porcentajeConsumido300.toStringAsFixed(1)}%',
            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
          ),
        ],
      ));
    } else
      return Container();
  }

  Widget _centroRoscoDatos() {
    var consumoProvider = Provider.of<ConsumoProvider>(context);
    String con = consumoProvider.consumoDatos.toStringAsFixed(1);

    consumoProvider.bonoDatos;
    consumoProvider.balanceDatos;
    consumoProvider.consumoDatos;
    consumoProvider.masMegasDatos;

    double resultadoPorcentajeDatos = consumoProvider.consumoDatos * 100 / consumoProvider.bonoDatos;

    return consumoProvider.bonoDatos >= 250.0
        ? Center(
            child: Container(
              child: Icon(
                Icons.all_inclusive,
                size: 8 * SizeConfig.heightMultiplier,
                color: darkBlueAthenea,
              ),
            ),
          )
        : consumoProvider.bonoDatos == 0.0
            ? Container(
                child: Center(
                    child: Text(
                  'Tarifa\nsin\nDatos',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 1.6 * SizeConfig.heightMultiplier, color: darkBlueAthenea, fontWeight: FontWeight.bold),
                )),
              )
            : Center(
                child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Usado',
                    style: TextStyle(fontSize: 1.6 * SizeConfig.heightMultiplier, color: darkBlueAthenea, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    '$con GB',
                    style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                  ),
                  Text(
                    '${resultadoPorcentajeDatos.toStringAsFixed(1)} %',
                    style: TextStyle(
                      color: darkBlueAthenea,
                      fontSize: 1.6 * SizeConfig.heightMultiplier,
                    ),
                  ),
                ],
              ));
  }

  bonoMovilTeQuedan() {
    String tarifaTequedan;
    var consumoProvider = Provider.of<ConsumoProvider>(context);
    var bonoMovil = consumoProvider.bonoMovil;
    var tarifaMovil = consumoProvider.tarifaMovil;

    double consumoMovil = consumoProvider.consumoMovil;

    if (bonoMovil.contains('ILIMIT') == true || tarifaMovil.toString().contains("TCC_")) {
      tarifaTequedan = 'Min. Ilimitados';
    } else {
      if (bonoMovil.contains('150') == true) {
        if ((150 - consumoMovil).round() < 0) {
          tarifaTequedan = '-';
          excesoMin = (consumoMovil - 150).round().toString();
          print("Exceso: $excesoMin");
        } else {
          tarifaTequedan = '${(150 - consumoMovil).round()} Min';
        }
      } else {
        if (bonoMovil.contains('100') == true) {
          if ((100 - consumoMovil).round() < 0) {
            tarifaTequedan = '-';
            excesoMin = (consumoMovil - 100).round().toString();
            print("Exceso: $excesoMin");
          } else {
            tarifaMovil.contains('DISPLATA')
                ? tarifaTequedan = '${(150 - consumoMovil).round()} Min'
                : tarifaTequedan = '${(100 - consumoMovil).round()} Min';
          }
        } else {
          if (bonoMovil.contains('200') == true) {
            if ((200 - consumoMovil).round() < 0) {
              tarifaTequedan = '-';
              excesoMin = (consumoMovil - 200).round().toString();
              print("Exceso: $excesoMin");
            } else {
              tarifaTequedan = '${(200 - consumoMovil).round()} Min';
            }
          } else {
            if (bonoMovil.contains('120') == true) {
              if ((120 - consumoMovil).round() < 0) {
                tarifaTequedan = '-';
                excesoMin = (consumoMovil - 120).round().toString();
                print("Exceso: $excesoMin");
              } else {
                tarifaTequedan = '${(120 - consumoMovil).round()} Min';
              }
            } else {
              tarifaTequedan = 'HAY QUE TRATARLO';
            }
          }
        }
      }
    }

    return tarifaTequedan;
  }
}
