import 'package:comunicacionesatheneacenfor/constants.dart';
import 'package:comunicacionesatheneacenfor/models/Llamadas.dart';
import 'package:comunicacionesatheneacenfor/models/linea.dart';
import 'package:comunicacionesatheneacenfor/providers/lineas_cliente_provider.dart';
import 'package:comunicacionesatheneacenfor/providers/llamadas_provider.dart';
import 'package:comunicacionesatheneacenfor/widgets/size_config.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:xml2json/xml2json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class DetallePage extends StatefulWidget {
  DetallePage({Key key}) : super(key: key);

  @override
  State<DetallePage> createState() => _DetallePageState();
}

class _DetallePageState extends State<DetallePage> {
  ProgressDialog pr;
  String dropdownValue = 'Elige un teléfono';

  Future leerDetallesLlamadas() async {
    pr.show();
    print(dropdownValue);
    Xml2Json xml2json = new Xml2Json();
    try {
      String url = 'http://212.225.255.130:8010/ws/uvcopella2/ip/token/$dropdownValue';
      http.Response resp = await http.get(url);
      await pr.hide();

      xml2json.parse(resp.body);
      String xmldata = xml2json.toGData();
      final jsondata = json.decode(xmldata);
      final llamadas = new Llamadas.fromJsonList(jsondata['result']['linea']);
      List<Llamada> itemsllamadas = llamadas.items;
      final detalleLlamadas = List<Llamada>.of(itemsllamadas);
      var detalleLlamadasProvider = Provider.of<LlamadasProvider>(context);
      detalleLlamadasProvider.detalleLlamada = detalleLlamadas;
    }
    //SALTA EL CATCH
    catch (e) {
      print("Error en detalle llamadas");
      print('Error: $e');
      await pr.hide();
      showDialog(
          context: context,
          builder: (context) => new AlertDialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                content: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.error,
                      color: redAthenea,
                      size: 3 * SizeConfig.heightMultiplier,
                    ),
                    SizedBox(width: 2 * SizeConfig.heightMultiplier),
                    Expanded(
                      child: Text(
                        "Error de comunicación. Inténtelo más tarde o póngase en contacto con su operador.",
                        style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                      ),
                    ),
                  ],
                ),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Text('Ok',
                        style: TextStyle(
                          color: darkBlueAthenea,
                          fontSize: 2 * SizeConfig.heightMultiplier,
                        )),
                  ),
                ],
              ));
    }
    return [];
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    pr.style(message: 'Leyendo datos...');
    var lineasClienteProvider = Provider.of<LineasClienteProvider>(context);
    var detalleLlamadasProvider = Provider.of<LlamadasProvider>(context);
    final List<Llamada> items = detalleLlamadasProvider.detalleLlamada;

    List<Linea> lineasCliente = lineasClienteProvider.lineasCliente;
    List<String> menuItems = [];

    for (var i = 0; i < lineasCliente.length; i++) {
      var numero = int.parse(lineasCliente[i].telefono);
      if (numero > 600000000 && numero < 799999999) {
        menuItems.add(lineasCliente[i].telefono);
      } else {}
    }
    final List<DropdownMenuItem<String>> _dropDownMenuItems = menuItems
        .map(
          (String value) => DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          ),
        )
        .toList();
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);
            return Scaffold(
              appBar: AppBar(
                backgroundColor: blueAthenea,
                iconTheme: IconThemeData(
                  color: yellowAthenea,
                ),
                title: Text(
                  'Detalle de llamadas',
                  style: TextStyle(
                    fontSize: 2 * SizeConfig.heightMultiplier,
                    color: whiteAthenea,
                  ),
                ),
              ),
              body: Container(
                child: Stack(children: <Widget>[
                  Container(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 1 * SizeConfig.heightMultiplier,
                        ),
                        Center(
                          child: Card(
                            shadowColor: darkBlueAthenea,
                            elevation: 2.5,
                            color: whiteAthenea,
                            child: Container(
                              /*     width: MediaQuery.of(context).size.width - 20.0, */
                              width: 95 * SizeConfig.heightMultiplier,
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(
                                  1 * SizeConfig.heightMultiplier,
                                  1 * SizeConfig.heightMultiplier,
                                  1 * SizeConfig.heightMultiplier,
                                  1 * SizeConfig.heightMultiplier,
                                ),
                                child: Column(
                                  /*    runSpacing: 2.5 * SizeConfig.heightMultiplier,
                                  alignment: WrapAlignment.center, */
                                  children: <Widget>[
                                    Text(
                                      'Consulta el detalle de llamadas del número seleccionado',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 1.8 * SizeConfig.heightMultiplier,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 2 * SizeConfig.heightMultiplier,
                                    ),
                                    Container(
                                      /* width: MediaQuery.of(context).size.width * 0.60, */
                                      width: SizeConfig.heightMultiplier * 25,
                                      decoration: BoxDecoration(
                                        border: Border.all(color: blueAthenea, width: 3.0),
                                        borderRadius: BorderRadius.circular(3.0),
                                      ),
                                      child: Center(
                                        child: DropdownButton<String>(
                                            isDense: true,
                                            value: null,
                                            hint: Text(
                                              dropdownValue,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(fontSize: 1.8 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                                            ),
                                            icon: Icon(Icons.keyboard_arrow_down, color: yellowAthenea),
                                            iconSize: 4 * SizeConfig.heightMultiplier,
                                            elevation: 16,
                                            style: TextStyle(
                                              color: darkBlueAthenea,
                                              fontSize: 1.8 * SizeConfig.heightMultiplier,
                                            ),
                                            underline: Container(
                                              height: 0,
                                            ),
                                            onChanged: (String value) {
                                              setState(() {
                                                dropdownValue = value;
                                                leerDetallesLlamadas();
                                              });
                                            },
                                            items: _dropDownMenuItems),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 2 * SizeConfig.heightMultiplier,
                                    ),
                                    Text(
                                      'Llamadas de la línea:   $dropdownValue',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: darkBlueAthenea,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 1.8 * SizeConfig.heightMultiplier,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 0.5 * SizeConfig.heightMultiplier,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: dropdownValue == 'Elige un teléfono'
                              ? Container()
                              : Container(
                                  /*      width: MediaQuery.of(context).size.width - 12.0, */
                                  width: 95 * SizeConfig.heightMultiplier,
                                  child: ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: items.length,
                                    itemBuilder: (context, index) {
                                      return Card(
                                        elevation: 2.5,
                                        shadowColor: darkBlueAthenea,
                                        color: whiteAthenea,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Padding(
                                              padding: EdgeInsets.fromLTRB(0.5 * SizeConfig.heightMultiplier,
                                                  0.5 * SizeConfig.heightMultiplier, 0.5 * SizeConfig.heightMultiplier, 0),
                                              child: Container(
                                                /*   width: MediaQuery.of(context).size.width - 30, */
                                                width: SizeConfig.heightMultiplier * 95,
                                                decoration: BoxDecoration(
                                                  color: blueAthenea,
                                                  borderRadius: BorderRadius.circular(3.0),
                                                ),
                                                child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                    0.5 * SizeConfig.heightMultiplier,
                                                    0.2 * SizeConfig.heightMultiplier,
                                                    0.5 * SizeConfig.heightMultiplier,
                                                    0.3 * SizeConfig.heightMultiplier,
                                                  ),
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: <Widget>[
                                                      Text(
                                                          'Fecha: ${items[index].fecha.substring(6, 8)}-${items[index].fecha.substring(4, 6)}-${items[index].fecha.substring(0, 4)}',
                                                          style: TextStyle(
                                                            fontSize: 1.3 * SizeConfig.heightMultiplier,
                                                            color: whiteAthenea,
                                                          )),
                                                      Text('Hora: ${calculaHora(index)}',
                                                          style: TextStyle(
                                                            fontSize: 1.3 * SizeConfig.heightMultiplier,
                                                            color: whiteAthenea,
                                                          )),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                            ListTile(
                                              dense: true,
                                              leading: Padding(
                                                padding: EdgeInsets.fromLTRB(
                                                  0.5 * SizeConfig.heightMultiplier,
                                                  0.5 * SizeConfig.heightMultiplier,
                                                  0.5 * SizeConfig.heightMultiplier,
                                                  0.3 * SizeConfig.heightMultiplier,
                                                ),
                                                child:
                                                    Icon(_icono(index), color: lightBlueAthenea, size: 4.0 * SizeConfig.heightMultiplier),
                                              ),
                                              title: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
                                                  Flexible(
                                                    flex: 2,
                                                    child: Text(
                                                      'Destino: ${items[index].destino}',
                                                      style: TextStyle(
                                                        fontSize: 1.5 * SizeConfig.heightMultiplier,
                                                        color: darkBlueAthenea,
                                                      ),
                                                    ),
                                                  ),
                                                  //NUEVO 26/10/21 - OCULTAR IMPORTE FACTURADO
                                                  /*  Flexible(
                                            flex: 1,
                                            child: Text(
                                              'Importe',
                                              style: TextStyle(
                                                  color: darkBlueAthenea,
                                                  fontSize: 11,
                                                  fontFamily: 'Made Tommy'),
                                            ),
                                          ), */
                                                ],
                                              ),
                                              subtitle: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: <Widget>[
                                                  Text(
                                                    '${_duracionDatos(index)}',
                                                    style: TextStyle(
                                                      color: darkBlueAthenea,
                                                      fontSize: 1.5 * SizeConfig.heightMultiplier,
                                                    ),
                                                  ),
                                                  //NUEVO 26/10/21 - OCULTAR IMPORTE FACTURADO
                                                  /*   Text(
                                            '${_importe(index)}',
                                            style: TextStyle(
                                                fontSize: 11.0,
                                                fontFamily: 'Made Tommy',
                                                color: darkBlueAthenea),
                                          ), */
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                  ),
                                ),
                        )
                      ],
                    ),
                  ),
                ]),
              ),
            );
          },
        );
      },
    );
  }

  String calculaHora(index) {
    String _hora;

    var detalleLlamadasProvider = Provider.of<LlamadasProvider>(context);

    final List<Llamada> items = detalleLlamadasProvider.detalleLlamada;

    if (items[index].hora.length == 6) {
      _hora = '${items[index].hora.substring(0, 2)}:${items[index].hora.substring(2, 4)}';
    }
    if (items[index].hora.length == 5) {
      _hora = '${items[index].hora.substring(0, 1)}:${items[index].hora.substring(1, 3)}';
    }
    if (items[index].hora.length == 4) {
      _hora = '00:${items[index].hora.substring(0, 2)}';
    }
    if (items[index].hora.length == 3) {
      _hora = '${items[index].hora.substring(0, 1)}:${items[index].hora.substring(1, 3)}';
    }
    if (items[index].hora.length == 2) {
      _hora = '00:00:${items[index].hora.substring(0, 2)}';
    }

    return _hora;
  }

  IconData _icono(int index) {
    IconData icono;
    var detalleLlamadasProvider2 = Provider.of<LlamadasProvider>(context);
    final List<Llamada> items2 = detalleLlamadasProvider2.detalleLlamada;
    String largo;
    largo = items2[index].destino.replaceAll(' ', '');
    if (largo == 'DATOS') {
      icono = Icons.data_usage;
    } else {
      icono = Icons.phone_forwarded;
    }
    return icono;
  }

  _duracionDatos(index) {
    String _duracionDatos;
    var detalleLlamadasProvider4 = Provider.of<LlamadasProvider>(context);
    final List<Llamada> items4 = detalleLlamadasProvider4.detalleLlamada;
    var dato = items4[index].duracion.replaceAll(' ', '');
    int numero = int.parse(dato);

    if (items4[index].destino.replaceAll(' ', '') == 'DATOS') {
      var result = numero / 1000000;
      _duracionDatos = '${result.toStringAsFixed(2)} Mb';
    } else {
      Duration tiempo = new Duration(seconds: numero);

      var h = tiempo.toString().split(":")[0];
      var m = tiempo.toString().split(":")[1];
      var s = tiempo.toString().split(":")[2];
      var output = "";
      if (int.parse(h) != 0) {
        output += h + " h ";
      }
      if (int.parse(m) != 0) {
        if (m.substring(0, 1) == "0") {
          output += m.substring(1, 2) + " min ";
        } else {
          output += m + " min ";
        }
      }
      s = s.split(".000000")[0];
      if (s.substring(0, 1) == "0") {
        output += s.substring(1, 2) + " seg ";
      } else {
        output += s + " seg ";
      }

      _duracionDatos = output;
    }

    return _duracionDatos;
  }

/* _importe(int index) {
    String importe;
    var detalleLlamadasProvider3 = Provider.of<LlamadasProvider>(context);
    final List<Llamada> items3 = detalleLlamadasProvider3.detalleLlamada;
    String largo;
    largo = items3[index].importe.replaceAll(' ', '');
    double numero = double.parse(largo);
    if (numero == 0.00) {
      importe = 'Incluida en Tarifa';
    } else {
      importe = numero.toString() + ' €';
    }

    return importe;
  } */

}
