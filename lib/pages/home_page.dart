import 'dart:io';

import 'package:comunicacionesatheneacenfor/constants.dart';
import 'package:comunicacionesatheneacenfor/models/cliente.dart';
import 'package:comunicacionesatheneacenfor/models/linea.dart';
import 'package:comunicacionesatheneacenfor/providers/clientes_info_provider.dart';
import 'package:comunicacionesatheneacenfor/providers/lineas_cliente_provider.dart';
import 'package:comunicacionesatheneacenfor/widgets/size_config.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:xml2json/xml2json.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'dart:convert';

class HomePage extends StatefulWidget {
  final String tercero;
  HomePage({Key key, @required this.tercero}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //VARIABLES QUE VAMOS A USAR
  Cliente cliente;
  ProgressDialog pr;
  List<String> tarifas = [];
  List<String> telefonos = [];
  List<String> peticiones = [];
  List<String> listadopeticiones = [];
  List<String> listadoLineas = [];
  List<Linea> lineasCliente = [];
  List<Linea> lineasClienteFinal = [];
  String tarifaNombre = "";
  var _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    httpCall();
    super.initState();
  }

  //METODO PARA CARGAR DATOS DEL CLIENTE PASANDOLE EL TERCERO
  void httpCall() async {
    String tercero = widget.tercero;
    Xml2Json xml2json = new Xml2Json();

    /* print(tercero); */
    //HACEMOS LA PETICION EN UN TRY-CATCH
    try {
      String urlclient = 'http://212.225.255.130:8010/ws/datoscli/$tercero';
      http.Response response = await http.get(urlclient);
      xml2json.parse(response.body);
      String jsondataclient = xml2json.toGData();
      Map<String, dynamic> data = convert.jsonDecode(jsondataclient);

      String nombre = data['result']['cliente']['Nombre'];
      String dni = data['result']['cliente']['Dni'];
      String provincia = data['result']['cliente']['provincia'];
      String poblacion = data['result']['cliente']['poblacion'];
      //CARGAMOS LAS LINEAS DEL USUARIO LLAMANDO A ESTE METODO
      Future<List<String>> getLineas() async {
        String urlclient2 = 'http://212.225.255.130:8010/ws/uvdevlinea/$tercero';
        http.Response respuestaLineas = await http.get(urlclient2);
        xml2json.parse(respuestaLineas.body);
        String xmldata = xml2json.toGData();
        final jsondata = json.decode(xmldata);
        if (json.decode(xmldata)['result']['Registro'] is List) {
          final lineas = new Lineas.fromJsonList(jsondata['result']['Registro']);
          lineasCliente = lineas.items;
        } else
          lineasCliente.add(Linea.fromJsonMap(json.decode(xmldata)['result']['Registro']));

        for (var i = 0; i < lineasCliente.length; i++) {
          var numero = int.parse(lineasCliente[i].telefono);
          if (numero > 600000000 && numero < 799999999) {
            lineasClienteFinal.add(lineasCliente[i]);
            String telefonoConsultaTarifa = (lineasCliente[i].telefono);
            /*  print('Telefono: ' + telefonoConsultaTarifa);
            print('Peticion: ' + lineasCliente[i].peticion); */

            Xml2Json xml2json = new Xml2Json();
            //CARGAMOS EN UN TRY-CATCH LAS TARIFAS DEL CLIENTE DE SUS LINEAS
            try {
              String urltarifa = 'http://212.225.255.130:8010/ws/devtarifa/$telefonoConsultaTarifa';
              http.Response resptarifa = await http.get(urltarifa);
              /* print(urltarifa + ': ' + resptarifa.body); */
              xml2json.parse(resptarifa.body);
              String xmldata = xml2json.toGData();
              Map<String, dynamic> data = convert.jsonDecode(xmldata);
              String tarifa = data['result']['Registro']['Tarifa'];

              /*  print('Tarifa: ' + tarifa);
              print("___"); */

              tarifas.add(tarifa);
            } catch (e) {
              /* print('Error: $e'); */
            }
          } else {}
        }

        setState(() {
          var lineasClienteProvider = Provider.of<LineasClienteProvider>(context);
          lineasClienteProvider.lineasCliente = lineasClienteFinal;
        });

        return telefonos;
      }

      setState(() {
        var clienteProvider = Provider.of<ClientesInfoProvider>(context);
        cliente = Cliente(nombre, dni, poblacion, provincia);
        clienteProvider.tercero = tercero;
        clienteProvider.nombre = nombre;
        clienteProvider.provincia = provincia;
        clienteProvider.poblacion = poblacion;
        //LUCAS NUEVO 25/1/22 -- PRINTS PARA VER QUE DEVUELVE DE INFORMACION DEL CLIENTE
        /* print("Nombre: ${clienteProvider.nombre.toString()}");
        print("DNI: ${clienteProvider.dni.toString()}");
        print("PROVINCIA: ${clienteProvider.provincia.toString()}");
        print("POBLACION: ${clienteProvider.poblacion.toString()}");
        print("TERCERO: ${clienteProvider.tercero.toString()}");
        print("TELEFONO: "); */
      });
      getLineas();
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    var lineasClienteProvider = Provider.of<LineasClienteProvider>(context);
    List telefonos = lineasClienteProvider.lineasCliente;

    pr = new ProgressDialog(context);
    pr.style(message: 'Realizando Petición...');
    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(builder: (context, orientation) {
        SizeConfig().init(constraints, orientation);
        return new WillPopScope(
          onWillPop: () async {
            return false;
          },
          child: Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              backgroundColor: blueAthenea,
              iconTheme: IconThemeData(
                color: yellowAthenea,
              ),
              title: Text(
                'Inicio',
                style: TextStyle(
                  fontSize: 2 * SizeConfig.heightMultiplier,
                  color: whiteAthenea,
                ),
              ),
              centerTitle: false,
            ),
            body: cliente == null
                ? Center(child: CircularProgressIndicator())
                : Padding(
                    padding: EdgeInsets.fromLTRB(
                      1 * SizeConfig.heightMultiplier,
                      1 * SizeConfig.heightMultiplier,
                      1 * SizeConfig.heightMultiplier,
                      1 * SizeConfig.heightMultiplier,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[],
                        ),
                        Container(
                          width: 40 * SizeConfig.imageSizeMultiplier,
                          child: Image(
                            image: AssetImage('assets/images/logo512.png'),
                          ),
                        ),
                        Divider(
                          color: yellowAthenea,
                          thickness: 2,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 1 * SizeConfig.heightMultiplier),
                          child: Text(
                            'Bienvenid@ a tu área de cliente',
                            style: TextStyle(color: darkBlueAthenea, fontSize: 2 * SizeConfig.heightMultiplier),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 1 * SizeConfig.heightMultiplier),
                          child: Text(
                            '${cliente.name}',
                            style:
                                TextStyle(color: darkBlueAthenea, fontWeight: FontWeight.bold, fontSize: 1.6 * SizeConfig.heightMultiplier),
                          ),
                        ),
                        Divider(
                          color: yellowAthenea,
                          thickness: 2,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 2 * SizeConfig.heightMultiplier),
                          child: Text('Estas son tus líneas con nosotros: ',
                              style: TextStyle(color: darkBlueAthenea, fontSize: 1.6 * SizeConfig.heightMultiplier)),
                        ),
                        Expanded(
                          child: telefonos.isEmpty
                              ? Center(
                                  child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(darkBlueAthenea),
                                ))
                              : Container(
                                  child: ListView.builder(
                                      shrinkWrap: true,
                                      //LUCAS NUEVO 25/1/22 -- COMENTADO "-1" PORQUE QUITABA UNO DE LA LISTA
                                      itemCount: telefonos.length,
                                      /* - 1 */
                                      itemBuilder: (context, index) {
                                        return Card(
                                          elevation: 2.5,
                                          color: blueAthenea,
                                          shadowColor: darkBlueAthenea,
                                          child: ListTile(
                                            leading: Icon(
                                              Icons.phone_iphone,
                                              color: yellowAthenea,
                                              size: 3.5 * SizeConfig.heightMultiplier,
                                            ),
                                            title: Text(
                                              '${lineasClienteFinal[index].telefono}',
                                              style: TextStyle(color: whiteAthenea, fontSize: 2 * SizeConfig.heightMultiplier),
                                            ),
                                            //LUCAS 8/2/22 -> COMENTAR LINEA PARA QUE NO SE MUESTREN NOMBRES DE TARIFAS
                                            /*  subtitle: Text(
                                          'Tarifa: ${tarifas[index].contains('TBC_AVATEL') ? 'AVATEL' : tarifas[index].contains('TBC_DISTRIBUIDOR') ? 'DISTRIBUIDOR' : tarifas[index].contains('AVATEL_ILIMITADA') ? 'AVATEL ILIMITADA' : tarifas[index].contains('TCC_DIS_25GB') ? 'DISTR 25 GB' : tarifas[index].contains('TCC_DIS_15GB') ? 'DISTR 15 GB' : tarifas[index].contains('DISHABLAILIMITADA') ? 'DISHABLA ILIMITADA' : tarifas[index]}',
                                          style: TextStyle(
                                              color: darkBlueAthenea,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'Made Tommy',
                                              fontSize: SizeConfig.screenWidth *
                                                  0.029),
                                        ), */
                                            // BOTON ACTIVAR PROMOCIONES POR LINEA
                                            /* trailing: tarifas[index]
                                                      .contains('DISHABLA') ||
                                                  tarifas[index].contains(
                                                      'DISHABLAILIMITADA') ||
                                                  tarifas[index].contains(
                                                      'TBC_DISTRIBUIDOR')
                                              ? Container(
                                                  height:
                                                      SizeConfig.screenWidth *
                                                          0.10,
                                                  width:
                                                      SizeConfig.screenWidth *
                                                          0.35,
                                                )
                                              : tarifas[index].contains('DIS')
                                                  ? Container(
                                                      decoration: BoxDecoration(
                                                          color: Color.fromRGBO(
                                                              33, 53, 106, 1.0),
                                                          borderRadius:
                                                              new BorderRadius
                                                                      .all(
                                                                  Radius
                                                                      .circular(
                                                                          5))),
                                                      height: SizeConfig
                                                              .screenWidth *
                                                          0.10,
                                                      width: SizeConfig
                                                              .screenWidth *
                                                          0.35,
                                                      child: MaterialButton(
                                                        onPressed: () {
                                                          pr.show();
                                                          contratarBono(
                                                              lineasClienteFinal[
                                                                      index]
                                                                  .peticion);
                                                        },
                                                        child: Center(
                                                          child: Text(
                                                            'Bono 2Gb Gratis',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  : Container(
                                                      height: SizeConfig
                                                              .screenWidth *
                                                          0.10,
                                                      width: SizeConfig
                                                              .screenWidth *
                                                          0.35,
                                                    ) */
                                          ),
                                        );
                                      }),
                                ),
                        )
                      ],
                    ),
                  ),
            //DRAWER
            drawer: Container(
              width: 36 * SizeConfig.heightMultiplier,
              child: Drawer(
                child: ListView(
                  children: <Widget>[
                    DrawerHeader(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Container(
                            /*   height: SizeConfig.screenWidth * 0.15,
                        width: SizeConfig.screenWidth * 0.46, */
                            height: 10 * SizeConfig.heightMultiplier,
                            child: Image(
                              image: AssetImage('assets/images/logomesa.png'),
                              width: 30 * SizeConfig.heightMultiplier,
                            ),
                          ),
                          /*   SizedBox(
                            height: 1 * SizeConfig.heightMultiplier,
                          ), */
                          /*  Row(
                            children: <Widget>[
                              //NUEVO 21/6/23 COMENTADO ESTA LINEA
                              /*  Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text('Bienvenido \n',
                                      style: TextStyle(
                                        color: darkBlueAthenea,
                                        fontFamily: 'Made Tommy',
                                        fontSize: 1.2 * SizeConfig.heightMultiplier,
                                      )),
                                  cliente == null
                                      ? Container()
                                      : Text(cliente.name,
                                          textAlign: TextAlign.justify,
                                          style: TextStyle(
                                              color: darkBlueAthenea,
                                              fontFamily: 'Made Tommy',
                                              fontSize: 1.2 * SizeConfig.heightMultiplier,
                                              fontWeight: FontWeight.bold))
                                ],
                              ), */
                            ],
                          ), */
                        ],
                      ),
                    ),
                    /*    SizedBox(
                      height: 1 * SizeConfig.heightMultiplier,
                    ), */
                    Divider(
                      color: yellowAthenea,
                      thickness: 1,
                    ),
                    ListTile(
                      leading: Icon(
                        Icons.account_circle,
                        color: lightBlueAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      title: Text('Tus datos',
                          style: TextStyle(
                            fontSize: 1.6 * SizeConfig.heightMultiplier,
                            color: darkBlueAthenea,
                          )),
                      onTap: () {
                        Navigator.pushNamed(context, 'tusdatos');
                      },
                    ),
                    ListTile(
                      leading: Icon(
                        Icons.cached,
                        color: lightBlueAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      title: Text('Tu consumo',
                          style: TextStyle(
                            fontSize: 1.6 * SizeConfig.heightMultiplier,
                            color: darkBlueAthenea,
                          )),
                      onTap: () {
                        Navigator.pushNamed(context, 'consumo');
                      },
                    ),
                    ListTile(
                      leading: Icon(
                        Icons.view_column,
                        color: lightBlueAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      title: Text('Detalle de llamadas',
                          style: TextStyle(
                            fontSize: 1.6 * SizeConfig.heightMultiplier,
                            color: darkBlueAthenea,
                          )),
                      onTap: () {
                        Navigator.pushNamed(context, 'detalle');
                      },
                    ),
                    ListTile(
                      leading: Icon(
                        Icons.settings,
                        color: lightBlueAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      title: Text('Configurar servicios',
                          style: TextStyle(
                            fontSize: 1.6 * SizeConfig.heightMultiplier,
                            color: darkBlueAthenea,
                          )),
                      onTap: () {
                        Navigator.pushNamed(context, 'configuracion');
                      },
                    ),
                    ListTile(
                      leading: Icon(
                        Icons.lock,
                        color: lightBlueAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      title: Text('Cambiar contraseña',
                          style: TextStyle(
                            fontSize: 1.6 * SizeConfig.heightMultiplier,
                            color: darkBlueAthenea,
                          )),
                      onTap: () {
                        Navigator.pushNamed(context, 'cambiarpassword');
                      },
                    ),
                    ListTile(
                      leading: Icon(
                        Icons.info,
                        color: lightBlueAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      title: Text('Acerca de',
                          style: TextStyle(
                            fontSize: 1.6 * SizeConfig.heightMultiplier,
                            color: darkBlueAthenea,
                          )),
                      onTap: () {
                        Navigator.pushNamed(context, 'about');
                      },
                    ),
                    Divider(color: yellowAthenea, thickness: 1),
                    ListTile(
                      leading: Icon(
                        Icons.exit_to_app,
                        color: lightBlueAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      title: Text('Salir',
                          style: TextStyle(
                            fontSize: 1.6 * SizeConfig.heightMultiplier,
                            color: darkBlueAthenea,
                          )),
                      onTap: () {
                        _onWillPop();
                      },
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      });
    });
  }

  Future<bool> _onWillPop() {
    return showDialog(
          barrierDismissible: true,
          context: context,
          builder: (context) => new AlertDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
            content: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Icon(
                  Icons.exit_to_app,
                  size: 5 * SizeConfig.heightMultiplier,
                  color: lightBlueAthenea,
                ),
                SizedBox(width: 2 * SizeConfig.heightMultiplier),
                Expanded(
                  child: Text(
                    "¿Quiere Salir?",
                    style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                  ),
                ),
              ],
            ),
            actions: <Widget>[
              new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                  _scaffoldKey.currentState.openEndDrawer();
                },
                child: new Text('No',
                    style: TextStyle(
                      color: yellowAthenea,
                      fontSize: 2 * SizeConfig.heightMultiplier,
                    )),
              ),
              new FlatButton(
                onPressed: () {
                  exit(0);
                },
                child: new Text('Si',
                    style: TextStyle(
                      color: yellowAthenea,
                      fontSize: 2 * SizeConfig.heightMultiplier,
                    )),
              ),
            ],
          ),
        ) ??
        false;
  }
}
