import 'dart:async';

import 'package:comunicacionesatheneacenfor/constants.dart';
import 'package:comunicacionesatheneacenfor/pages/home_page.dart';
import 'package:comunicacionesatheneacenfor/providers/clientes_info_provider.dart';
import 'package:comunicacionesatheneacenfor/widgets/size_config.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:xml2json/xml2json.dart';

class LoginForm extends StatefulWidget {
  LoginForm({Key key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  //VARIABLES QUE VAMOS A USAR
  String _password = '';
  String _userName = '';
  var _caracteres = 0;
  var myControllerPass = TextEditingController();
  var myControllerUsuario = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _obscureText = true;

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  @override
  void initState() {
    //INICIAMOS LOS METODOS QUE RECOGEN USUARIO Y CONTRASEÑA DE SHAREDPREFERENCES
    getUsuarioValuesSP();
    getPassValuesSP();
    initConnectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    myControllerPass.dispose();
    myControllerUsuario.dispose();

    super.dispose();
  }

  //AÑADIR NOMBRE DE USUARIO A SHAREDPREFERENCES
  addUsuarioToSP() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('usuario', _userName);
  }

  //AÑADIR PASSWORD DE USUARIO A SHAREDPREFERENCES
  addPassToSP() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('pass', _password);
  }

  //RECOGER NOMBRE DE USUARIO
  getUsuarioValuesSP() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    String usuarioSP = prefs.getString('usuario');
    myControllerUsuario.text = usuarioSP;
    _userName = usuarioSP;
  }

  //RECOGER PASSWORD DE USUARIO
  getPassValuesSP() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    String passSP = prefs.getString('pass');
    myControllerPass.text = passSP;
    _password = passSP;
  }

  //METODO PARA VALIDAR PASSWORD
  final passwordValidator = MultiValidator([
    RequiredValidator(errorText: 'Password requerido'),
    /* PatternValidator(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$', errorText: 'Al Menos Mayúscula, Minúscula y Número'), */
    MinLengthValidator(8, errorText: 'Password debe tener 8 dígitos'),
    MaxLengthValidator(8, errorText: 'Password debe tener 8 dígitos'),
  ]);

  //METODO PARA VALIDAR USUARIO
  final userValidator =
      MultiValidator([RequiredValidator(errorText: 'Usuario requerido'), MaxLengthValidator(50, errorText: 'Máximo sobrepasado')]);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(builder: (context, orientation) {
        SizeConfig().init(constraints, orientation);
        return new WillPopScope(
            onWillPop: () async {
              return false;
            },
            child: Form(
              key: _formKey,
              child: Scaffold(
                backgroundColor: whiteAthenea,
                body: SingleChildScrollView(
                  child: Container(
                    height: 100 * SizeConfig.heightMultiplier,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 50 * SizeConfig.imageSizeMultiplier,
                          child: Image.asset('assets/images/logo512.png'),
                        ),
                        SizedBox(
                          height: 1 * SizeConfig.heightMultiplier,
                        ),
                        _usuario(),
                        _passwordIntroducido(),
                        _darAlta(),
                        //NUEVO 22/6/23 DESCOMENTADO ESTA LINEA PARA QUE APAREZCA ESTA OPCION Y LOS TESTER NO DEN POR CULO
                        _recuperarpassword(),
                        _botonEntrar(),
                      ],
                    ),
                  ),
                ),
              ),
            ));
      });
    });
  }

  //METODO PARA TEXTFORMFIELD DE USUARIO
  Widget _usuario() {
    return Padding(
      padding: EdgeInsets.fromLTRB(
        2 * SizeConfig.heightMultiplier,
        1 * SizeConfig.heightMultiplier,
        5 * SizeConfig.heightMultiplier,
        2 * SizeConfig.heightMultiplier,
      ),
      child: TextFormField(
        style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
        keyboardType: TextInputType.text,
        autocorrect: true,
        textCapitalization: TextCapitalization.characters,
        controller: myControllerUsuario,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(1.5 * SizeConfig.heightMultiplier),
          filled: true,
          fillColor: whiteAthenea,
          border: OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea)),
          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea), borderRadius: BorderRadius.circular(3.0)),
          hintText: 'Introduzca su NIF/NIE',
          hintStyle: TextStyle(color: blueAthenea.withOpacity(0.5)),
          labelText: 'Usuario',
          labelStyle: TextStyle(color: blueAthenea, fontWeight: FontWeight.bold),
          icon: Icon(
            Icons.account_circle,
            color: lightBlueAthenea,
            size: 3 * SizeConfig.heightMultiplier,
          ),
        ),
        onChanged: (myControllerUsuario) {
          setState(() {
            _userName = myControllerUsuario;
          });
        },
        validator: userValidator,
      ),
    );
  }

  //METODO PARA TEXTFORMFIELD DE PASSWORD
  Widget _passwordIntroducido() {
    return Padding(
      padding: EdgeInsets.fromLTRB(
        2 * SizeConfig.heightMultiplier,
        1 * SizeConfig.heightMultiplier,
        5 * SizeConfig.heightMultiplier,
        2 * SizeConfig.heightMultiplier,
      ),
      child: TextFormField(
        style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
        controller: myControllerPass,
        keyboardType: TextInputType.text,
        obscureText: _obscureText,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(1.5 * SizeConfig.heightMultiplier),
          filled: true,
          fillColor: whiteAthenea,
          border: OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea)),
          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea), borderRadius: BorderRadius.circular(3.0)),
          counter: Text('Caracteres: ' + contarPassword(),
              style: TextStyle(
                color: darkBlueAthenea,
                fontSize: 1.5 * SizeConfig.heightMultiplier,
              )),
          hintText: 'Introduzca su Password',
          hintStyle: TextStyle(
            color: blueAthenea.withOpacity(0.5),
          ),
          labelText: 'Password',
          labelStyle: TextStyle(color: blueAthenea, fontWeight: FontWeight.bold),
          icon: Icon(
            _obscureText ? Icons.lock : Icons.lock_open,
            color: lightBlueAthenea,
            size: 3 * SizeConfig.heightMultiplier,
          ),
        ),
        onChanged: (myControllerPass) {
          setState(() {
            _password = myControllerPass;
          });
        },
        //MOSTRAR/OCULTAR PASSWORD
        onTap: () {
          setState(() {
            _obscureText = !_obscureText;
          });
        },
        validator: passwordValidator,
      ),
    );
  }

  //METODO PARA CONTAR CARACTERES DE TEXTFORMFIELD DE PASSWORD
  String contarPassword() {
    if (_password == null) {
      return _caracteres.toString();
    } else {
      return _password.length.toString();
    }
  }

  //METODO QUE ES UN BOTÓN QUE LLEVA A LA VISTA DE DAR ALTA
  Widget _darAlta() {
    return FlatButton(
      onPressed: () {
        Navigator.pushNamed(context, 'altacliente');
      },
      child: Text(
        'Date de alta',
        style: TextStyle(
            decoration: TextDecoration.underline,
            fontSize: 2 * SizeConfig.heightMultiplier,
            fontWeight: FontWeight.bold,
            color: blueAthenea),
      ),
    );
  }

  //METODO QUE LLEVA A LA VISTA DE RECUPERAR PASSWORD
  Widget _recuperarpassword() {
    return FlatButton(
      onPressed: () {
        Navigator.pushNamed(context, 'recuperarpassword');
      },
      child: Text(
        'Recuperar contraseña',
        style: TextStyle(
            decoration: TextDecoration.underline,
            fontSize: 2 * SizeConfig.heightMultiplier,
            fontWeight: FontWeight.bold,
            color: blueAthenea),
      ),
    );
  }

  //BOTÓN QUE LLEVA A HOME SI LAS CREDENCIALES SON CORRECTAS
  Widget _botonEntrar() {
    return Padding(
      padding: EdgeInsets.fromLTRB(
        2 * SizeConfig.heightMultiplier,
        2 * SizeConfig.heightMultiplier,
        2 * SizeConfig.heightMultiplier,
        2 * SizeConfig.heightMultiplier,
      ),
      child: Container(
        width: 18 * SizeConfig.heightMultiplier,
        child: MaterialButton(
          height: 6 * SizeConfig.heightMultiplier,
          shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          onPressed: () async {
            //SI LAS VALIDACIONES SON CORRECTAS LLAMA AL METODO PARA COMPROBAR DATOS
            if (_formKey.currentState.validate()) {
              _comprobarDatos();
              final clientesInfo = Provider.of<ClientesInfoProvider>(context);
              clientesInfo.dni = _userName;
              clientesInfo.password = _password;
            }
          },
          color: yellowAthenea,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Entrar',
                  style: TextStyle(
                    color: whiteAthenea,
                    fontSize: 2.5 * SizeConfig.heightMultiplier,
                  )),
            ],
          ),
        ),
      ),
    );
  }

  //METODO PARA RECOGER TERCERO DE USUARIO
  bool _comprobarDatos() {
    //USUARIO NO VALIDO
    if (_userName == '') {
      /* print("Usuario no válido"); */
      showDialog(
          context: context,
          builder: (context) => new AlertDialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                content: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.error,
                      color: redAthenea,
                      size: 3 * SizeConfig.heightMultiplier,
                    ),
                    SizedBox(width: 2 * SizeConfig.heightMultiplier),
                    Expanded(
                      child: Text(
                        "Usuario no válido. Inténtelo más tarde o póngase en contacto con su operador.",
                        style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                      ),
                    ),
                  ],
                ),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Text('Ok',
                        style: TextStyle(
                          color: yellowAthenea,
                          fontSize: 2 * SizeConfig.heightMultiplier,
                        )),
                  ),
                ],
              ));
    }
    //PASSWORD NO VALIDO
    else if (_password == '' || _password.length < 8) {
      /* print("Password no válido"); */
      showDialog(
          context: context,
          builder: (context) => new AlertDialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                content: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.error,
                      color: redAthenea,
                      size: 3 * SizeConfig.heightMultiplier,
                    ),
                    SizedBox(width: 2 * SizeConfig.heightMultiplier),
                    Expanded(
                      child: Text(
                        "Password no válido. Póngase en contacto con su operador.",
                        style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                      ),
                    ),
                  ],
                ),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Text('Ok',
                        style: TextStyle(
                          color: yellowAthenea,
                          fontSize: 2 * SizeConfig.heightMultiplier,
                        )),
                  ),
                ],
              ));
    }
    //EN CASO DE QUE VAYA OK LLAMA AL METODO GETTERCEROID
    else {
      getTerceroId(_connectionStatus);
      //DEVUELVE TRUE SI OK
      return true;
    }
    //DEVUELVE FALSE
    return false;
  }

  //METODO PARA EXTRAER TERCERO DE USUARIO LLAMANDO A WS
  getTerceroId(String statusConnection) async {
    //GUARDAMOS USUARIO Y PASSWORD EN SHAREDPREFERENCES
    addUsuarioToSP();
    addPassToSP();
    Xml2Json xml2json = new Xml2Json();
    String url = 'http://212.225.255.130:8010/ws/uvcusr/$_userName/$_password';
    /*  print(url); */
    //SI EL DISPOSITIVO NO TIENE INTERNET
    if (statusConnection == 'ConnectivityResult.none') {
      showDialog(
          context: context,
          builder: (context) => new AlertDialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                content: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.error,
                      color: redAthenea,
                      size: 3 * SizeConfig.heightMultiplier,
                    ),
                    SizedBox(width: 2 * SizeConfig.heightMultiplier),
                    Expanded(
                      child: Text(
                        "Error de conexión. Inténtelo más tarde o póngase en contacto con su operador.",
                        style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                      ),
                    ),
                  ],
                ),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Text('Ok',
                        style: TextStyle(
                          color: yellowAthenea,
                          fontSize: 2 * SizeConfig.heightMultiplier,
                        )),
                  ),
                ],
              ));
    } else {
      //HACEMOS LA LLAMADA Y LO METEMOS EN UN TRY-CATCH
      try {
        http.Response response = await http.get(url);
        xml2json.parse(response.body);

        /* print(response.body); */
        String jsondata = xml2json.toGData();

        Map<String, dynamic> data = convert.jsonDecode(jsondata);

        String id = data['result']['tercero']['id'];
        /* print(id); */
        //SI EL USUARIO ES CORRECTO PERO EL PASSWORD NO
        if (id == 'FALSE') {
          showDialog(
            context: context,
            builder: (context) => new AlertDialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
              content: new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    Icons.error,
                    color: redAthenea,
                    size: 3 * SizeConfig.heightMultiplier,
                  ),
                  SizedBox(width: 2 * SizeConfig.heightMultiplier),
                  Expanded(
                    child: Container(
                      child: Text(
                        'Contraseña Incorrecta',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 2 * SizeConfig.heightMultiplier,
                          color: darkBlueAthenea,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text('Ok',
                      style: TextStyle(
                        color: yellowAthenea,
                        fontSize: 2 * SizeConfig.heightMultiplier,
                      )),
                ),
              ],
            ),
          );
        }
        //SI LLEGA A 5 INTENTOS EL USUARIO SE BLOQUEA
        else if (id == 'E001: Intentos igual 5') {
          showDialog(
            context: context,
            builder: (context) => new AlertDialog(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
              content: new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    Icons.error,
                    color: redAthenea,
                    size: 3 * SizeConfig.heightMultiplier,
                  ),
                  SizedBox(width: 2 * SizeConfig.heightMultiplier),
                  Expanded(
                    child: Container(
                      child: Text(
                        'USUARIO BLOQUEADO. \n Póngase en contacto con su operador.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 2 * SizeConfig.heightMultiplier,
                          color: darkBlueAthenea,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text('Ok',
                      style: TextStyle(
                        color: yellowAthenea,
                        fontSize: 2 * SizeConfig.heightMultiplier,
                      )),
                ),
              ],
            ),
          );
        }
        //SI OK GUARDAMOS EN SHAREDPREFERENCES EL TERCERO DE CLIENTEINFO Y LLEVAMOS AL USUARIO A HOME
        else {
          final clientesInfo = Provider.of<ClientesInfoProvider>(context);
          clientesInfo.tercero = id;
          /* print('final tercero: ' + clientesInfo.tercero); */

          Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => HomePage(
              tercero: id,
            ),
          ));
        }
      } catch (e) {
        //SI SALTA EL CATCH
        /* print("Error en login."); */
        showDialog(
            context: context,
            builder: (context) => new AlertDialog(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                  content: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        Icons.error,
                        color: redAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      SizedBox(width: 2 * SizeConfig.heightMultiplier),
                      Expanded(
                        child: Text(
                          "Error de usuario. Inténtelo más tarde o póngase en contacto con su operador.",
                          style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                        ),
                      ),
                    ],
                  ),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: new Text('Ok',
                          style: TextStyle(
                            color: yellowAthenea,
                            fontSize: 2 * SizeConfig.heightMultiplier,
                          )),
                    ),
                  ],
                ));
        /*  print(e); */
      }
    }
  }

  //METODO PARA COMPROBAR CONECTIVIDAD DEL DISPOSITIVO
  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
      case ConnectivityResult.none:
        setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get connectivity.');
        break;
    }
  }
}
