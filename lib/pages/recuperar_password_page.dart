import 'package:comunicacionesatheneacenfor/atheneacenfor.dart';
import 'package:comunicacionesatheneacenfor/constants.dart';
import 'package:comunicacionesatheneacenfor/widgets/size_config.dart';
import 'package:flutter/material.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';
import 'package:mailer/smtp_server/gmail.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:xml2json/xml2json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class RecuperarPassword extends StatefulWidget {
  @override
  _RecuperarPasswordState createState() => _RecuperarPasswordState();
}

class _RecuperarPasswordState extends State<RecuperarPassword> {
  //VARIABLES QUE VAMOS A USAR
  final _dniController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  //String _ip = 'Unknown';
  ProgressDialog pr;

  @override
  void initState() {
    super.initState();
    //initPlatformState();
  }

  /* Future<void> initPlatformState() async {
    String ipAddress;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      ipAddress = await GetIp.ipAddress;
    } on PlatformException {
      ipAddress = 'Failed to get ipAdress.';
    }

    if (!mounted) return;

    setState(() {
      _ip = ipAddress;
    });
  } */

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    pr.style(message: 'Conectando...');
    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(builder: (context, orientation) {
        SizeConfig().init(constraints, orientation);
        return Scaffold(
            body: Center(
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: blueAthenea,
              iconTheme: IconThemeData(
                color: yellowAthenea,
              ),
              title: Text('Recuperación de contraseña',
                  style: TextStyle(
                    fontSize: 2.5 * SizeConfig.heightMultiplier,
                    color: whiteAthenea,
                  )),
            ),
            body: Form(
              key: _formKey,
              child: Center(
                child: Container(
                  width: 90 * SizeConfig.widthMultiplier,
                  height: 100 * SizeConfig.heightMultiplier,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 4 * SizeConfig.heightMultiplier,
                      ),
                      Text(
                        'Para recuperar la contraseña es necesario que introduzca el número del documento de identidad. Recibirá mediante email la nueva contraseña para acceder.',
                        style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                        textAlign: TextAlign.justify,
                      ),
                      SizedBox(
                        height: 4 * SizeConfig.heightMultiplier,
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                          2 * SizeConfig.heightMultiplier,
                          1 * SizeConfig.heightMultiplier,
                          2 * SizeConfig.heightMultiplier,
                          2 * SizeConfig.heightMultiplier,
                        ),
                        child: TextFormField(
                          style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          controller: _dniController,
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.characters,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(1.5 * SizeConfig.heightMultiplier),
                            filled: true,
                            fillColor: whiteAthenea,
                            border: OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea)),
                            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: lightBlueAthenea), borderRadius: BorderRadius.circular(3.0)),
                            hintText: 'NIF, CIF, NIE...',
                            hintStyle: TextStyle(
                              fontSize: 2 * SizeConfig.heightMultiplier,
                              color: blueAthenea.withOpacity(0.5),
                            ),
                            labelText: 'Documento de identidad',
                            labelStyle:
                                TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: blueAthenea, fontWeight: FontWeight.bold),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'No puede estar vacío';
                            }
                            if (value.length < 8) {
                              return 'Formato no válido, reviselo';
                            }
                            if (value.length > 15) {
                              return 'Máximo permitido excedido';
                            }
                            return null;
                          },
                        ),
                      ),
                      SizedBox(
                        height: 4 * SizeConfig.heightMultiplier,
                      ),
                      RaisedButton(
                        color: yellowAthenea,
                        shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                        onPressed: () {
                          // Validate returns true if the form is valid, otherwise false.
                          if (_formKey.currentState.validate()) {
                            // If the form is valid, display a snackbar. In the real world,
                            // you'd often call a server or save the information in a database.
                            pr.show();
                            recuperarPasswordUsuario();
                          }
                        },
                        child: Text(
                          'Recuperar Contraseña',
                          style: TextStyle(
                            fontSize: 2 * SizeConfig.heightMultiplier,
                            color: whiteAthenea,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 4 * SizeConfig.heightMultiplier,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
      });
    });
  }

  //METODO PARA RECUPERAR CONTRASEÑA DEL USUARIO MANDANDO LA NUEVA AL CORREO
  Future recuperarPasswordUsuario() async {
    String usuario = _dniController.text;
    Xml2Json xml2json = new Xml2Json();
    //METEMOS EN UN TRY-CATCH
    try {
      String url = 'http://212.225.255.130:8010/ws/rtvusrope/$usuario';
      http.Response response = await http.get(url);
      xml2json.parse(response.body);
      String jsondata = xml2json.toGData();
      Map<String, dynamic> data = convert.jsonDecode(jsondata);
      /*   print(data); */
      pr.hide();
      //VALIDACION INCORRECTA, DOCUMENTO NO ENCONTRADO
      if (data['result'].containsKey('tercero') == true) {
        showDialog(
            context: context,
            builder: (context) => new AlertDialog(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                  content: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        Icons.error,
                        color: redAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      SizedBox(width: 2 * SizeConfig.heightMultiplier),
                      Expanded(
                        child: Text(
                          "Validación incorrecta, Documento no encontrado",
                          style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                        ),
                      ),
                    ],
                  ),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: new Text('Ok',
                          style: TextStyle(
                            color: yellowAthenea,
                            fontSize: 2 * SizeConfig.heightMultiplier,
                          )),
                    ),
                  ],
                ));
      }
      //EXITO
      else {
        String resultMail = data['result']['Registro']['email'];
        String resultNewPass = data['result']['Registro']['pass'];
        //LLAMA AL METODO QUE ENVIA CORREO AL USUARIO
        sendmail(resultMail, resultNewPass, usuario);
      }
    }
    //SALTA EL CATCH
    catch (e) {
      /*    print("Error de conexión, inténtelo más tarde");
      print(e); */
      pr.hide();
      showDialog(
          context: context,
          builder: (context) => new AlertDialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                content: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.error,
                      color: redAthenea,
                      size: 3 * SizeConfig.heightMultiplier,
                    ),
                    SizedBox(width: 2 * SizeConfig.heightMultiplier),
                    Expanded(
                      child: Text(
                        "Error de usuario. Inténtelo más tarde o póngase en contacto con su operador.",
                        style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                      ),
                    ),
                  ],
                ),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Text('Ok',
                        style: TextStyle(
                          color: darkBlueAthenea,
                          fontSize: 2 * SizeConfig.heightMultiplier,
                        )),
                  ),
                ],
              ));
    }
  }

  void sendmail(String resultMail, String resultNewPass, String usuario) async {
    /* print('EMAIL DONDE ENVIAR: $resultMail');
    print('NUEVA PASS: $resultNewPass');
    print('USUARIO: $usuario'); */

    /* String username = 'hola@mmovilnetwork.com';
    String password = 'marketing23team@m'; */

    String username = 'hola@mmovilnetwork.com';
    String password = 'bzrukfomyuixwfyw';

    final smtpServer = atheneacenfor(username, password);

    final message = Message()
      ..from = Address(username, 'MiMundoMOVIL')
      ..recipients.add('$resultMail')
      //..ccRecipients.addAll(['destCc1@example.com', 'destCc2@example.com'])
      //..bccRecipients.add(Address('bccAddress@example.com'))
      ..subject = 'Cambio de Clave'
      ..html =
          ''' <p>Estimado usuario, se ha reseteado tu clave de acceso, tus nuevas credenciales son:<br><br><strong>DNI: $usuario<br>Clave: $resultNewPass</strong><br><br>Por seguridad, te recomendamos que cambies la clave una vez que accedas a la app.</p><p style="color:#777;"><small><i>Este es un correo automatizado, no respondas al remitente.<i></small></p>''';

    try {
      final sendReport = await send(message, smtpServer);
      print('Message sent: ' + sendReport.toString());
      //LUCAS NUEVO 5/11/21 - AÑADIDO TEXTO Y FORMATO AL RECUPERAR CONTRASEÑA EN SHOWDIALOG
      showDialog(
          context: context,
          builder: (context) => new AlertDialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                content: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.error,
                      color: redAthenea,
                      size: 3 * SizeConfig.heightMultiplier,
                    ),
                    SizedBox(width: 2 * SizeConfig.heightMultiplier),
                    Expanded(
                      child: Text(
                        "Contraseña reseteada con éxito. Revisa tu correo electrónico $resultMail y sigue las instrucciones. Pueden transcurrir unos minutos hasta que el correo llegue a tu bandeja de entrada. Recuerda revistar también tu carpeta de correos no deseados.",
                        style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                      ),
                    ),
                  ],
                ),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () async {
                      Navigator.pushNamed(context, 'login');
                      SharedPreferences prefs = await SharedPreferences.getInstance();
                      prefs.remove('pass');
                    },
                    child: new Text('Ok',
                        style: TextStyle(
                          color: yellowAthenea,
                          fontSize: 2 * SizeConfig.heightMultiplier,
                        )),
                  ),
                ],
              ));
    } on MailerException catch (e) {
      print('Message not sent.');
      print(e);
      for (var p in e.problems) {
        print('Problem: ${p.code}: ${p.msg}');
      }
    }
  }
}
