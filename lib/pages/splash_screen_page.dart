import 'dart:async';

import 'package:comunicacionesatheneacenfor/constants.dart';
import 'package:comunicacionesatheneacenfor/pages/login_form.dart';
import 'package:comunicacionesatheneacenfor/widgets/size_config.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 3),
        () => Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => LoginForm())));
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);
            return Scaffold(
                body: Container(
              color: Colors.white,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
                Column(
                  children: [
                    Image.asset(
                      'assets/images/logo512.png',
                      height: 30 * SizeConfig.heightMultiplier,
                      width: 30 * SizeConfig.heightMultiplier,
                    ),
                    SizedBox(
                      height: 1 * SizeConfig.heightMultiplier,
                    ),
                    RichText(
                      text: TextSpan(
                        text: 'Para los que valoran un buen servicio ',
                        style: TextStyle(fontSize: 2.4 * SizeConfig.heightMultiplier, color: blueAthenea),
                        children: <TextSpan>[
                          TextSpan(
                            text: '\n nosotros somos su empresa. ',
                            style:
                                TextStyle(fontSize: 2.4 * SizeConfig.heightMultiplier, color: yellowAthenea, fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: '\n - ',
                            style:
                                TextStyle(fontSize: 2.4 * SizeConfig.heightMultiplier, color: yellowAthenea, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ]),
            ));
          },
        );
      },
    );
  }
}
