import 'package:comunicacionesatheneacenfor/constants.dart';
import 'package:comunicacionesatheneacenfor/providers/clientes_info_provider.dart';
import 'package:comunicacionesatheneacenfor/widgets/size_config.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TusDatosPage extends StatefulWidget {
  TusDatosPage({Key key}) : super(key: key);

  @override
  State<TusDatosPage> createState() => _TusDatosPageState();
}

class _TusDatosPageState extends State<TusDatosPage> {
  String nombreA, dniA, poblacionA, provinciaA;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final clienteProvider = Provider.of<ClientesInfoProvider>(context);
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(builder: (context, orientation) {
          SizeConfig().init(constraints, orientation);
          return Scaffold(
            appBar: AppBar(
              backgroundColor: blueAthenea,
              iconTheme: IconThemeData(
                color: yellowAthenea,
              ),
              title: Text('Tus datos',
                  style: TextStyle(
                    fontSize: 2 * SizeConfig.heightMultiplier,
                    color: whiteAthenea,
                  )),
            ),
            body: Padding(
              padding: EdgeInsets.fromLTRB(
                1 * SizeConfig.heightMultiplier,
                1 * SizeConfig.heightMultiplier,
                1 * SizeConfig.heightMultiplier,
                1 * SizeConfig.heightMultiplier,
              ),
              child: SingleChildScrollView(
                  child: Column(
                children: <Widget>[
                  _buildHeaderText(),
                  SizedBox(
                    height: 1 * SizeConfig.heightMultiplier,
                  ),
                  _buildField(
                    clienteProvider.nombre,
                    'Nombre completo',
                    darkBlueAthenea,
                  ),
                  SizedBox(
                    height: 1 * SizeConfig.heightMultiplier,
                  ),
                  _buildField(
                    clienteProvider.dni,
                    'Identificación Fiscal',
                    darkBlueAthenea,
                  ),
                  SizedBox(
                    height: 1 * SizeConfig.heightMultiplier,
                  ),
                  _buildField(
                    clienteProvider.poblacion,
                    'Población',
                    darkBlueAthenea,
                  ),
                  SizedBox(
                    height: 1 * SizeConfig.heightMultiplier,
                  ),
                  _buildField(
                    clienteProvider.provincia,
                    'Provincia',
                    darkBlueAthenea,
                  ),
                  SizedBox(
                    height: 1 * SizeConfig.heightMultiplier,
                  ),
                ],
              )),
            ),
          );
        });
      },
    );
  }

  Widget _buildHeaderText() {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 8.0),
        child: Text(
          'Si necesitas cambiar alguno de tus datos contacta con atención al cliente para gestionar tu solicitud.',
          textAlign: TextAlign.justify,
          style: TextStyle(
            color: darkBlueAthenea,
            fontSize: 2 * SizeConfig.heightMultiplier,
          ),
        ));
  }

  Widget _buildField(String initialValue, String labelText, Color colorTC) {
    return TextFormField(
      initialValue: initialValue,
      enabled: false,
      style: TextStyle(
        color: darkBlueAthenea,
        fontSize: 2 * SizeConfig.heightMultiplier,
      ),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(1 * SizeConfig.heightMultiplier),
        labelText: labelText,
        labelStyle: TextStyle(color: blueAthenea, fontSize: 2 * SizeConfig.heightMultiplier, fontWeight: FontWeight.bold),
        disabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: yellowAthenea),
        ),
      ),
      textCapitalization: TextCapitalization.words,
    );
  }
}
