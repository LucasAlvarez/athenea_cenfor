import 'package:flutter/material.dart';

class ClientesInfoProvider with ChangeNotifier {
  String _tercero = '';
  String _dni = '';
  String _password = '';
  String _nombre = '';
  String _poblacion = '';
  String _provincia = '';
  //String _token = '';

  get tercero {
    return _tercero;
  }

  get dni {
    return _dni;
  }

  get password {
    return _password;
  }

  get nombre {
    return _nombre;
  }

  get poblacion {
    return _poblacion;
  }

  get provincia {
    return _provincia;
  }
  //get token     {return _token;}

  set tercero(String newTercero) {
    this._tercero = newTercero;
    notifyListeners();
  }

  set dni(String newDni) {
    this._dni = newDni;
    notifyListeners();
  }

  set password(String newPassword) {
    this._password = newPassword;
    notifyListeners();
  }

  //LUCAS NUEVO 25/1/22 -- NUEVO METODO PARA QUE RECOJA EL CAMBIO DE PASSWORD
  setPassword(String newPassword) {
    this._password = newPassword;
    notifyListeners();
  }

  set nombre(String newNombre) {
    this._nombre = newNombre;
    notifyListeners();
  }

  set poblacion(String newPoblacion) {
    this._poblacion = newPoblacion;
    notifyListeners();
  }

  set provincia(String newProvincia) {
    this._provincia = newProvincia;
    notifyListeners();
  }
}
