import 'package:flutter/material.dart';

class ConsumoProvider with ChangeNotifier {
  double _bonoDatos;
  double _balanceDatos;
  double _consumoDatos;
  double _masMegasDatos;
  String _tarifaMovil;
  String _tipollamada;
  double _consumoMovil;
  String _tipoRed;
  String _bonoMovil;
  double _roscoDatosLlamadas;
  double _bonoR;
  double _balanceR;
  double _teQuedan;
  get roscoDatosLlamadas {
    return _roscoDatosLlamadas;
  }

  get tarifaMovil {
    return _tarifaMovil;
  }

  get tipollamada {
    return _tipollamada;
  }

  get consumoMovil {
    return _consumoMovil;
  }

  get tipoRed {
    return _tipoRed;
  }

  get bonoMovil {
    return _bonoMovil;
  }

  get bonoDatos {
    return _bonoDatos;
  }

  get balanceDatos {
    return _balanceDatos;
  }

  get consumoDatos {
    return _consumoDatos;
  }

  get masMegasDatos {
    return _masMegasDatos;
  }

  get bonoR {
    return _bonoR;
  }

  get balanceR {
    return _balanceR;
  }

  get teQuedan {
    return _teQuedan;
  }

  set roscoDatosLlamadas(double newRoscoDatosLlamadas) {
    this._roscoDatosLlamadas = newRoscoDatosLlamadas;
    notifyListeners();
  }

  set bonoDatos(double newBonoDatos) {
    this._bonoDatos = newBonoDatos;
    notifyListeners();
  }

  set balanceDatos(double newBalanceDatos) {
    this._balanceDatos = newBalanceDatos;
    notifyListeners();
  }

  set consumoDatos(double newConsumoDatos) {
    this._consumoDatos = newConsumoDatos;
    notifyListeners();
  }

  set masMegasDatos(double newMasMegasDatos) {
    this._masMegasDatos = newMasMegasDatos;
    notifyListeners();
  }

  set tarifaMovil(String newTarifaMovil) {
    this._tarifaMovil = newTarifaMovil;
    notifyListeners();
  }

  set tipollamada(String newTipollamada) {
    this._tipollamada = newTipollamada;
    notifyListeners();
  }

  set consumoMovil(double newConsumoMovil) {
    this._consumoMovil = newConsumoMovil;
    notifyListeners();
  }

  set tipoRed(String newTipoRed) {
    this._tipoRed = newTipoRed;
    notifyListeners();
  }

  set bonoMovil(String newBonoMovil) {
    this._bonoMovil = newBonoMovil;
    notifyListeners();
  }

  set bonoR(double newBonoR) {
    this._bonoR = newBonoR;
    notifyListeners();
  }

  set balanceR(double newBalanceR) {
    this._balanceR = newBalanceR;
    notifyListeners();
  }

  set teQuedan(double newTeQuedan) {
    this._teQuedan = newTeQuedan;
    notifyListeners();
  }
}
