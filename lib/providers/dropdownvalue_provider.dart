import 'package:flutter/material.dart';

class DropDownValueProvider with ChangeNotifier {
  String _dropDownValue;

  get dropDownValue {
    return _dropDownValue;
  }

  set dropDownValue(String newDropDownValue) {
    this._dropDownValue = newDropDownValue;
    notifyListeners();
  }
}
