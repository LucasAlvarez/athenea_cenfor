import 'package:flutter/material.dart';

class EstadosFeaturesProvider with ChangeNotifier {
  var _s1;
  var _s2;
  var _s3;
  var _s3parm1;
  var _s4;
  var _s4parm1;
  var _s4parm2;
  var _s5;
  var _s5parm1;
  var _s6;
  var _s6parm1;
  var _s7;
  var _s8;
  var _s9;
  var _s10;
  var _s11;
  var _s12;

  get s1 {
    return _s1;
  }

  get s2 {
    return _s2;
  }

  get s3 {
    return _s3;
  }

  get s3parm1 {
    return _s3parm1;
  }

  get s4 {
    return _s4;
  }

  get s4parm1 {
    return _s4parm1;
  }

  get s4parm2 {
    return _s4parm2;
  }

  get s5 {
    return _s5;
  }

  get s5parm1 {
    return _s5parm1;
  }

  get s6 {
    return _s6;
  }

  get s6parm1 {
    return _s6parm1;
  }

  get s7 {
    return _s7;
  }

  get s8 {
    return _s8;
  }

  get s9 {
    return _s9;
  }

  get s10 {
    return _s10;
  }

  get s11 {
    return _s11;
  }

  get s12 {
    return _s12;
  }

  set s1(var newS1) {
    this._s1 = newS1;
    notifyListeners();
  }

  set s2(var newS2) {
    this._s2 = newS2;
    notifyListeners();
  }

  set s3(var newS3) {
    this._s3 = newS3;
    notifyListeners();
  }

  set s3parm1(var newS3parm1) {
    this._s3parm1 = newS3parm1;
    notifyListeners();
  }

  set s4(var newS4) {
    this._s4 = newS4;
    notifyListeners();
  }

  set s4parm1(var newS4parm1) {
    this._s4parm1 = newS4parm1;
    notifyListeners();
  }

  set s4parm2(var newS4parm2) {
    this._s4parm2 = newS4parm2;
    notifyListeners();
  }

  set s5(var newS5) {
    this._s5 = newS5;
    notifyListeners();
  }

  set s5parm1(var newS5parm1) {
    this._s5parm1 = newS5parm1;
    notifyListeners();
  }

  set s6(var newS6) {
    this._s6 = newS6;
    notifyListeners();
  }

  set s6parm1(var newS6parm1) {
    this._s6parm1 = newS6parm1;
    notifyListeners();
  }

  set s7(var newS7) {
    this._s7 = newS7;
    notifyListeners();
  }

  set s8(var newS8) {
    this._s8 = newS8;
    notifyListeners();
  }

  set s9(var newS9) {
    this._s9 = newS9;
    notifyListeners();
  }

  set s10(var newS10) {
    this._s10 = newS10;
    notifyListeners();
  }

  set s11(var newS11) {
    this._s11 = newS11;
    notifyListeners();
  }

  set s12(var newS12) {
    this._s12 = newS12;
    notifyListeners();
  }
}
