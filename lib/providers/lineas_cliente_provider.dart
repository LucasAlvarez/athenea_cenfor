import 'package:comunicacionesatheneacenfor/models/linea.dart';
import 'package:flutter/material.dart';

class LineasClienteProvider with ChangeNotifier {
  List<Linea> _lineasCliente = [];

  get lineasCliente {
    return _lineasCliente;
  }

  set lineasCliente(List<Linea> newLineasCliente) {
    this._lineasCliente = newLineasCliente;
    notifyListeners();
  }
}
