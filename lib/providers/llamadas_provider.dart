import 'package:comunicacionesatheneacenfor/models/Llamadas.dart';
import 'package:flutter/material.dart';

class LlamadasProvider with ChangeNotifier {
  List<Llamada> _detalleLlamada = [];

  get detalleLlamada {
    return _detalleLlamada;
  }

  set detalleLlamada(List<Llamada> newDetallesLlamada) {
    this._detalleLlamada = newDetallesLlamada;
    notifyListeners();
  }
}
