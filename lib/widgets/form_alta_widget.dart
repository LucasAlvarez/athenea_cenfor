import 'package:comunicacionesatheneacenfor/constants.dart';
import 'package:comunicacionesatheneacenfor/pages/login_form.dart';
import 'package:comunicacionesatheneacenfor/widgets/size_config.dart';
import 'package:flutter/material.dart';
import 'package:xml2json/xml2json.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class FormAlta extends StatefulWidget {
  @override
  FormAltaState createState() {
    return FormAltaState();
  }
}

class FormAltaState extends State<FormAlta> {
  //VARIABLES QUE VAMOS A USAR
  final _formKey = GlobalKey<FormState>();
  final _dniController = TextEditingController();
  final _telefonoController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  String _password = '';
  String _password2 = '';
  var _caracteres = 0;
  var _caracteres2 = 0;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(builder: (context, orientation) {
        SizeConfig().init(constraints, orientation);
        return Form(
            key: _formKey,
            child: Container(
              width: 90 * SizeConfig.widthMultiplier,
              height: 100 * SizeConfig.heightMultiplier,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    //ICONO CABECERA
                    Padding(
                      padding: EdgeInsets.all(1.5 * SizeConfig.heightMultiplier),
                      child: Icon(
                        Icons.account_circle,
                        color: lightBlueAthenea,
                        size: 12 * SizeConfig.heightMultiplier,
                      ),
                    ),
                    //TEXTFORMFIELD DNI
                    Padding(
                      padding: EdgeInsets.fromLTRB(
                        2 * SizeConfig.heightMultiplier,
                        1 * SizeConfig.heightMultiplier,
                        2 * SizeConfig.heightMultiplier,
                        2 * SizeConfig.heightMultiplier,
                      ),
                      child: TextFormField(
                        style: TextStyle(
                          fontSize: 3 * SizeConfig.heightMultiplier,
                          color: darkBlueAthenea,
                        ),
                        controller: _dniController,
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(1.5 * SizeConfig.heightMultiplier),
                          filled: true,
                          fillColor: whiteAthenea,
                          border: OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea)),
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                          enabledBorder:
                              OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea), borderRadius: BorderRadius.circular(3.0)),
                          hintText: 'NIF, CIF, NIE...',
                          hintStyle: TextStyle(
                            fontSize: 2 * SizeConfig.heightMultiplier,
                            color: blueAthenea.withOpacity(0.5),
                          ),
                          labelText: 'Documento de identidad',
                          labelStyle: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: blueAthenea, fontWeight: FontWeight.bold),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'No puede estar vacío';
                          }
                          if (value.length > 15) {
                            return 'Máximo permitido excedido';
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(
                      height: 1.5 * SizeConfig.heightMultiplier,
                    ),
                    //TEXTFORMFIELD TELEFONO
                    Padding(
                      padding: EdgeInsets.fromLTRB(
                        2 * SizeConfig.heightMultiplier,
                        1 * SizeConfig.heightMultiplier,
                        2 * SizeConfig.heightMultiplier,
                        2 * SizeConfig.heightMultiplier,
                      ),
                      child: TextFormField(
                        style: TextStyle(
                          fontSize: 3 * SizeConfig.heightMultiplier,
                          color: darkBlueAthenea,
                        ),
                        controller: _telefonoController,
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(1.5 * SizeConfig.heightMultiplier),
                          filled: true,
                          fillColor: whiteAthenea,
                          border: OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea)),
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                          enabledBorder:
                              OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea), borderRadius: BorderRadius.circular(3.0)),
                          hintText: 'Teléfono',
                          hintStyle: TextStyle(
                            fontSize: 2 * SizeConfig.heightMultiplier,
                            color: blueAthenea.withOpacity(0.5),
                          ),
                          labelText: 'Número de teléfono',
                          labelStyle: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: blueAthenea, fontWeight: FontWeight.bold),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'No puede estar vacío';
                          }
                          if (value.length != 9) {
                            return 'El teléfono debe tener 9 dígitos';
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(
                      height: 1.5 * SizeConfig.heightMultiplier,
                    ),
                    //TEXTFORMFIELD EMAIL
                    Padding(
                      padding: EdgeInsets.fromLTRB(
                        2 * SizeConfig.heightMultiplier,
                        1 * SizeConfig.heightMultiplier,
                        2 * SizeConfig.heightMultiplier,
                        2 * SizeConfig.heightMultiplier,
                      ),
                      child: TextFormField(
                        style: TextStyle(
                          fontSize: 3 * SizeConfig.heightMultiplier,
                          color: darkBlueAthenea,
                        ),
                        controller: _emailController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(1.5 * SizeConfig.heightMultiplier),
                          filled: true,
                          fillColor: whiteAthenea,
                          border: OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea)),
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                          enabledBorder:
                              OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea), borderRadius: BorderRadius.circular(3.0)),
                          hintText: 'Email',
                          hintStyle: TextStyle(
                            fontSize: 2 * SizeConfig.heightMultiplier,
                            color: blueAthenea.withOpacity(0.5),
                          ),
                          labelText: 'Correo electrónico',
                          labelStyle: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: blueAthenea, fontWeight: FontWeight.bold),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'No puede estar vacío';
                          }
                          if (value.length > 50) {
                            return 'Máximo permitido sobrepasado';
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(
                      height: 1.5 * SizeConfig.heightMultiplier,
                    ),
                    //TEXTFORMFIELD PASSWORD
                    Padding(
                      padding: EdgeInsets.fromLTRB(
                        2 * SizeConfig.heightMultiplier,
                        1 * SizeConfig.heightMultiplier,
                        2 * SizeConfig.heightMultiplier,
                        2 * SizeConfig.heightMultiplier,
                      ),
                      child: TextFormField(
                        style: TextStyle(
                          fontSize: 3 * SizeConfig.heightMultiplier,
                          color: darkBlueAthenea,
                        ),
                        controller: _passwordController,
                        keyboardType: TextInputType.text,
                        obscureText: true,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(1.5 * SizeConfig.heightMultiplier),
                          filled: true,
                          fillColor: whiteAthenea,
                          border: OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea)),
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                          enabledBorder:
                              OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea), borderRadius: BorderRadius.circular(3.0)),
                          counter: Text('Caracteres: ' + contarPassword(),
                              style: TextStyle(
                                fontSize: 1.5 * SizeConfig.heightMultiplier,
                                color: darkBlueAthenea,
                              )),
                          hintText: 'Contraseña',
                          hintStyle: TextStyle(
                            fontSize: 2 * SizeConfig.heightMultiplier,
                            color: blueAthenea.withOpacity(0.5),
                          ),
                          labelText: 'Contraseña (8 caracteres)',
                          labelStyle: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: blueAthenea, fontWeight: FontWeight.bold),
                        ),
                        onChanged: (value) {
                          setState(() {
                            _password = value;
                          });
                        },
                        // The validator receives the text that the user has entered.
                        validator: (_password) {
                          if (_password.isEmpty) {
                            return 'No puede estar vacío';
                          }
                          if (_password.length < 6 || _password.length > 8) {
                            return '8 caracteres alfanuméricos';
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(
                      height: 1.5 * SizeConfig.heightMultiplier,
                    ),
                    //TEXTFORMFIELD PASSWORD 2
                    Padding(
                      padding: EdgeInsets.fromLTRB(
                        2 * SizeConfig.heightMultiplier,
                        1 * SizeConfig.heightMultiplier,
                        2 * SizeConfig.heightMultiplier,
                        2 * SizeConfig.heightMultiplier,
                      ),
                      child: TextFormField(
                        style: TextStyle(
                          fontSize: 3 * SizeConfig.heightMultiplier,
                          color: darkBlueAthenea,
                        ),
                        keyboardType: TextInputType.text,
                        obscureText: true,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(1.5 * SizeConfig.heightMultiplier),
                          filled: true,
                          fillColor: whiteAthenea,
                          border: OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea)),
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                          enabledBorder:
                              OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea), borderRadius: BorderRadius.circular(3.0)),
                          counter: Text('Caracteres: ' + contarPassword2(),
                              style: TextStyle(
                                fontSize: 1.5 * SizeConfig.heightMultiplier,
                                color: darkBlueAthenea,
                              )),
                          hintText: 'Confirmar contraseña',
                          hintStyle: TextStyle(
                            fontSize: 2 * SizeConfig.heightMultiplier,
                            color: blueAthenea.withOpacity(0.5),
                          ),
                          labelText: 'Repetir contraseña',
                          labelStyle: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: blueAthenea, fontWeight: FontWeight.bold),
                        ),
                        onChanged: (value) {
                          setState(() {
                            _password2 = value;
                          });
                        },
                        validator: (_password2) {
                          if (_password2.isEmpty) {
                            return 'Confirme la contraseña';
                          }
                          if (validarPassWord() == false) {
                            return 'Las contraseñas no coinciden';
                          }
                          return null;
                        },
                      ),
                    ),
                    SizedBox(
                      height: 2 * SizeConfig.heightMultiplier,
                    ),
                    //BOTONDARDEALTA
                    RaisedButton(
                      color: yellowAthenea,
                      shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                      onPressed: () {
                        // Validate returns true if the form is valid, otherwise false.
                        if (_formKey.currentState.validate()) {
                          // If the form is valid, display a snackbar. In the real world,
                          // you'd often call a server or save the information in a database.
                          _consultarCliente();
                          Scaffold.of(context).showSnackBar(SnackBar(
                            duration: Duration(seconds: 1),
                            content: Text('Procesando...',
                                style: TextStyle(
                                  color: whiteAthenea,
                                )),
                            backgroundColor: darkBlueAthenea,
                          ));
                        }
                      },
                      child: Text(
                        'Date de alta',
                        style: TextStyle(
                          fontSize: 2.5 * SizeConfig.heightMultiplier,
                          color: whiteAthenea,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ));
      });
    });
  }

  String contarPassword() {
    if (_password == null) {
      return _caracteres.toString();
    } else {
      return _password.length.toString();
    }
  }

  String contarPassword2() {
    if (_password2 == null) {
      return _caracteres2.toString();
    } else {
      return _password2.length.toString();
    }
  }

  bool validarPassWord() {
    if (_password2 != _password) {
      return false;
    } else
      return true;
  }

  //METODO PARA COMPROBAR
  void _consultarCliente() async {
    String usuario = _dniController.text;
    String telefono = _telefonoController.text;
    String email = _emailController.text;
    String clave = _passwordController.text;

    /* print('datos introducidos: $usuario, $telefono, $email, $clave'); */
    Xml2Json xml2json = new Xml2Json();
    //HACEMOS LA LLAMADA Y LO METEMOS EN UN TRY-CATCH
    try {
      String url = 'http://212.225.255.130:8010/ws/accesoope/$usuario/$telefono/$email/$clave';
      http.Response response = await http.get(url);
      xml2json.parse(response.body);
      String jsondata = xml2json.toGData();

      Map<String, dynamic> data = convert.jsonDecode(jsondata);

      /* print(jsondata); */
      String id = data['result']['tercero']['id'];
      print(id);
      //USUARIO REGISTRADO CORRECTAMENTE
      if (id == 'True') {
        showDialog(
            context: context,
            builder: (context) => new AlertDialog(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                  content: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        Icons.error,
                        color: redAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      SizedBox(width: 2 * SizeConfig.heightMultiplier),
                      Expanded(
                        child: Text(
                          "Usuario registrado correctamente.",
                          style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                        ),
                      ),
                    ],
                  ),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => LoginForm(),
                            ));
                      },
                      child: new Text('Ok',
                          style: TextStyle(
                            color: yellowAthenea,
                            fontSize: 2 * SizeConfig.heightMultiplier,
                          )),
                    ),
                  ],
                ));
      } else {
        //YA EXISTE EL USUARIO
        if (id == 'E1') {
          Scaffold.of(context).hideCurrentSnackBar();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Usuario ya registrado. Debe loguearse para entrar.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: yellowAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
        //TELEFONO NO ENCONTRADO
        if (id == 'E2') {
          Scaffold.of(context).hideCurrentSnackBar();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Teléfono no encontrado. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: yellowAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
        //ERROR DOCUMENTO DE IDENTIDAD
        if (id == 'E3') {
          Scaffold.of(context).hideCurrentSnackBar();
          showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                    content: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.error,
                          color: redAthenea,
                          size: 3 * SizeConfig.heightMultiplier,
                        ),
                        SizedBox(width: 2 * SizeConfig.heightMultiplier),
                        Expanded(
                          child: Text(
                            "Error documento de identidad. Inténtelo más tarde o póngase en contacto con su operador.",
                            style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                          ),
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: new Text('Ok',
                            style: TextStyle(
                              color: yellowAthenea,
                              fontSize: 2 * SizeConfig.heightMultiplier,
                            )),
                      ),
                    ],
                  ));
        }
      }
    }
    //SI SALTA EL CATCH
    catch (e) {
      /*  print("Error al dar de alta");
      print(e); */
      showDialog(
          context: context,
          builder: (context) => new AlertDialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                content: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.error,
                      color: redAthenea,
                      size: 3 * SizeConfig.heightMultiplier,
                    ),
                    SizedBox(width: 2 * SizeConfig.heightMultiplier),
                    Expanded(
                      child: Text(
                        "Error de usuario. Inténtelo más tarde o póngase en contacto con su operador.",
                        style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                      ),
                    ),
                  ],
                ),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Text('Ok',
                        style: TextStyle(
                          color: yellowAthenea,
                          fontSize: 2 * SizeConfig.heightMultiplier,
                        )),
                  ),
                ],
              ));
    }
  }
}
