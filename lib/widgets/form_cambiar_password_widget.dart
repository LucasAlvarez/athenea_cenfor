import 'dart:convert';
import 'package:comunicacionesatheneacenfor/atheneacenfor.dart';
import 'package:comunicacionesatheneacenfor/constants.dart';
import 'package:comunicacionesatheneacenfor/pages/tus_datos_page.dart';
import 'package:comunicacionesatheneacenfor/providers/clientes_info_provider.dart';
import 'package:comunicacionesatheneacenfor/widgets/size_config.dart';
import 'package:flutter/material.dart';
import 'package:mailer/mailer.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:xml2json/xml2json.dart';
import 'dart:convert' as convert;
import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart' as crypto;

class FormCambiarPassword extends StatefulWidget {
  FormCambiarPassword({
    Key key,
  }) : super(key: key);

  @override
  State<FormCambiarPassword> createState() => _FormCambiarPasswordState();
}

class _FormCambiarPasswordState extends State<FormCambiarPassword> {
  ProgressDialog pr;
  final _formKey = GlobalKey<FormState>();
  final _passwordController = TextEditingController();
  final _passwordNewController = TextEditingController();
  final _passwordNew2Controller = TextEditingController();
  String _ip = 'Unknown';
  String _password = '';
  String _passwordNew = '';
  String _passwordNew2 = '';
  var _caracteres = 0;
  var _caracteres2 = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);

    pr.style(message: 'Conectando...');
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(builder: (context, orientation) {
          SizeConfig().init(constraints, orientation);
          return Form(
              key: _formKey,
              child: SingleChildScrollView(
                  child: Container(
                      width: 90 * SizeConfig.widthMultiplier,
                      height: 110 * SizeConfig.heightMultiplier,
                      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
                        //ICONO CABECERA
                        Padding(
                          padding: EdgeInsets.all(1.5 * SizeConfig.heightMultiplier),
                          child: Icon(
                            Icons.lock_outline,
                            color: lightBlueAthenea,
                            size: 12 * SizeConfig.heightMultiplier,
                          ),
                        ),
                        //TEXTFORMFIELD PASSWORD
                        Padding(
                          padding: EdgeInsets.fromLTRB(
                            2 * SizeConfig.heightMultiplier,
                            1 * SizeConfig.heightMultiplier,
                            2 * SizeConfig.heightMultiplier,
                            2 * SizeConfig.heightMultiplier,
                          ),
                          child: TextFormField(
                            style: TextStyle(fontSize: 3 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                            controller: _passwordController,
                            keyboardType: TextInputType.text,
                            obscureText: true,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(1.5 * SizeConfig.heightMultiplier),
                              filled: true,
                              fillColor: whiteAthenea,
                              border: OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea)),
                              focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: lightBlueAthenea), borderRadius: BorderRadius.circular(3.0)),
                              counter: Text('Caracteres: ' + contarPassword(),
                                  style: TextStyle(
                                    fontSize: 2 * SizeConfig.heightMultiplier,
                                    color: darkBlueAthenea,
                                  )),
                              hintText: 'Introduzca su password',
                              hintStyle: TextStyle(
                                color: blueAthenea.withOpacity(0.5),
                              ),
                              labelText: 'Contraseña actual',
                              labelStyle:
                                  TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: blueAthenea, fontWeight: FontWeight.bold),
                            ),
                            onChanged: (value) {
                              setState(() {
                                _password = value;
                              });
                            },
                            // The validator receives the text that the user has entered.
                            validator: (_password) {
                              if (_password.isEmpty) {
                                return 'No puede estar vacío';
                              }
                              if (_password.length < 6 || _password.length > 8) {
                                return '8 caracteres alfanuméricos';
                              }
                              return null;
                            },
                          ),
                        ),
                        //TEXTFORMFIELD PASSWORD NEW
                        Padding(
                          padding: EdgeInsets.fromLTRB(
                            2 * SizeConfig.heightMultiplier,
                            1 * SizeConfig.heightMultiplier,
                            2 * SizeConfig.heightMultiplier,
                            2 * SizeConfig.heightMultiplier,
                          ),
                          child: TextFormField(
                            style: TextStyle(fontSize: 3 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                            controller: _passwordNewController,
                            keyboardType: TextInputType.text,
                            obscureText: true,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(1.5 * SizeConfig.heightMultiplier),
                              filled: true,
                              fillColor: whiteAthenea,
                              border: OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea)),
                              focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: lightBlueAthenea), borderRadius: BorderRadius.circular(3.0)),
                              counter: Text('Caracteres: ' + contarPasswordNew(),
                                  style: TextStyle(
                                    fontSize: 2 * SizeConfig.heightMultiplier,
                                    color: darkBlueAthenea,
                                  )),
                              hintText: 'Introduzca su password',
                              hintStyle: TextStyle(
                                color: blueAthenea.withOpacity(0.5),
                              ),
                              labelText: 'Nueva contraseña (8 caracteres)',
                              labelStyle:
                                  TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: blueAthenea, fontWeight: FontWeight.bold),
                            ),
                            onChanged: (value) {
                              setState(() {
                                _passwordNew = value;
                              });
                            },
                            // The validator receives the text that the user has entered.
                            validator: (_passwordNew) {
                              if (_passwordNew.isEmpty) {
                                return 'No puede estar vacío';
                              }
                              if (_passwordNew.length < 6 || _passwordNew.length > 8) {
                                return '8 caracteres alfanuméricos';
                              }
                              return null;
                            },
                          ),
                        ),
                        //TEXTFORMFIELD PASSWORD NEW 2 CONFIRMAR PASSWORD
                        Padding(
                          padding: EdgeInsets.fromLTRB(
                            2 * SizeConfig.heightMultiplier,
                            1 * SizeConfig.heightMultiplier,
                            2 * SizeConfig.heightMultiplier,
                            2 * SizeConfig.heightMultiplier,
                          ),
                          child: TextFormField(
                            style: TextStyle(fontSize: 3 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                            controller: _passwordNew2Controller,
                            keyboardType: TextInputType.text,
                            obscureText: true,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(1.5 * SizeConfig.heightMultiplier),
                              filled: true,
                              fillColor: whiteAthenea,
                              border: OutlineInputBorder(borderSide: BorderSide(color: lightBlueAthenea)),
                              focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: blueAthenea)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: lightBlueAthenea), borderRadius: BorderRadius.circular(3.0)),
                              counter: Text('Caracteres: ' + contarPasswordNew2(),
                                  style: TextStyle(
                                    fontSize: 2 * SizeConfig.heightMultiplier,
                                    color: darkBlueAthenea,
                                  )),
                              hintText: 'Introduzca su password',
                              hintStyle: TextStyle(
                                color: blueAthenea.withOpacity(0.5),
                              ),
                              labelText: 'Repetir nueva contraseña',
                              labelStyle:
                                  TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: blueAthenea, fontWeight: FontWeight.bold),
                            ),
                            onChanged: (value) {
                              setState(() {
                                _passwordNew2 = value;
                              });
                            },
                            // The validator receives the text that the user has entered.
                            validator: (_passwordNew2) {
                              if (_passwordNew2.isEmpty) {
                                return 'Confirme la contraseña';
                              }
                              if (validarPassWordNew() == false) {
                                return 'Las contraseñas no coinciden';
                              }
                              return null;
                            },
                          ),
                        ),
                        SizedBox(
                          height: 2 * SizeConfig.heightMultiplier,
                        ),
                        //BOTONDARDEALTA
                        RaisedButton(
                          shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                          color: yellowAthenea,
                          onPressed: () {
                            // Validate returns true if the form is valid, otherwise false.
                            if (_formKey.currentState.validate()) {
                              // If the form is valid, display a snackbar. In the real world,
                              // you'd often call a server or save the information in a database.
                              //pr.show();
                              _cambiarPassword();
                            }
                          },
                          child: Text(
                            'Cambiar contraseña',
                            style: TextStyle(
                              fontSize: 2.5 * SizeConfig.heightMultiplier,
                              color: whiteAthenea,
                            ),
                          ),
                        )
                      ]))));
        });
      },
    );
  }

  String contarPassword() {
    if (_password == null) {
      return _caracteres.toString();
    } else {
      return _password.length.toString();
    }
  }

  String contarPasswordNew() {
    if (_passwordNew == null) {
      return _caracteres.toString();
    } else {
      return _passwordNew.length.toString();
    }
  }

  String contarPasswordNew2() {
    if (_passwordNew2 == null) {
      return _caracteres2.toString();
    } else {
      return _passwordNew2.length.toString();
    }
  }

  bool validarPassWordNew() {
    if (_passwordNew2 != _passwordNew) {
      return false;
    } else
      return true;
  }

  //METODO PARA CAMBIAR DE PASSWORD
  Future _cambiarPassword() async {
    var clienteProvider = Provider.of<ClientesInfoProvider>(context);
    String claveOld = _passwordController.text;
    String claveNueva = _passwordNewController.text;
    String usuario = clienteProvider.dni;
    String tercero = clienteProvider.tercero;
    //Objeto Xml2Json para enviar email
    Xml2Json xml2json = new Xml2Json();

    print('ModificarPass usuario: ' + clienteProvider.dni);
    print('ModificarPass tercero: ' + clienteProvider.tercero);
    print('ModificarPass ip: ' + _ip);
    print('ModificarPass clave old: ' + claveOld);
    print('ModificarPass clave new: ' + claveNueva);

    ///Generate MD5 hash
    _token(String data) {
      var content = new Utf8Encoder().convert(data);
      var md5 = crypto.md5;
      var digest = md5.convert(content);
      return hex.encode(digest.bytes);
    }

    String token = _token(clienteProvider.dni + claveOld + 'WCROPECPA');
    print(token);
    /* pr.show(); */

    //HACEMOS LA PETICION DENTRO DE UN TRY-CATCH
    try {
      String url = 'http://212.225.255.130:8010/ws/wcropecpa/$_ip/$token/$usuario/$tercero/$claveOld/$claveNueva';
      http.Response response = await http.get(url);
      /*  print("RESPUESTA RESPONSE.BODY");
      print(response.body); */
      //SI ESTA CORRECTO
      if (response.body.contains('TRUE')) {
        await pr.hide();
        /* print('TRUE'); */
        //LUCAS NUEVO - LLAMADA A METODO SET PASSWORD PASANDOLE CLAVE NUEVA COMO PARAMETRO
        clienteProvider.password(claveNueva);
        clienteProvider.setPassword(claveNueva);
        clienteProvider.password = claveNueva;

        /*   print('Clave actual: ' + clienteProvider.password);
        print("meter alert de que se ha modificado la contraseña"); */
        //Enviar email
        String urlEmail = 'http://212.225.255.130:8010/ws/rtvusrope/$usuario';
        http.Response responseEmail = await http.get(urlEmail);

        xml2json.parse(responseEmail.body);
        String jsondata = xml2json.toGData();

        Map<String, dynamic> data = convert.jsonDecode(jsondata);

        /*       print(data); */
        String resultMail = data['result']['Registro']['email'];
        /*   print("Email: $resultMail"); */
        sendmail(resultMail, claveNueva, usuario);
        /*     Navigator.pushNamed(context, 'login');
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.remove('pass'); */
      }
      //LA CONTRASEÑA ACTUAL NO ES CORRECTA
      else if (response.body.contains('E004')) {
        /*   print('E004'); */
        await pr.hide();
        showDialog(
            context: context,
            builder: (context) => new AlertDialog(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                  content: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        Icons.error,
                        color: redAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      SizedBox(width: 2 * SizeConfig.heightMultiplier),
                      Expanded(
                        child: Text(
                          "La contraseña actual no es correcta.",
                          style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                        ),
                      ),
                    ],
                  ),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: new Text('Ok',
                          style: TextStyle(
                            color: yellowAthenea,
                            fontSize: 2 * SizeConfig.heightMultiplier,
                          )),
                    ),
                  ],
                ));
      }
      //LA CONTRASEÑA NO PUEDE SER IGUAL QUE LA ANTERIOR
      if (response.body.contains('E005')) {
        /* print('E005'); */
        await pr.hide();
        showDialog(
            context: context,
            builder: (context) => new AlertDialog(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                  content: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        Icons.error,
                        color: redAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      SizedBox(width: 2 * SizeConfig.heightMultiplier),
                      Expanded(
                        child: Text(
                          "La nueva contraseña no puede ser igual que la anterior.",
                          style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                        ),
                      ),
                    ],
                  ),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: new Text('Ok',
                          style: TextStyle(
                            color: yellowAthenea,
                            fontSize: 2 * SizeConfig.heightMultiplier,
                          )),
                    ),
                  ],
                ));
      }
      //USUARIO BLOQUEADO
      if (response.body.contains('E006')) {
        /*  print('E006'); */
        await pr.hide();
        showDialog(
            context: context,
            builder: (context) => new AlertDialog(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                  content: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        Icons.error,
                        color: redAthenea,
                        size: 3 * SizeConfig.heightMultiplier,
                      ),
                      SizedBox(width: 2 * SizeConfig.heightMultiplier),
                      Expanded(
                        child: Text(
                          "Usuario bloqueado. Póngase en contacto con su operador.",
                          style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                        ),
                      ),
                    ],
                  ),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: new Text('Ok',
                          style: TextStyle(
                            color: yellowAthenea,
                            fontSize: 2 * SizeConfig.heightMultiplier,
                          )),
                    ),
                  ],
                ));
      }
      pr.hide();
    }
    //SI SALTA EL CATCH
    catch (e) {
      /*   print("error de conexión, inténtelo más tarde"); */
      showDialog(
          context: context,
          builder: (context) => new AlertDialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                content: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.error,
                      color: redAthenea,
                      size: 3 * SizeConfig.heightMultiplier,
                    ),
                    SizedBox(width: 2 * SizeConfig.heightMultiplier),
                    Expanded(
                      child: Text(
                        "Error de usuario. Inténtelo más tarde o póngase en contacto con su operador.",
                        style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                      ),
                    ),
                  ],
                ),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: new Text('Ok',
                        style: TextStyle(
                          color: yellowAthenea,
                          fontSize: 2 * SizeConfig.heightMultiplier,
                        )),
                  ),
                ],
              ));
      /*  print(e); */
      pr.hide();
    }
  }

  void sendmail(String resultMail, String resultNewPass, String usuario) async {
    print('EMAIL DONDE ENVIAR: $resultMail');
    print('NUEVA PASS: $resultNewPass');
    print('USUARIO: $usuario');
/* 
    String username = 'hola@mmovilnetwork.com';
    String password = 'marketing23team@m'; */

    String username = 'hola@mmovilnetwork.com';
    String password = 'bzrukfomyuixwfyw';

    final smtpServer = atheneacenfor(username, password);

    final message = Message()
      ..from = Address(username, 'MiMundoMOVIL')
      ..recipients = ['$resultMail']
      //..ccRecipients.addAll(['destCc1@example.com', 'destCc2@example.com'])
      //..bccRecipients.add(Address('bccAddress@example.com'))
      ..subject = 'Cambio de Clave'
      ..html =
          ''' <p>Estimado usuario, se ha reseteado tu clave de acceso, tus nuevas credenciales son:<br><br><strong>DNI: $usuario<br>Clave: $resultNewPass</strong><br><br>Por seguridad, te recomendamos que cambies la clave una vez que accedas a la app.</p><p style="color:#777;"><small><i>Este es un correo automatizado, no respondas al remitente.<i></small></p>''';

    try {
      final sendReport = await send(message, smtpServer);
      print('Message sent: ' + sendReport.toString());
      //LUCAS NUEVO 5/11/21 - AÑADIDO TEXTO Y FORMATO AL RECUPERAR CONTRASEÑA EN SHOWDIALOG
      showDialog(
          context: context,
          builder: (context) => new AlertDialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
                content: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.error,
                      color: redAthenea,
                      size: 3 * SizeConfig.heightMultiplier,
                    ),
                    SizedBox(width: 2 * SizeConfig.heightMultiplier),
                    Expanded(
                      child: Text(
                        "Contraseña modificada.",
                        style: TextStyle(fontSize: 2 * SizeConfig.heightMultiplier, color: darkBlueAthenea),
                      ),
                    ),
                  ],
                ),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () async {
                      SharedPreferences prefs = await SharedPreferences.getInstance();
                      prefs.remove('pass');
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => TusDatosPage(),
                          ));
                    },
                    child: new Text('Ok',
                        style: TextStyle(
                          color: yellowAthenea,
                          fontSize: 2 * SizeConfig.heightMultiplier,
                        )),
                  ),
                ],
              ));
    } on MailerException catch (e) {
      print('Message not sent.');
      for (var p in e.problems) {
        print('Problem: ${p.code}: ${p.msg}');
      }
    }
  }
}
